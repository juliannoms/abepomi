<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="relFatAssociado" language="groovy" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" forecolor="#FFFFFF" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="ASSOCIACAO" class="org.abepomi.model.Associacao" isForPrompting="false"/>
	<parameter name="NOME_RELATORIO" class="java.lang.String" isForPrompting="false"/>
	<parameter name="DETALHE" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C:\\Users\\Julianno\\Projetos\\ABEPOMI\\src\\java\\relatorios\\"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[]]>
	</queryString>
	<field name="associado" class="org.abepomi.model.Associado">
		<fieldDescription><![CDATA[associado]]></fieldDescription>
	</field>
	<field name="extra" class="java.lang.Double">
		<fieldDescription><![CDATA[extra]]></fieldDescription>
	</field>
	<field name="mensalidade" class="java.lang.Double">
		<fieldDescription><![CDATA[mensalidade]]></fieldDescription>
	</field>
	<field name="referencia" class="org.abepomi.model.Referencia">
		<fieldDescription><![CDATA[referencia]]></fieldDescription>
	</field>
	<field name="servicos" class="java.lang.Double">
		<fieldDescription><![CDATA[servicos]]></fieldDescription>
	</field>
	<field name="telefonia" class="java.lang.Double">
		<fieldDescription><![CDATA[telefonia]]></fieldDescription>
	</field>
	<field name="vivo" class="java.lang.Double">
		<fieldDescription><![CDATA[vivo]]></fieldDescription>
	</field>
	<field name="parcelas" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[parcelas]]></fieldDescription>
	</field>
	<variable name="valor" class="java.lang.Double">
		<variableExpression><![CDATA[$F{extra}+$F{mensalidade}+$F{servicos}+$F{telefonia}+$F{vivo}+$F{parcelas}]]></variableExpression>
	</variable>
	<title>
		<band height="113" splitType="Stretch">
			<rectangle>
				<reportElement x="0" y="0" width="555" height="92"/>
			</rectangle>
			<textField>
				<reportElement x="139" y="39" width="408" height="14"/>
				<textElement>
					<font fontName="DejaVu Sans" size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{ASSOCIACAO}.getEndereco().getLogradouro()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="110" y="96" width="347" height="16"/>
				<textElement textAlignment="Center">
					<font fontName="DejaVu Sans" size="10" isItalic="true" isUnderline="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{NOME_RELATORIO}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="139" y="5" width="408" height="14"/>
				<textElement>
					<font fontName="DejaVu Sans" size="9" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{ASSOCIACAO}.getNome()]]></textFieldExpression>
			</textField>
			<image>
				<reportElement x="17" y="5" width="108" height="82"/>
				<graphicElement>
					<pen lineStyle="Solid" lineColor="#FFFFFF"/>
				</graphicElement>
				<imageExpression class="java.lang.String"><![CDATA["logo abepomi.JPG"]]></imageExpression>
			</image>
			<line>
				<reportElement x="0" y="112" width="555" height="1"/>
			</line>
			<textField>
				<reportElement x="139" y="53" width="408" height="14"/>
				<textElement>
					<font fontName="DejaVu Sans" size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{ASSOCIACAO}.getEndereco().getCidade() +
", " +
$P{ASSOCIACAO}.getEndereco().getUf().name() +
" - " +
org.abepomi.model.util.Mascarar.colocaMascara(
    "##.###-###",
$P{ASSOCIACAO}.getEndereco().getCep())]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="184" y="78" width="217" height="9"/>
				<textElement>
					<font fontName="DejaVu Sans" size="6"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{ASSOCIACAO}.getContato() + " - " +
org.abepomi.model.util.Mascarar.colocaMascara(
    "(##)####-####",
$P{ASSOCIACAO}.getTelefone())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="139" y="78" width="45" height="9"/>
				<textElement>
					<font fontName="DejaVu Sans" size="6" isBold="true"/>
				</textElement>
				<text><![CDATA[Responsável:]]></text>
			</staticText>
			<textField>
				<reportElement x="139" y="19" width="408" height="14"/>
				<textElement>
					<font fontName="DejaVu Sans" size="9" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[org.abepomi.model.util.Mascarar.colocaMascara(
    "##.###.###/####-##",
$P{ASSOCIACAO}.getCnpj())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="462" y="78" width="32" height="9"/>
				<textElement>
					<font fontName="DejaVu Sans" size="6" isBold="true"/>
				</textElement>
				<text><![CDATA[Emissão:]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="494" y="78" width="53" height="9"/>
				<textElement>
					<font fontName="DejaVu Sans" size="6"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="16">
			<textField>
				<reportElement x="390" y="0" width="100" height="15"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{referencia}.getRef()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="10" y="0" width="42" height="15"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Nome:]]></text>
			</staticText>
			<textField>
				<reportElement x="51" y="0" width="264" height="15"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{associado}.getNome()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="332" y="0" width="59" height="15"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Referência:]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="15" width="555" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="142">
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="14" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{servicos}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="28" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{telefonia}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="42" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{vivo}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="56" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{extra}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="167" y="0" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Mensalidade:]]></text>
			</staticText>
			<staticText>
				<reportElement x="167" y="14" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Serviços:]]></text>
			</staticText>
			<staticText>
				<reportElement x="167" y="28" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Telefonia Claro:]]></text>
			</staticText>
			<staticText>
				<reportElement x="167" y="42" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Telefonia Vivo:]]></text>
			</staticText>
			<staticText>
				<reportElement x="167" y="56" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Extras:]]></text>
			</staticText>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="0" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{mensalidade}]]></textFieldExpression>
			</textField>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="84" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$V{valor}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="167" y="84" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total:]]></text>
			</staticText>
			<subreport>
				<reportElement x="0" y="100" width="555" height="42"/>
				<dataSourceExpression><![CDATA[org.abepomi.dao.DAOFactory.createExtrasDAO().getExtrasAssRef($F{associado},$F{referencia})]]></dataSourceExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{SUBREPORT_DIR} + "relFatAssociado_det_extras.jasper"]]></subreportExpression>
			</subreport>
			<line>
				<reportElement x="0" y="99" width="555" height="1"/>
			</line>
			<staticText>
				<reportElement x="167" y="70" width="100" height="15"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Parcelamentos:]]></text>
			</staticText>
			<textField pattern="¤ #,##0.00">
				<reportElement x="267" y="70" width="80" height="15"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{parcelas}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="13">
			<textField>
				<reportElement x="141" y="3" width="156" height="10"/>
				<textElement textAlignment="Right">
					<font fontName="DejaVu Sans" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Página "+$V{PAGE_NUMBER}+" de"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="297" y="3" width="135" height="10"/>
				<textElement>
					<font fontName="DejaVu Sans" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}+ "."]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="0" width="555" height="1"/>
			</line>
		</band>
	</pageFooter>
</jasperReport>
