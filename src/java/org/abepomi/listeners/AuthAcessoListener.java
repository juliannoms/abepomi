/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.listeners;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;
import org.abepomi.model.User;

/**
 *
 * @author Julianno
 */
public class AuthAcessoListener implements PhaseListener {

    public void afterPhase(PhaseEvent event) {
    }

    public void beforePhase(PhaseEvent event) {

        FacesContext facesContext = event.getFacesContext();
        String page = facesContext.getViewRoot().getViewId();

        if(!page.endsWith(".xhtml")){
            return;
        }

        if(page.contains("faturaweb")){
            return;
        }

        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");

        if(null == user){
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "loginPage");
            return;
        }

        if (!user.isAutorizada(page)) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Acesso não permitido!");
            facesContext.addMessage(page, message);
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "falha");
            return;
        }
    }

    /*private boolean isAutorizada(String page, List<Acesso> autorizadas) {

        if (!page.endsWith(".xhtml")) {
            return true;
        }


        for (Acesso acesso : autorizadas) {
            if (page.endsWith(acesso.getPagina())) {
                return true;
            }
        }

        return false;
    }*/

    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }
}
