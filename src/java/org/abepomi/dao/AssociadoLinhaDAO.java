/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.AssociadoLinhaUtil;
import org.abepomi.dao.util.AssociadoLinhaUtilRel;
import org.abepomi.model.Associado;
import org.abepomi.model.AssociadoLinha;

/**
 *
 * @author Julianno
 */
public interface AssociadoLinhaDAO extends CrudDAO<AssociadoLinha> {

    public List<AssociadoLinhaUtil> linhasPorAssociado(int associado);

    public List<Integer> associadosComLinhas();

    public List<AssociadoLinha> linhasAtivasPorAssociado(Associado ass);

    public AssociadoLinha associadoLinhaPorId(int id) throws DAOException;

    public List<AssociadoLinhaUtilRel> linhasPorAssociado();

}
