/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.commons;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Julianno
 */
public class CrudDAOJPA<T> implements CrudDAO<T>, Serializable {

    private Class<T> classEntity;

    @SuppressWarnings("unchecked")
    public CrudDAOJPA() {
        this.classEntity = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        try {
            String query = "from " + classEntity.getSimpleName() + " order by id";
            return manager.createQuery(query).getResultList();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public T findById(Serializable id) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        try {
            return manager.find(classEntity, id);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void addEntity(T entity) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        EntityTransaction t = manager.getTransaction();
        try {
            t.begin();
            manager.persist(entity);
            t.commit();
        } catch (Exception ex) {
            throw new DAOException(ex);
        }
    }

    public void updateEntity(T entity) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        EntityTransaction t = manager.getTransaction();
        try {
            t.begin();
            manager.merge(entity);
            t.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void removeEntity(T entity) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        EntityTransaction t = manager.getTransaction();
        try {
            t.begin();
            entity = (T) manager.getReference(entity.getClass(), entity);
            manager.remove(entity);
            t.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

}
