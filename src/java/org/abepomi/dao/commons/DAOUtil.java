/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.commons;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Julianno
 */
public class DAOUtil {

    private static EntityManagerFactory factory;
    private static EntityManager manager;

    public static EntityManagerFactory getEntityManagerFactory(){
        if(factory==null){
            factory = Persistence.createEntityManagerFactory("ABEPOMIPU");
        }
        return factory;
    }

    public static EntityManager getEntityManager(){
        //if(manager==null){
        manager = createEntityManager();
        //}
        return manager;
    }

    private static EntityManager createEntityManager(){
        return getEntityManagerFactory().createEntityManager();
    }

}
