/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.User;

/**
 *
 * @author Julianno
 */
public interface UserDAO extends CrudDAO<User>{

    public List<User> buscarPorLogin(String login);

}
