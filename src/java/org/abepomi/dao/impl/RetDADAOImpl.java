/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import javax.persistence.EntityManager;
import org.abepomi.dao.RetDADAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.RetDA;

/**
 *
 * @author Julianno
 */
public class RetDADAOImpl extends CrudDAOJPA<RetDA> implements RetDADAO {

    public Integer getMaxNsa() throws DAOException{
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return (Integer) manager.createNamedQuery("MaxNSA").getSingleResult();
    }

}
