/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.ExtrasTelefoniaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.ExtrasTelefonia;

/**
 *
 * @author Julianno
 */
public class ExtrasTelefoniaDAOImpl extends CrudDAOJPA<ExtrasTelefonia> implements ExtrasTelefoniaDAO{

    public List<ExtrasTelefonia> getPorLinhaSFaturar(String linha) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("extrasSFaturarPorLinha");
        query.setParameter("linha", linha);
        return query.getResultList();
    }

}
