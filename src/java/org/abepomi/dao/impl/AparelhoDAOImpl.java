/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import org.abepomi.dao.AparelhoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Aparelho;

/**
 *
 * @author Julianno
 */
public class AparelhoDAOImpl extends CrudDAOJPA<Aparelho> implements AparelhoDAO{

    public List<Aparelho> buscarComEstoque() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("Aparelho.estoque").getResultList();
    }

}
