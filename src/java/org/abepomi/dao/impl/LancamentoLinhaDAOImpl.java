/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.LancamentoLinhaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.LancamentoLinhaTotalUtil;
import org.abepomi.dao.util.LancamentoLinhaUtil;
import org.abepomi.model.LancamentoLinha;

/**
 *
 * @author Julianno
 */
public class LancamentoLinhaDAOImpl extends CrudDAOJPA<LancamentoLinha> implements LancamentoLinhaDAO {

    //LancamentoLinhaUtil(String tipo, Date data, Date hora, Double duracao, Double tarifa, Double valor, Double cobrado, String descricao)
    public List<LancamentoLinhaUtil> getLctosPorLinha(String linha, int tipo) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.LancamentoLinhaUtil(lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, "
                + " lcto.valor, lcto.cobrado, lcto.descricao, lcto.fatura.codigo) from LancamentoLinha lcto, Fatura fat "
                + "where lcto.fatura = fat and fat.faturada = 'false' and lcto.linha.numero = :linha ";
        if (tipo == 0) {
            sql = sql + "and lcto.hora is not null ";
        } else if (tipo == 1) {
            sql = sql + "and lcto.hora is null ";
        }
        sql = sql + "order by lcto.data, lcto.hora";
        Query query = manager.createQuery(sql);
        query.setParameter("linha", linha);

        return query.getResultList();
    }

    public Double valorGastoPorPeriodo(Date inicio, Date fim, String linha, int fatura) {
        return valorGastoPorPeriodoDemais(fim, linha, fatura, inicio) + valorGastoPorPeriodoLocais(fim, linha, fatura, inicio);
    }

    private Double valorGastoPorPeriodoDemais(Date fim, String linha, int fatura, Date inicio) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select sum(lcto.valor) from LancamentoLinha lcto where lcto.linha.numero = :linha " 
                + "and lcto.hora is not null and lcto.fatura.codigo = :fatura and lcto.descricao <> '"
                +LancamentoLinhaUtil.DESCRICAO_LOCAIS+"' and lcto.data >= :inicio ";
        if (!(null == fim)) {
            sql = sql + " and lcto.data <= :fim";
        }
        Query query = manager.createQuery(sql);
        query.setParameter("linha", linha);
        query.setParameter("fatura", fatura);
        query.setParameter("inicio", inicio);
        if (!(null == fim)) {
            query.setParameter("fim", fim);
        }
        Double valor = (Double) query.getSingleResult();
        return valor == null ? 0.00 : valor;
    }
    private Double valorGastoPorPeriodoLocais(Date fim, String linha, int fatura, Date inicio) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select sum(lcto.duracao * 0.3) from LancamentoLinha lcto where lcto.linha.numero = :linha "
                + "and lcto.hora is not null and lcto.fatura.codigo = :fatura and lcto.descricao = '"
                +LancamentoLinhaUtil.DESCRICAO_LOCAIS+"' and lcto.data >= :inicio ";
        if (!(null == fim)) {
            sql = sql + " and lcto.data <= :fim";
        }
        Query query = manager.createQuery(sql);
        query.setParameter("linha", linha);
        query.setParameter("fatura", fatura);
        query.setParameter("inicio", inicio);
        if (!(null == fim)) {
            query.setParameter("fim", fim);
        }
        Double valor = (Double) query.getSingleResult();
        return valor == null ? 0.00 : valor;
    }

    public List<LancamentoLinhaUtil> getLctosPorInicioTipo(Date data, int tipo) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.LancamentoLinhaUtil(lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, "
                + " lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino) from LancamentoLinha lcto, Fatura f "
                + "where lcto.fatura = f.codigo and f.inicio = :data ";
        if (tipo == LancamentoLinhaUtil.LCTO_UTILIZACAO) {
            sql = sql + "and lcto.hora is not null ";
        } else if (tipo == LancamentoLinhaUtil.LCTO_MENSALIDADES) {
            sql = sql + "and lcto.hora is null ";
        } else if (tipo == LancamentoLinhaUtil.LCTO_UTILIZACAO_OUTROS) {
            sql = sql + "and lcto.hora is not null and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_LOCAIS + "' and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_TARZERO + "' ";
        }
        sql = sql + "group by lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino order by lcto.linha, lcto.data, lcto.hora";
        Query query = manager.createQuery(sql);
        query.setParameter("data", data);

        return query.getResultList();
    }

    public LancamentoLinhaTotalUtil getUtilizacaoPorFiltro(String linha, String filtro, int fatura, Date inicio, Date fim) {
        String sql = "select new org.abepomi.dao.util.LancamentoLinhaTotalUtil(l.linha.numero, sum(l.duracao)) "
                + "from LancamentoLinha l where l.linha.numero = :linha and "
                + " l.fatura.codigo = :fatura and l.data between :inicio and :fim " + filtro + " "
                + "group by l.linha.numero ";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery(sql);
        query.setParameter("linha", linha);
        query.setParameter("fatura", fatura);
        query.setParameter("inicio", inicio);
        query.setParameter("fim", fim);
        try {
            return (LancamentoLinhaTotalUtil) query.getSingleResult();
        } catch (NoResultException nex) {
            return null;
        }
    }

    public JRBeanCollectionDataSource getLancUtilOutros(String linha, Date inicio, Date fim, Date iniFatura) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.LancamentoLinhaUtil(lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, "
                + " lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino) from LancamentoLinha lcto, Fatura f "
                + "where lcto.data >= :inicio and lcto.data <= :fim and lcto.linha.numero = :linha and lcto.fatura = f and f.inicio = :iniFatura ";
        sql = sql + "and lcto.hora is not null and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_LOCAIS + "' and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_TARZERO + "' ";
        sql = sql + "group by lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino order by lcto.linha, lcto.data, lcto.hora";
        Query query = manager.createQuery(sql);
        query.setParameter("inicio", inicio);
        query.setParameter("fim", fim);
        query.setParameter("linha", linha);
        query.setParameter("iniFatura", iniFatura);
        return new JRBeanCollectionDataSource(query.getResultList());
    }

    public JRBeanCollectionDataSource getLancUtilMensalidades(String linha, Date inicio, Date fim, Date iniFatura) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.LancamentoLinhaUtil(lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, "
                + " lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino) from LancamentoLinha lcto, Fatura f "
                + "where lcto.data >= :inicio and lcto.data <= :fim and lcto.linha.numero = :linha and lcto.fatura = f and f.inicio = :iniFatura ";
        sql = sql + "and lcto.hora is null ";
        sql = sql + "and lcto.tipoLancamento not like '%" + LancamentoLinhaUtil.TIPO_GESTOR + "%' ";
        sql = sql + "and lcto.tipoLancamento not like '%" + LancamentoLinhaUtil.TIPO_TARZERO + "%' ";
        sql = sql + "and lcto.tipoLancamento not like '%" + LancamentoLinhaUtil.TIPO_ASSINATURA + "%' ";
        sql = sql + "and lcto.tipoLancamento not like '%" + LancamentoLinhaUtil.TIPO_PLANO_MODEM + "%' ";
        sql = sql + "and lcto.tipoLancamento not like '%" + LancamentoLinhaUtil.TIPO_PLANO_TABLET + "%' ";
        sql = sql + "group by lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino order by lcto.linha, lcto.data, lcto.hora";
        //System.out.println(sql);
        Query query = manager.createQuery(sql);
        query.setParameter("inicio", inicio);
        query.setParameter("fim", fim);
        query.setParameter("linha", linha);
        query.setParameter("iniFatura", iniFatura);
        return new JRBeanCollectionDataSource(query.getResultList());
    }
    /* EntityManager manager = EntityManagerUtil.getEntityManager();
    String sql = "select NEW org.abepomi.dao.util.LancamentoLinhaUtil(lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, "
    + " lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino) from LancamentoLinha lcto, Fatura f "
    + "where lcto.data >= :inicio and lcto.data <= :fim and lcto.linha.numero = :linha and lcto.fatura = f and f.inicio = :iniFatura ";
    if (tipo == LancamentoLinhaUtil.LCTO_UTILIZACAO) {
    sql = sql + "and lcto.hora is not null ";
    } else if (tipo == LancamentoLinhaUtil.LCTO_MENSALIDADES) {
    sql = sql + "and lcto.hora is null ";
    sql = sql + "and lcto.tipo not like '%"+LancamentoLinhaUtil.TIPO_GESTOR+"%' ";
    sql = sql + "and lcto.tipo not like '%"+LancamentoLinhaUtil.TIPO_TARZERO+"%' ";
    sql = sql + "and lcto.tipo not like '%"+LancamentoLinhaUtil.TIPO_ASSINATURA+"%' ";
    sql = sql + "and lcto.tipo not like '%"+LancamentoLinhaUtil.TIPO_PLANO_MODEM+"%' ";
    } else if (tipo == LancamentoLinhaUtil.LCTO_UTILIZACAO_OUTROS) {
    sql = sql + "and lcto.hora is not null and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_LOCAIS + "' and lcto.descricao <> '" + LancamentoLinhaUtil.DESCRICAO_TARZERO + "' ";
    }
    sql = sql + "group by lcto.tipoLancamento, lcto.data, lcto.hora, lcto.duracao, lcto.valor, lcto.linha.numero, lcto.origem, lcto.numeroDestino order by lcto.linha, lcto.data, lcto.hora";
    Query query = manager.createQuery(sql);
    query.setParameter("inicio", inicio);
    query.setParameter("fim", fim);
    query.setParameter("linha", linha);
    query.setParameter("iniFatura", iniFatura);
    return new JRBeanCollectionDataSource(query.getResultList());*/
}
