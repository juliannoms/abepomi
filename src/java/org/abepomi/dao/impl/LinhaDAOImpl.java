/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import org.abepomi.dao.LinhaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Linha;

/**
 *
 * @author Julianno
 */
public class LinhaDAOImpl extends CrudDAOJPA<Linha> implements LinhaDAO {

    public List<Linha> getLinhasLivres() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("linhasLivres").getResultList();
    }

}
