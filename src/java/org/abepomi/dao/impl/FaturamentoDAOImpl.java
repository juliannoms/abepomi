/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.FaturamentoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.FaturamentoRefRel;
import org.abepomi.model.Associado;
import org.abepomi.model.Faturamento;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class FaturamentoDAOImpl extends CrudDAOJPA<Faturamento> implements FaturamentoDAO {

    @Override
    public Faturamento buscaFatPorAssRef(Associado ass, Referencia ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("select fat from Faturamento fat where fat.associado = :ass and fat.referencia = :ref");
        query.setParameter("ref", ref);
        query.setParameter("ass", ass);
        return (Faturamento) query.getSingleResult();
    }

    @Override
    public List<FaturamentoRefRel> receitaPorReferencia(Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String hql = "select new org.abepomi.dao.util.FaturamentoRefRel(ass.nome, f.servicos, f.mensalidade, f.telefonia, f.vivo, f.extra, f.parcelas, r.ref) ";
        hql = hql + "from Associado ass, Faturamento f, Referencia r ";
        hql = hql + "where ass = f.associado and f.referencia = r and r = :referencia ";
        hql = hql + "order by ass.nome";
        Query query = manager.createQuery(hql);
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

    @Override
    public List<FaturamentoRefRel> receitaPorReferencia() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String hql = "select new org.abepomi.dao.util.FaturamentoRefRel(r.ref, sum(f.servicos), sum(f.mensalidade), sum(f.telefonia), sum(f.vivo), sum(f.extra), sum(f.parcelas), r.ref) ";
        hql = hql + "from Faturamento f, Referencia r ";
        hql = hql + "where f.referencia = r group by r.ref, r.id ";
        hql = hql + "order by r.id desc";
        Query query = manager.createQuery(hql);
        return query.getResultList();
    }

    @Override
    public List<Faturamento> buscaCobrancaRef(Referencia ref) {
        String sql = "select f from Faturamento f where f.referencia = :ref"
                + " and f.associado not in (select m.assDA.associado from MovDA m where m.remDA.ref = :ref)";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery(sql);
        query.setParameter("ref", ref);
        //query.setParameter("rref", ref);
        try {
            return query.getResultList();
        } catch (NoResultException noex) {
        }

        return null;
    }

    @Override
    public List<FaturamentoRefRel> faturadosSemDebito(Referencia ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<FaturamentoRefRel> retorno = new ArrayList();
        String hql = "select ass.nome, fat.valor_servicos, fat.valor_mensalidade, fat.valor_telefonia, "
                + "fat.valor_vivo, fat.valor_extra, fat.valor_parcelas, r.mesref from faturamento fat, associados ass, referencia r "
                + " where fat.ass_id = ass.id and fat.ref_id = r.id and r.id = :ref and ass.id not in "
                + "(select a.id from associados a, ass_aut aa, mov_aut ma, aut_remessa ar "
                + "where a.id = aa.associado and aa.id = ma.ass_id and ma.rem_id = ar.id and "
                + "ar.ref_id = :ref) order by ass.nome;";
        Query query = manager.createNativeQuery(hql);
        query.setParameter("ref", ref.getId());
        List<Object[]> lista = query.getResultList();
        for (Object[] result : lista) {
            retorno.add(new FaturamentoRefRel((String) result[0], (Double) result[1], (Double) result[2], (Double) result[3], (Double) result[4], (Double) result[5], (BigDecimal) result[6], (String) result[7]));
        }
        return retorno;
    }

    @Override
    public Boolean isAssociadoCobravelBoleto(int ass, int ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String resultado = null;
        Query query = manager.createNativeQuery("select ma.status from aut_remessa ar, mov_aut ma, ass_aut aa where aa.id = "
                + "ma.ass_id and ar.id = ma.rem_id and aa.associado = :associado and ar.ref_id = :ref");
        query.setParameter("associado",ass);
        query.setParameter("ref",ref);

        try{
            resultado = (String) query.getSingleResult();
        }catch(NoResultException ex){
            return true;
        }

        if(resultado.equals("00")|| resultado.equals("AB")){
            return false;
        }
        return true;

    }

    @Override
    public List<Faturamento> faturamentoAssociadoBoleto(Referencia referencia){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("select f from Faturamento f , AssDA a  where f.associado = a.associado "
                + "and a.opcao <> 'C' and f.referencia = :referencia");
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

    @Override
    public List<Faturamento> faturamentoAssociadoNDebitado(Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("select f from Faturamento f, RemDA r, MovDA m, AssDA d where f.referencia = :ref and r.ref = f.referencia "
                                        + "and m.remDA = r and f.associado = d.associado and d = m.assDA and m.status not in ('00','31','96')");
        query.setParameter("ref", referencia);
        return query.getResultList();
    }
}
