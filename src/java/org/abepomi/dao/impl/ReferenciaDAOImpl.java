/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.ReferenciaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class ReferenciaDAOImpl extends CrudDAOJPA<Referencia> implements ReferenciaDAO{

    public List<Referencia> buscaRefSemFaturamento() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("RefSemFaturamento").getResultList();
    }

    public Referencia findByRef(String ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("findByRef");
        query.setParameter("referencia", ref);
        return (Referencia)query.getSingleResult();
    }

    public List<Referencia> buscaRefSemRemessa() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("RefSemRemessa").getResultList();
    }

    public List<Referencia> buscaTodasDecrescente() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("RefTodasDesc").getResultList();
    }

    public Referencia buscarUltima(){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Referencia.buscarUltima");
        Referencia r = null;
        try{
            r = (Referencia)query.getSingleResult();
        }catch(NoResultException ex){}
        return r;

    }



}
