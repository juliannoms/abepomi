/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.UserDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.User;

/**
 *
 * @author Julianno
 */
public class UserDAOImpl extends CrudDAOJPA<User> implements UserDAO{

    public List<User> buscarPorLogin(String login) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("buscarPorLogin");
        query.setParameter("login", "%"+login+"%");
        return query.getResultList();
    }
}
