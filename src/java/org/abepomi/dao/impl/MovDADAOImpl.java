/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.MovDADAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.MovDA;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class MovDADAOImpl extends CrudDAOJPA<MovDA> implements MovDADAO{

    public MovDA buscaPorAssVencimento(String ass, Double valor) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("PesquisaByAssValor");
        query.setParameter("associado", ass);
        query.setParameter("valor", valor);
        try{
            return (MovDA)query.getSingleResult();
        }catch(NoResultException nre){
            System.out.print("O associado "+ass+" não possui o valor "+valor+" em aberto!");
            return null;
        }
    }

    public List<MovDA> buscaPorMesRef(Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("PorMesRef");
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

    public List<MovDA> buscaDebitados(Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Debitados");
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

    public List<MovDA> buscaNDebitados(Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("NDebitados");
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

    public void atualizarStatusDA(String status, String associado, BigDecimal valor) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("AtualizarStatus");
        query.setParameter("status",status);
        query.setParameter("associado", associado);
        query.setParameter("valor", valor);
        query.executeUpdate();
    }

}
