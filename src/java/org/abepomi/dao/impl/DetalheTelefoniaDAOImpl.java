/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;


import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.DetalheTelefoniaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.DetalheTelefoniaUtil;
import org.abepomi.dao.util.DetalheTelefoniaUtilRel;
import org.abepomi.model.DetalheTelefonia;

/**
 *
 * @author Julianno
 */
public class DetalheTelefoniaDAOImpl extends CrudDAOJPA<DetalheTelefonia> implements DetalheTelefoniaDAO {

    public DetalheTelefoniaUtil getTotalPorReferenciaAssociado(int referencia, int associado) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.DetalheTelefoniaUtil(sum(d.plano), sum(d.pacotes), sum(d.outros), sum(d.franquia), "
                + "sum(d.excedidos * d.valor_minuto), sum(d.aparelho), sum(d.habilitacao), sum(d.credito), sum(d.desconto), sum(d.acrescimo)) from DetalheTelefonia d, FechamentoTelefonia f "
                    +"where  d.fechamento = f and f.referencia.id = "+referencia+" and d.associado = "+associado;
        Query query = manager.createQuery(sql);
        return (DetalheTelefoniaUtil) query.getSingleResult();
    }

    public List<DetalheTelefoniaUtilRel> getTotaisLinhaPorReferencia(Date inicio, int ass_id) {
        /*String sql= "select NEW org.abepomi.dao.util.DetalheTelefoniaUtilRel(d.plano, d.pacotes, d.outros, d.franquia, d.excedidos * d.valor_minuto, d.valor_minuto, d.excedidos, d.aparelho,"
                + "d.habilitacao, d.credito, d.debito, f.inicio, f.fim, al.entrega, al.devolucao, a.nome, d.linha, d.descricaoPlano, d.minutosLocais, d.desconto, d.acrescimo)"
                + "from DetalheTelefonia d, Fatura f, Associado a, Referencia r, FechamentoTelefonia fe, AssociadoLinha al "
                + "where d.fatura = f.codigo and d.associado = a.id and a = al.associado "
                + "and d.fechamento = fe and fe.referencia = r and d.linha = al.linha.numero and r.id = :ref "
                + "order by a.nome";*/
        String sql= "select NEW org.abepomi.dao.util.DetalheTelefoniaUtilRel(d.plano, d.pacotes, d.outros, d.franquia, d.excedidos * d.valor_minuto, d.valor_minuto, d.excedidos, d.aparelho,"
                + "d.habilitacao, d.credito, d.debito, f.inicio, f.fim, al.entrega, al.devolucao, a.nome, d.linha, d.descricaoPlano, d.minutosLocais, d.desconto, d.acrescimo)"
                + "from DetalheTelefonia d, Fatura f, Associado a, AssociadoLinha al "
                + "where d.fatura = f.codigo and d.associado = a.id and a = al.associado "
                + "and d.linha = al.linha.numero and f.inicio = :data and a.id = :ass_id ";
                //+ "order by a.nome";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery(sql);
        query.setParameter("data", inicio);
        query.setParameter("ass_id", ass_id);
        return query.getResultList();
    }

    public List<DetalheTelefoniaUtilRel> getTotaisLinhaAssReferencia(int ref_id, int ass_id) {
        String sql= "select NEW org.abepomi.dao.util.DetalheTelefoniaUtilRel(d.plano, d.pacotes, d.outros, d.franquia, d.excedidos * d.valor_minuto, d.valor_minuto, d.excedidos, d.aparelho,"
                + "d.habilitacao, d.credito, d.debito, f.inicio, f.fim, al.entrega, al.devolucao, a.nome, d.linha, d.descricaoPlano, d.minutosLocais, d.desconto, d.acrescimo)"
                + "from DetalheTelefonia d, Fatura f, Associado a, AssociadoLinha al, FechamentoTelefonia ft "
                + "where d.fatura = f.codigo and d.associado = a.id and a = al.associado "
                + "and d.linha = al.linha.numero and d.fechamento = ft and a.id = :ass_id and ft.referencia.id = :ref_id ";
                //+ "order by a.nome";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery(sql);
        query.setParameter("ref_id", ref_id);
        query.setParameter("ass_id", ass_id);
        return query.getResultList();
    }

    public List<DetalheTelefonia> getDetalhesSFaturar(int associado){
        String sql = "select d from DetalheTelefonia d where d.associado = :associado and d.fechamento.referencia is null";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery(sql).setParameter("associado", associado);
        return query.getResultList();
    }
    

}
