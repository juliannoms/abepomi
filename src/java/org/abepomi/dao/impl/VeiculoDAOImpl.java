/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.VeiculoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Veiculo;

/**
 *
 * @author Julianno
 */
public class VeiculoDAOImpl extends CrudDAOJPA<Veiculo> implements VeiculoDAO{

    public void delete(Veiculo veiculo) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("delete from Veiculo v  where v = :veiculo");
        query.setParameter("veiculo", veiculo);
        try{
            query.executeUpdate();
        }catch(Exception e){
            throw new DAOException(e);
        }
    }

    public List<Veiculo> listaPorAssociado() throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("VeiculosPorAssociado");
        try{
            return query.getResultList();
        } catch (Exception e){
            throw new DAOException(e);
        }
    }

}
