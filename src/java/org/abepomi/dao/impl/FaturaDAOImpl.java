/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import org.abepomi.dao.FaturaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.FaturaUtil;
import org.abepomi.dao.util.FaturaUtilRelCusto;
import org.abepomi.model.Fatura;

/**
 *
 * @author Julianno
 */
public class FaturaDAOImpl extends CrudDAOJPA<Fatura> implements FaturaDAO {

    @Override
    public int qtasSemFaturar() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("semFaturar");
        return (Integer) query.getSingleResult();
    }

    @Override
    public List<FaturaUtil> buscaNFaturados() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("buscaNFaturados");
        return query.getResultList();
    }

    @Override
    public int buscaPorTelefoneSFaturar(String numero) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select faturas.codigo from faturas, lancamentos where lancamentos.fatura = faturas.codigo and faturas.faturada = 'false' "
                + "and lancamentos.numero = '"+numero+"' group by faturas.codigo";
        Query query = manager.createNativeQuery(sql);
        try{
            return (Integer) query.getSingleResult();
        }catch(NoResultException ex){    
            Logger.getLogger(FaturaDAOImpl.class.getName()).log(Level.SEVERE, "Numero da linha: "+numero, ex);
        }catch (NonUniqueResultException ex){
            Logger.getLogger(FaturaDAOImpl.class.getName()).log(Level.SEVERE, "Número da linha: "+numero, ex);
            throw new NonUniqueResultException(ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<FaturaUtilRelCusto> custoPorTelefone(){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("Fatura.custoPorLinha").getResultList();
    }

    @Override
    public void updateWithFlush(Fatura fatura){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        manager.merge(fatura);
        manager.flush();
        manager.clear();
    }

    @Override
    public boolean isFaturaCadastrada(String notafiscal){
        String sql = "select f from Fatura f where f.nota = :notafiscal";
        Object o = null;
        try{
            o = EntityManagerUtil.getEntityManager().createQuery(sql).setParameter("notafiscal", notafiscal).getSingleResult();
        }catch(NoResultException norex){}
        return null == o ? false : true;
    }

    @Override
    public List<Date> buscarDatasInicio(){
        String sql = "select f.inicio from Fatura f group by f.inicio order by f.inicio desc";
        EntityManager manager  = EntityManagerUtil.getEntityManager();
        List<Date> datas = manager.createQuery(sql).getResultList();
        Logger.getLogger(FaturaDAOImpl.class.getName()).log(Level.SEVERE, "Qtde de datas: {0}", datas.size());
        if(null == datas){
            return null;
        }
        return datas;
    }
}
