/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.ContratoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Contrato;

/**
 *
 * @author Julianno
 */
public class ContratoDAOImpl extends CrudDAOJPA<Contrato> implements ContratoDAO {

    public Contrato buscaPorNumero(String numero) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("buscaPorNumero");
        query.setParameter("numero", numero);
        return (Contrato)query.getSingleResult();
    }

    public int qtdeContratos() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("qtdeContratos");
        return (Integer) query.getSingleResult();
    }


}
