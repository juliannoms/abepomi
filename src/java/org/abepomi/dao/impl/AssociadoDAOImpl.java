/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.AssociadoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.AssociadoEmail;
import org.abepomi.model.Associado;

/**
 *
 * @author Julianno
 */
public class AssociadoDAOImpl extends CrudDAOJPA<Associado> implements AssociadoDAO{
    
    @SuppressWarnings("unchecked")
    public List<Associado> buscaPorNome(String nome) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        StringBuilder hql = new StringBuilder("SELECT Associado from Associado associado where upper(associado.nome) like '"+nome.toUpperCase()+"%'");
        Query query = manager.createQuery(hql.toString());
        return (List<Associado>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Associado> buscaPorCpf(String cpf) throws DAOException{
        EntityManager manager = EntityManagerUtil.getEntityManager();
        StringBuilder hql = new StringBuilder("SELECT Associado from Associado associado where associado.cpf like :cpf");
        Query query = manager.createQuery(hql.toString());
        query.setParameter("cpf", cpf);
        return (List<Associado>) query.getResultList();
    }

    public List<Associado> findAllByNome() throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return (List<Associado>) manager.createNamedQuery("Associado.findAllByNome").getResultList();
    }

    public List<AssociadoEmail> buscaAssociadoEmail() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Associado.assEmail");
        return (List<AssociadoEmail>) query.getResultList();
    }

}
