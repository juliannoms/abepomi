/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.FechamentoTelefoniaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.FechamentoTelefonia;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class FechamentoTelefoniaDAOImpl extends CrudDAOJPA<FechamentoTelefonia>  implements FechamentoTelefoniaDAO{

    public void atualizarReferencia(Referencia referencia) {
        String sql = "update FechamentoTelefonia f set f.referencia = :referencia where f.referencia is null";
        EntityManager manager = EntityManagerUtil.getEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createQuery(sql).setParameter("referencia",referencia);
        query.executeUpdate();
        manager.getTransaction().commit();
    }

    public void updateWithFlush(FechamentoTelefonia fecha) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.merge(fecha);
        em.flush();
        em.clear();
    }

}
