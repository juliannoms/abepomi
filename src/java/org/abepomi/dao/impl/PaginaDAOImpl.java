/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.PaginaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Pagina;

/**
 *
 * @author Julianno
 */
public class PaginaDAOImpl extends CrudDAOJPA<Pagina> implements PaginaDAO {

    public List<Pagina> getPaginasSeguras(){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("paginasSeguras");
        return query.getResultList();
    }

    public List<Pagina> getPaginasLiberadas(){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("paginasLiberadas");
        return query.getResultList();
    }

}
