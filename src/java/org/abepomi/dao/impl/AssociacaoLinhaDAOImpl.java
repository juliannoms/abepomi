/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import org.abepomi.dao.AssociacaoLinhaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.AssociacaoLinha;

/**
 *
 * @author Julianno
 */
public class AssociacaoLinhaDAOImpl extends CrudDAOJPA<AssociacaoLinha> implements AssociacaoLinhaDAO{

    public List<AssociacaoLinha> linhasUsoAssociacao() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        //return manager.createNamedQuery("AssociacaoLinhas.linhasNaoDevolvidas").getResultList();

        return manager.createQuery("select aso from AssociacaoLinha aso where aso.devolucao is null order by aso.linha.numero").getResultList();
    }

}
