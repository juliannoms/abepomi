/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.AssDADAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.AssDA;
import org.abepomi.model.Associado;
import org.abepomi.model.util.EstadoDA;

/**
 *
 * @author Julianno
 */
public class AssDADAOImpl extends CrudDAOJPA<AssDA> implements AssDADAO{

    @SuppressWarnings("unchecked")
    public AssDA buscaPorAssociado(Associado ass) throws DAOException{
        EntityManager manager = EntityManagerUtil.getEntityManager();
        StringBuilder hql = new StringBuilder("SELECT AssDA from AssDA assDA where assDA.associado = :ass");
        Query query = manager.createQuery(hql.toString());
        query.setParameter("ass", ass);
        AssDA da = null;
        try{
            da = (AssDA) query.getSingleResult();
        }catch(NoResultException e){
            //TODO
        }
        return da;
    }

    public List<AssDA> buscaPorEstado(EstadoDA estado) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("DAPorEstado");
        query.setParameter("opcao", estado);
        try{
            return (List<AssDA>) query.getResultList();
        }catch(NoResultException e){
            //TODO
        }
        return null;
    }

    public List<AssDA> buscaPorNaoEstado(EstadoDA estado) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("DAPorNaoEstado");
        query.setParameter("opcao", estado);
        try{
            return (List<AssDA>) query.getResultList();
        }catch(NoResultException e){
            //TODO
        }
        return null;
    }
}
