/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.ExtrasDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Associado;
import org.abepomi.model.Extras;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class ExtrasDAOImpl extends CrudDAOJPA<Extras> implements ExtrasDAO {

    public Double totalPorAssociado(Associado ass, Referencia ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select sum(e.valor) from Extras e where e.ass_id = :ass and e.ref_id = :ref");
        query.setParameter("ass", ass.getId());
        query.setParameter("ref", ref.getId());
        Double resultado = (Double)query.getSingleResult();
        return resultado == null ? (Double)0.00 : resultado;
    }

    public int proximaId(){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select nextval(\'seq_extras\')");
        return ((BigInteger) query.getSingleResult()).intValue();
    }

    public JRBeanCollectionDataSource getExtrasAssRef(Associado ass, Referencia ref) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select e.descricao, e.valor from extras e where e.ass_id = :ass and e.ref_id = :ref order by e.descricao;");
        query.setParameter("ass", ass.getId());
        query.setParameter("ref", ref.getId());
        List<Object[]> ex = query.getResultList();
        List<Extras> extras = new ArrayList<Extras>();
        for(Object[] o : ex){
            extras.add(new Extras(String.valueOf(o[0]),(Double)o[1]));
        }
        return new JRBeanCollectionDataSource(extras);
    }

}
