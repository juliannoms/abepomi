/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.AssociadoLinhaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.AssociadoLinhaUtil;
import org.abepomi.dao.util.AssociadoLinhaUtilRel;
import org.abepomi.model.Associado;
import org.abepomi.model.AssociadoLinha;

/**
 *
 * @author Julianno
 */
public class AssociadoLinhaDAOImpl extends CrudDAOJPA<AssociadoLinha> implements AssociadoLinhaDAO{

    public List<AssociadoLinhaUtil> linhasPorAssociado(int associado) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("linhasPorAssociado");
        query.setParameter("associado", associado);
        return query.getResultList();
    }

    public List<Integer> associadosComLinhas() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("associadosComTelefone").getResultList();
    }

    public List<AssociadoLinha> linhasAtivasPorAssociado(Associado ass) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<AssociadoLinha> lista = manager.createNamedQuery("AssociadoLinha.linhasAtivasPorAssociado").setParameter("ass", ass).getResultList();
        return lista;
    }

    public AssociadoLinha associadoLinhaPorId(int id) throws DAOException {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        try{
            return (AssociadoLinha)manager.createNamedQuery("AssociadoLinha.associadoLinhaPorId").setParameter("id", id).getSingleResult();
        }catch(Exception ex){
            throw new DAOException(ex);
        }
    }

    public List<AssociadoLinhaUtilRel> linhasPorAssociado() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        return manager.createNamedQuery("AssociadoLinha.linhasPorAssociado").getResultList();
    }

}
