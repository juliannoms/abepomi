/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.AssDA;
import org.abepomi.model.Associado;
import org.abepomi.model.util.EstadoDA;

/**
 *
 * @author Julianno
 */
public interface AssDADAO extends CrudDAO<AssDA> {

    public AssDA buscaPorAssociado(Associado ass) throws DAOException;

    public List<AssDA> buscaPorEstado(EstadoDA estado) throws DAOException;

    public List<AssDA> buscaPorNaoEstado(EstadoDA estado) throws DAOException;
}
