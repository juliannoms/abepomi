/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Linha;

/**
 *
 * @author Julianno
 */
public interface LinhaDAO extends CrudDAO<Linha>{

    public List<Linha> getLinhasLivres();

}
