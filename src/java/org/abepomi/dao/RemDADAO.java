/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.RemDA;

/**
 *
 * @author Julianno
 */
public interface RemDADAO extends CrudDAO<RemDA> {

    public Integer getMaxNsa() throws DAOException;

}
