/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.RetDA;

/**
 *
 * @author Julianno
 */
public interface RetDADAO extends CrudDAO<RetDA>{

    public Integer getMaxNsa() throws DAOException;

}
