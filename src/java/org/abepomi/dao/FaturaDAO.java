/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.Date;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.util.FaturaUtil;
import org.abepomi.dao.util.FaturaUtilRelCusto;
import org.abepomi.model.Fatura;

/**
 *
 * @author Julianno
 */
public interface FaturaDAO extends CrudDAO<Fatura> {

    public int qtasSemFaturar();

    public List<FaturaUtil> buscaNFaturados();

    public int buscaPorTelefoneSFaturar(String numero);

    public List<FaturaUtilRelCusto> custoPorTelefone();

    public void updateWithFlush(Fatura fatura);

    public boolean isFaturaCadastrada(String notafiscal);

    public List<Date> buscarDatasInicio();

}
