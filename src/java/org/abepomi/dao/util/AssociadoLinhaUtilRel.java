/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;
import org.abepomi.model.util.TipoLinha;

/**
 *
 * @author Julianno
 */
public class AssociadoLinhaUtilRel {

    private int associado;
    private String nome;
    private String numero;
    private Date entrega;
    private Date devolucao;
    private String plano;
    private TipoLinha tipo;

    public AssociadoLinhaUtilRel(int associado, String nome, String numero, Date entrega, Date devolucao, String plano, TipoLinha tipo) {
        this.associado = associado;
        this.nome = nome;
        this.numero = numero;
        this.entrega = entrega;
        this.devolucao = devolucao;
        this.plano = plano;
        this.tipo = tipo;
    }

    public String getPlano(){
        return plano;
    }

    public int getAssociado() {
        return associado;
    }

    

    public Date getDevolucao() {
        return devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public String getNome() {
        return nome;
    }

    public String getNumero() {
        return numero;
    }

    public String getTipo() {
        return tipo.getTipoLinha();
    }



}
