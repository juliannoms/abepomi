/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

/**
 *
 * @author Julianno
 */
public class LancamentoLinhaTotalUtil {
    
    public static final String FILTRO_LOCAIS = " and l.descricao = '"+LancamentoLinhaUtil.DESCRICAO_LOCAIS+"' ";

    public LancamentoLinhaTotalUtil(String linha, Double total){
        this.linha = linha;
        this.total = total;
    }

    private String linha = null;
    private Double total = null;

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }



}
