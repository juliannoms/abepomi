/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Julianno
 */
public class AssociadoVivoRel {

    public AssociadoVivoRel(int id, String nome, String linha, Date entrega, Date devolucao) {
        this.id = id;
        this.nome = nome;
        this.linha = linha;
        this.entrega = entrega;
        this.devolucao = devolucao;
    }
    
    private int id;
    private String nome;
    private String linha;
    private Date entrega;
    private Date devolucao;

    public Date getDevolucao() {
        if(null == devolucao){
            return new GregorianCalendar(2025,01,01).getTime();
        }
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
