/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class FaturaUtilRelCusto {

    //select l.numero, f.codigo, f.inicio, f.fim, f.notafiscal, f.vencimento, sum(l.cobrado)

    private String numero;
    private int faturaCodigo;
    private Date inicio;
    private Date fim;
    private String notaFiscal;
    private Date vencimento;
    private Double total;

    public FaturaUtilRelCusto(String numero, int faturaCodigo, Date inicio, Date fim, String notaFiscal, Date vencimento, Double total) {
        this.numero = numero;
        this.faturaCodigo = faturaCodigo;
        this.inicio = inicio;
        this.fim = fim;
        this.notaFiscal = notaFiscal;
        this.vencimento = vencimento;
        this.total = total;
    }

    public int getFaturaCodigo() {
        return faturaCodigo;
    }

    public Date getFim() {
        return fim;
    }

    public Date getInicio() {
        return inicio;
    }

    public String getNotaFiscal() {
        return notaFiscal;
    }

    public String getNumero() {
        return numero;
    }

    public Double getTotal() {
        return total;
    }

    public Date getVencimento() {
        return vencimento;
    }

    

}
