/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class LctoVivoRel {

    public LctoVivoRel(Date data, Date hora, String chamado, String subSecao, String secao, Double duracao, Double valor, String destino, String origem) {
        this.data = data;
        this.hora = hora;
        this.chamado = chamado;
        this.subSecao = subSecao.toUpperCase();
        this.secao = secao.toUpperCase();
        this.duracao = duracao;
        this.valor = valor;
        this.destino = destino;
        this.origem = origem;
    }

    private Date data;
    private Date hora;
    private String chamado;
    private String subSecao;
    private String secao;
    private Double duracao;
    private Double valor;
    private String destino;
    private String origem;

    public String getChamado() {
        return chamado;
    }

    public void setChamado(String chamado) {
        this.chamado = chamado;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Double getDuracao() {
        return duracao;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getSecao() {
        return secao;
    }

    public void setSecao(String secao) {
        this.secao = secao;
    }

    public String getSubSecao() {
        return subSecao;
    }

    public void setSubSecao(String subSecao) {
        this.subSecao = subSecao;
    }

    public Double getValor() {
        if(secao.contains("Torpedo SMS".toUpperCase())&&!(secao.contains("Outros Serviços".toUpperCase()))){
            return 0.39;
        }
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    
}
