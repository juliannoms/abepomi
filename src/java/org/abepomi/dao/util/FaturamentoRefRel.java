/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.math.BigDecimal;
import java.util.Date;
import org.abepomi.model.util.EstadoDA;

/**
 *
 * @author Julianno
 */
public class FaturamentoRefRel {

    public FaturamentoRefRel(String nome, Date inclusao, EstadoDA opcao, Double servicos, Double mensalidade, Double telefonia, Double vivo, Double extra, String referencia) {
        this.nome = nome;
        this.inclusao = inclusao;
        this.opcao = opcao;
        this.servicos = servicos;
        this.mensalidade = mensalidade;
        this.telefonia = telefonia;
        this.vivo = vivo;
        this.extra = extra;
        this.referencia = referencia;
    }

    public FaturamentoRefRel(String nome, Double servicos, Double mensalidade, Double telefonia, Double vivo, Double extra, BigDecimal parcelas, String referencia) {
        this.nome = nome;
        this.inclusao = null;
        this.opcao = null;
        this.servicos = servicos;
        this.mensalidade = mensalidade;
        this.telefonia = telefonia;
        this.vivo = vivo;
        this.extra = extra;
        this.parcelas = parcelas;
        this.referencia = referencia;
    }

    private String nome;
    private Date inclusao;
    private EstadoDA opcao;
    private Double servicos;
    private Double mensalidade;
    private Double telefonia;
    private Double vivo;
    private Double extra;
    private BigDecimal parcelas;
    private String referencia;

    public Double getExtra() {
        return extra;
    }

    public void setExtra(Double extra) {
        this.extra = extra;
    }

    public Date getInclusao() {
        return inclusao;
    }

    public void setInclusao(Date inclusao) {
        this.inclusao = inclusao;
    }

    public Double getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(Double mensalidade) {
        this.mensalidade = mensalidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EstadoDA getOpcao() {
        return opcao;
    }

    public void setOpcao(EstadoDA opcao) {
        this.opcao = opcao;
    }

    public Double getServicos() {
        return servicos;
    }

    public void setServicos(Double servicos) {
        this.servicos = servicos;
    }

    public Double getTelefonia() {
        return telefonia;
    }

    public void setTelefonia(Double telefonia) {
        this.telefonia = telefonia;
    }

    public Double getVivo() {
        return vivo;
    }

    public void setVivo(Double vivo) {
        this.vivo = vivo;
    }

    public BigDecimal getParcelas() {
        return parcelas;
    }

    public void setParcelas(BigDecimal parcelas) {
        this.parcelas = parcelas;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    
}
