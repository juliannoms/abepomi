/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

/**
 *
 * @author Julianno
 */
public class AssociadoEmail {

    private int id;
    private String nome;
    private String cpf;
    private String emails;

    public AssociadoEmail(int id, String nome, String cpf, String emails) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.emails = emails;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
