/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class LancamentoLinhaUtil {

    public static final String DESCRICAO_LOCAIS = "LIGAÇÕES LOCAIS";
    public static final String DESCRICAO_TARZERO = "SERVIÇO TARIFA ZERO";
    public static final String DESCRICAO_MENSALIDADE = "MENSALIDADES E PACOTES PROMOCIONAIS";
    public static final String TIPO_PLANO_MODEM = "PLANO INTERNET";
    public static final String TIPO_GESTOR = "GESTOR ONLINE";
    public static final String TIPO_TARZERO = "SERVIÇO TARIFA ZERO :";
    public static final String TIPO_ASSINATURA = "ASSINATURA";
    public static final String TIPO_PLANO_TABLET = "BANDA LARGA 3G 1 MBPS";
    public static final int LCTO_UTILIZACAO = 0;
    public static final int LCTO_MENSALIDADES = 1;
    public static final int LCTO_UTILIZACAO_OUTROS = 3;
    public static final int LCTO_TODOS = 2;

    public LancamentoLinhaUtil() {
    }

    public LancamentoLinhaUtil(String tipo, Date data, Date hora, Double duracao, Double valor, String descricao, int fatura) {
        this.tipoLancamento = tipo;
        this.data = data;
        this.descricao = descricao;
        this.duracao = duracao;
        this.hora = hora;
        this.valor = valor;
        this.fatura = fatura;
    }

    public LancamentoLinhaUtil(String tipo, Date data, Date hora, Double duracao, Double valor, Double cobrado, String descricao, int fatura) {
        this(tipo, data, hora, duracao, valor, descricao, fatura);
        this.cobrado = cobrado;
    }

    public LancamentoLinhaUtil(String tipo, Date data, Date hora, Double duracao, Double valor, String linha, String origem, String destino) {
        this(tipo, data, hora, duracao, valor, "", 0);
        this.linha = linha;
        this.origem = origem;
        this.destino = destino;
    }
    private String tipoLancamento = null;
    private Date data = null;
    private Date hora = null;
    private Double duracao = null;
    private Double valor = null;
    private Double cobrado = null;
    private String descricao = null;
    private int fatura = 0;
    private String linha = null;
    private String origem = null;
    private String destino = null;

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }
    

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Double getCobrado() {
        return cobrado;
    }

    public void setCobrado(Double cobrado) {
        this.cobrado = cobrado;
    }

    public int getFatura() {
        return fatura;
    }

    public void setFatura(int fatura) {
        this.fatura = fatura;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getDuracao() {
        return duracao;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(String tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
