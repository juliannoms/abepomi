/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 *
 * @author Julianno
 */
public class DetalheTelefoniaUtilRel extends DetalheTelefoniaUtil {

    private Date inicio = null;
    private Date fim = null;
    private Date entrega = null;
    private Date devolucao = null;
    private String nome = "";
    private BigDecimal minuto = null;
    private String linha = null;
    private BigDecimal debitos = null;
    private BigDecimal minExc = null;
    private String descPlano = null;
    private BigDecimal minLocais = null;


    public DetalheTelefoniaUtilRel(Double plano, Double pacotes, Double outros, Double franquia, Double excedidos, Double minuto, Double minex, Double aparelho, Double hablitacao,
            Double creditos, Double debitos, Date inicio, Date fim, Date entrega, Date devolucao, String nome, String linha, String descPlano, Double minLocais, Double desconto, Double acrescimo) {
        super(plano, pacotes, outros, franquia, excedidos, aparelho, hablitacao, creditos, desconto, acrescimo);
        this.inicio = inicio;
        this.fim = fim;
        this.entrega = entrega;
        this.devolucao = devolucao;
        this.nome = nome;
        this.linha = linha;
        this.descPlano = descPlano;
        this.minLocais = minLocais == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(minLocais.toString()).setScale(2,RoundingMode.DOWN);
        this.debitos = debitos == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(debitos.toString()).setScale(2,RoundingMode.DOWN);
        this.minuto = minuto == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(minuto.toString()).setScale(2,RoundingMode.DOWN);
        this.minExc = minex == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(minex.toString()).setScale(2,RoundingMode.DOWN);
   }

    

    public Date getFimPeriodo() {
        if(!(null == devolucao) && devolucao.getTime() < fim.getTime())
            return devolucao;
        else
            return fim;
    }

    public Date getInicioPeriodo() {
        if (!(null == entrega) && entrega.getTime() > inicio.getTime()) {
            return entrega;
        } else {
            return inicio;
        }
    }

    public String getNome() {
        return nome;
    }

    public BigDecimal getDebitos() {
        return debitos;
    }


    public BigDecimal getMinuto() {
        return minuto;
    }

    public String getLinha() {
        return linha;
    }

    public BigDecimal getMinExc() {
        return minExc;
    }

    public String getDescPlano() {
        return descPlano;
    }

    public BigDecimal getMinLocais() {
        return minLocais;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public Date getFim() {
        return fim;
    }

    public Date getInicio() {
        return inicio;
    }

    

}
