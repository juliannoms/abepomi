/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Julianno
 */
public class ResumoVivoRel {

    public ResumoVivoRel(String secao, String subSecao, double duracao, double valor) {
        this.secao = secao.toUpperCase();
        this.subSecao = subSecao.toUpperCase();
        this.duracao = duracao;
        this.valor = valor;
    }
    
    private String secao;
    private String subSecao;
    private double duracao;
    private double valor;

    public double getDuracao() {
        return duracao;
    }

    public void setDuracao(double duracao) {
        this.duracao = duracao;
    }

    public String getSecao() {
        return secao;
    }

    public void setSecao(String secao) {
        this.secao = secao;
    }

    public String getSubSecao() {
        return subSecao;
    }

    public void setSubSecao(String subSecao) {
        this.subSecao = subSecao;
    }

    public double getValor() {
        //BigDecimal aux = null;
        Double doub = valor;
        if(subSecao.contains("Grupo".toUpperCase())){
            if (duracao > 2000){
                doub = (duracao - 2000) * 0.25;
                doub = new BigDecimal(doub.toString()).setScale(2, RoundingMode.DOWN).doubleValue();
            }
        }
        if(subSecao.contains("Demais".toUpperCase())){
            if (duracao > 100){
                doub = (duracao - 100) * 0.25;
                doub = new BigDecimal(doub.toString()).setScale(2, RoundingMode.DOWN).doubleValue();
            }
        }
        if(secao.contains("Torpedo SMS".toUpperCase()) &&  !(secao.contains("Outros Serviços".toUpperCase()))){
            doub = duracao * 0.39;
            doub = new BigDecimal(doub.toString()).setScale(2, RoundingMode.DOWN).doubleValue();
        }
        
        return doub;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
