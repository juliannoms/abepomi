/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class AssociadoLinhaUtil {

    public static final long DIA = 1000 * 60 * 60 * 24;

    private Integer associado = null;
    private String linha = null;
    private Date entrega = null;
    private Date devolucao = null;
    private Integer plano = null;
    private int fatura = 0;

    public AssociadoLinhaUtil(int associado, String linha, Date entrega, Date devolucao, int plano) {
        this.associado = associado;
        this.devolucao = devolucao;
        this.entrega = entrega;
        this.linha = linha;
        this.plano = plano;
    }

    public int getFatura() {
        return fatura;
    }

    public void setFatura(int fatura) {
        this.fatura = fatura;
    }
    

    public Integer getAssociado() {
        return associado;
    }

    public void setAssociado(Integer associado) {
        this.associado = associado;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getDevolucaoD1(){
        return new Date(devolucao.getTime()+DIA);
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Integer getPlano() {
        return plano;
    }

    public void setPlano(Integer plano) {
        this.plano = plano;
    }

    public Double calculaProporcao(Date inicial, Date fim){
        return calculaProporcao(inicial, fim, this.entrega, this.devolucao);
    }

    public static Double calculaProporcao(Date inicial, Date fim, Date entrega, Date devolucao){

        if(null == entrega)
            return null;
        if(null == inicial)
            return null;
        if(null == fim)
            return null;

        //fim.setTime(fim.getTime()+DIA);
        Date aux = new Date(fim.getTime()+DIA);

        if(entrega.equals(aux))
            return 0.00;

        if(entrega.after(aux))
            return 0.00;
        if(aux.before(inicial))
            return 0.00;
        if(null != devolucao && devolucao.before(inicial))
            return 0.00;
        if(null == devolucao && entrega.before(inicial))
            return 1.00;
        if(null == devolucao && entrega.after(inicial))
            return (double) (aux.getTime() - entrega.getTime()) / (aux.getTime() - inicial.getTime());

        if(!(null == devolucao)){
            devolucao.setTime(devolucao.getTime() + DIA);
            if(devolucao.equals(inicial))
                return 0.00;
        }

        if(entrega.equals(inicial) && ((null == devolucao) || devolucao.after(aux) || devolucao.equals(aux)))
            return 1.00;

        if(null != devolucao && entrega.before(inicial) && (devolucao.after(aux)||devolucao.equals(aux)))
            return 1.00;
        if(null != devolucao && entrega.after(inicial) && devolucao.after(aux))
            return (double) (aux.getTime() - entrega.getTime()) / (aux.getTime() - inicial.getTime());
        if(null != devolucao && entrega.before(inicial) && devolucao.before(aux))
            return (double) (devolucao.getTime() - inicial.getTime()) / (aux.getTime() - inicial.getTime());
        if(null != devolucao && entrega.after(inicial) && devolucao.before(aux))
            return (double) (devolucao.getTime() - entrega.getTime()) / (aux.getTime() - inicial.getTime());

        return null;
    }


}
