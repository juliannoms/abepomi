/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Julianno
 */
public class DetalheTelefoniaUtil {

    public DetalheTelefoniaUtil(Double plano, Double pacotes, Double outros, Double franquia, Double excedidos, Double aparelho,
            Double habilitacao, Double creditos, Double desconto, Double acrescimo) {
        this.plano = plano == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) : new BigDecimal(plano.toString()).setScale(2,RoundingMode.DOWN);
        this.pacotes = pacotes == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(pacotes.toString()).setScale(2,RoundingMode.DOWN);
        this.outros = outros == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(outros.toString()).setScale(2,RoundingMode.DOWN);
        this.franquia = franquia == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(franquia.toString()).setScale(2,RoundingMode.DOWN);
        this.excedidos = excedidos == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(excedidos.toString()).setScale(2,RoundingMode.DOWN);
        this.aparelho = aparelho == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(aparelho.toString()).setScale(2,RoundingMode.DOWN);
        this.habilitacao = habilitacao == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(habilitacao.toString()).setScale(2,RoundingMode.DOWN);
        this.creditos = creditos == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(creditos.toString()).setScale(2,RoundingMode.DOWN);
        this.desconto = desconto == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(desconto.toString()).setScale(2,RoundingMode.DOWN);
        this.acrescimo = acrescimo == null ? new BigDecimal("0.00").setScale(2,RoundingMode.DOWN) :new BigDecimal(acrescimo.toString()).setScale(2,RoundingMode.DOWN);
    }

    private BigDecimal plano = null;
    private BigDecimal pacotes = null;
    private BigDecimal outros = null;
    private BigDecimal franquia = null;
    private BigDecimal excedidos = null;
    private BigDecimal aparelho = null;
    private BigDecimal habilitacao = null;
    private BigDecimal creditos = null;
    private BigDecimal desconto = null;
    private BigDecimal acrescimo = null;



    public double getTotal(){
        return plano.add(pacotes).add(outros).add(franquia).add(excedidos).add(aparelho).add(habilitacao)
                .subtract(creditos).subtract(desconto).add(acrescimo).doubleValue();
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public BigDecimal getCreditos() {
        return creditos;
    }

    public BigDecimal getAparelho() {
        return aparelho;
    }

    public BigDecimal getExcedidos() {
        return excedidos;
    }

    public BigDecimal getFranquia() {
        return franquia;
    }

    public BigDecimal getHabilitacao() {
        return habilitacao;
    }

    public BigDecimal getOutros() {
        return outros;
    }

    public BigDecimal getPacotes() {
        return pacotes;
    }

    public BigDecimal getPlano() {
        return plano;
    }

    public BigDecimal getAcrescimo() {
        return acrescimo;
    }

}
