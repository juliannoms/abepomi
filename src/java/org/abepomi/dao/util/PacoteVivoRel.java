/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class PacoteVivoRel {

    public PacoteVivoRel(String descricao, Date inicio, Date fim, double valor) {
        this.descricao = descricao.toUpperCase();
        this.inicio = inicio;
        this.fim = fim;
        this.valor = valor;
    }

    private String descricao;
    private Date inicio;
    private Date fim;
    private double valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
