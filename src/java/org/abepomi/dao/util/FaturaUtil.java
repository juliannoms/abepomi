/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Julianno
 */
public class FaturaUtil {

    private Integer codigo = null;
    private Date inicio = null;
    private Date fim = null;

    public FaturaUtil(Integer codigo, Date inicio, Date fim) {
        this.codigo = codigo;
        this.inicio = inicio;
        this.fim = fim;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public boolean maiorQueInicio(Date data){
        return inicio.before(data);
    }

    public boolean maiorQueFim(Date data){
        return fim.before(data);
    }

    public static FaturaUtil getPorCodigo(List<FaturaUtil> faturas, int codigo){
        for(FaturaUtil fat : faturas){
            if(fat.getCodigo() == codigo)
                return fat;
        }
        return null;
    }

}
