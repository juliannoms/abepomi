/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.util;

import java.util.Date;

/**
 *
 * @author Julianno
 */
public class RefVivoRel {

    public RefVivoRel(String ref, int fatura, Date inicio, Date fim) {
        this.ref = ref;
        this.fatura = fatura;
        this.inicio = inicio;
        this.fim = fim;

    }

    public RefVivoRel(String ref, int fatura, Date inicio, Date fim, int id, String nome, String linha, Date entrega, Date devolucao) {
        this.ref = ref;
        this.fatura = fatura;
        this.inicio = inicio;
        this.fim = fim;
        this.id = id;
        this.nome = nome;
        this.linha = linha;
        this.entrega = entrega;
        this.devolucao = devolucao;
    }


    
    private String ref;
    private int fatura;
    private Date inicio;
    private Date fim;
    private int id;
    private String nome;
    private String linha;
    private Date entrega;
    private Date devolucao;

    public int getFatura() {
        return fatura;
    }

    public void setFatura(int fatura) {
        this.fatura = fatura;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getInicioReal(){
        /*if (entrega.getTime() > inicio.getTime()){
            return entrega;
        }*/
        return entrega;
    }

    public Date getFimReal(){
        if(null == devolucao){
            return fim;
        }else if(devolucao.getTime() < fim.getTime()){
            return devolucao;
        }
        return fim;
    }

    
}
