/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Associado;
import org.abepomi.model.Extras;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public interface ExtrasDAO extends CrudDAO<Extras>{

    public Double totalPorAssociado(Associado ass, Referencia ref);

    public int proximaId();

    public JRBeanCollectionDataSource getExtrasAssRef(Associado ass, Referencia ref);

}
