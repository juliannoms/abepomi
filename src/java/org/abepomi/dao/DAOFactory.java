/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.cobranca.MensagemDAO;
import org.abepomi.dao.cobranca.ParcelaDAO;
import org.abepomi.dao.cobranca.ParcelamentoDAO;
import org.abepomi.dao.cobranca.TituloDAO;
import org.abepomi.dao.cobranca.impl.MensagemDAOImpl;
import org.abepomi.dao.cobranca.impl.ParcelaDAOImpl;
import org.abepomi.dao.cobranca.impl.ParcelamentoDAOImpl;
import org.abepomi.dao.cobranca.impl.TituloDAOImpl;
import org.abepomi.dao.vivo.AssociadoVivoDAO;
import org.abepomi.dao.vivo.FaturaVivoDAO;
import org.abepomi.dao.vivo.LctoVivoDAO;
import org.abepomi.dao.vivo.ResumoVivoDAO;
import org.abepomi.dao.vivo.PacoteVivoDAO;
import org.abepomi.dao.impl.AparelhoDAOImpl;
import org.abepomi.dao.impl.AparelhosAssociadosDAOImpl;
import org.abepomi.dao.impl.AssDADAOImpl;
import org.abepomi.dao.impl.AssociacaoDAOImpl;
import org.abepomi.dao.impl.AssociacaoLinhaDAOImpl;
import org.abepomi.dao.impl.AssociadoDAOImpl;
import org.abepomi.dao.impl.AssociadoLinhaDAOImpl;
import org.abepomi.dao.vivo.impl.AssociadoVivoDAOImpl;
import org.abepomi.dao.impl.ContratoDAOImpl;
import org.abepomi.dao.impl.ConvDADAOImpl;
import org.abepomi.dao.impl.DependenteDAOImpl;
import org.abepomi.dao.impl.DetalheTelefoniaDAOImpl;
import org.abepomi.dao.impl.EnderecoDAOImpl;
import org.abepomi.dao.impl.ExtrasDAOImpl;
import org.abepomi.dao.impl.ExtrasTelefoniaDAOImpl;
import org.abepomi.dao.impl.FaturaDAOImpl;
import org.abepomi.dao.vivo.impl.FaturaVivoDAOImpl;
import org.abepomi.dao.impl.FaturamentoDAOImpl;
import org.abepomi.dao.impl.FechamentoTelefoniaDAOImpl;
import org.abepomi.dao.impl.FornecedorDAOImpl;
import org.abepomi.dao.impl.ItensNotaDAOImpl;
import org.abepomi.dao.impl.LancamentoLinhaDAOImpl;
import org.abepomi.dao.vivo.impl.LctoVivoDAOImpl;
import org.abepomi.dao.impl.LinhaDAOImpl;
import org.abepomi.dao.impl.MovDADAOImpl;
import org.abepomi.dao.impl.MovDAPkDAOImpl;
import org.abepomi.dao.impl.NotaFiscalDAOImpl;
import org.abepomi.dao.vivo.impl.PacoteVivoDAOImpl;
import org.abepomi.dao.impl.PaginaDAOImpl;
import org.abepomi.dao.impl.PlanoDAOImpl;
import org.abepomi.dao.impl.ReferenciaDAOImpl;
import org.abepomi.dao.impl.RemDADAOImpl;
import org.abepomi.dao.vivo.impl.ResumoVivoDAOImpl;
import org.abepomi.dao.impl.RetDADAOImpl;
import org.abepomi.dao.impl.ServicoDAOImpl;
import org.abepomi.dao.impl.UserDAOImpl;
import org.abepomi.dao.impl.VeiculoDAOImpl;
import org.abepomi.dao.vivo.impl.ContratoVivoDAOImpl;
import org.abepomi.dao.vivo.impl.DetalheVivoDAOImpl;
import org.abepomi.dao.vivo.impl.LinhaVivoDAOImpl;
import org.abepomi.dao.vivo.impl.RelatorioVivoDAOImpl;
import org.abepomi.dao.vivo.ContratoVivoDAO;
import org.abepomi.dao.vivo.DetalheVivoDAO;
import org.abepomi.dao.vivo.LinhaVivoDAO;

/**
 *
 * @author Julianno
 */
public final class DAOFactory {

    public static AssociadoDAO createAssociadoDAO(){
        return new AssociadoDAOImpl();
    }

    public static AssociacaoDAO createAssociacaoDAO(){
        return new AssociacaoDAOImpl();
    }

    public static AssDADAO createAssDADAO(){
        return new AssDADAOImpl();
    }

    public static ConvDADAO createConvDADAO(){
        return new ConvDADAOImpl();
    }

    public static DependenteDAO createDependenteDAO(){
        return new DependenteDAOImpl();
    }

    public static EnderecoDAO createEnderecoDAO(){
        return new EnderecoDAOImpl();
    }

    public static FornecedorDAO createFornecedorDAO(){
        return new FornecedorDAOImpl();
    }

    public static MovDADAO createMovDADAO(){
        return new MovDADAOImpl();
    }

    public static MovDAPkDAO createMovDAPkDAO(){
        return new MovDAPkDAOImpl();
    }

    public static RemDADAO createRemDADAO(){
        return new RemDADAOImpl();
    }

    public static ServicoDAO createServicoDAO(){
        return new ServicoDAOImpl();
    }

    public static VeiculoDAO createVeiculoDAO(){
        return new VeiculoDAOImpl();
    }

    public static RetDADAO createRetDADAO(){
        return new RetDADAOImpl();
    }

    public static UserDAO createUserDAO(){
        return new UserDAOImpl();
    }

    public static FaturamentoDAO createFaturamentoDAO(){
        return new FaturamentoDAOImpl();
    }

    public static ExtrasDAO createExtrasDAO(){
        return new ExtrasDAOImpl();
    }

    public static ReferenciaDAO createReferenciaDAO(){
        return new ReferenciaDAOImpl();
    }

    public static PaginaDAO createPaginaDAO(){
        return new PaginaDAOImpl();
    }

    public static AssociadoLinhaDAO createAssociadoLinhaDAO(){
        return new AssociadoLinhaDAOImpl();
    }

    public static ContratoDAO createContratoDAO(){
        return new ContratoDAOImpl();
    }

    public static FaturaDAO createFaturaDAO(){
        return new FaturaDAOImpl();
    }

    public static LancamentoLinhaDAO createLancamentoLinhaDAO(){
        return new LancamentoLinhaDAOImpl();
    }

    public static LinhaDAO createLinhaDAO(){
        return new LinhaDAOImpl();
    }

    public static PlanoDAO createPlanoDAO(){
        return new PlanoDAOImpl();
    }

    public static DetalheTelefoniaDAO createDetalheDAO(){
        return new DetalheTelefoniaDAOImpl();
    }

    public static FechamentoTelefoniaDAO createFechamentoTelefoniaDAO(){
        return new FechamentoTelefoniaDAOImpl();
    }

    public static ExtrasTelefoniaDAO createExtrasTelefoniaDAO(){
        return new ExtrasTelefoniaDAOImpl();
    }

    public static AssociacaoLinhaDAO createAssociacaoLinhaDAO(){
        return new AssociacaoLinhaDAOImpl();
    }

    public static AparelhoDAO createAparelhoDAO(){
        return new AparelhoDAOImpl();
    }

    public static NotaFiscalDAO createNotaFiscalDAO(){
        return new NotaFiscalDAOImpl();
    }

    public static AparelhosAssociadosDAO createApAssociadosDAO(){
        return new AparelhosAssociadosDAOImpl();
    }

    public static ItensNotaDAO createItensNotaDAO(){
        return new ItensNotaDAOImpl();
    }

    public static FaturaVivoDAO createFaturaVivoDAO(){
        return new FaturaVivoDAOImpl();
    }

    public static PacoteVivoDAO createPacoteVivoDAO(){
        return new PacoteVivoDAOImpl();
    }

    public static LctoVivoDAO createLctoVivoDAO(){
        return new LctoVivoDAOImpl();
    }

    public static ResumoVivoDAO createResumoVivoDAO(){
        return new ResumoVivoDAOImpl();
    }

    public static AssociadoVivoDAO createAssociadoVivoDAO(){
        return new AssociadoVivoDAOImpl();
    }

    public static ContratoVivoDAO createContratoVivoDAO() {
        return new ContratoVivoDAOImpl();
    }

    public static LinhaVivoDAO createLinhaVivoDAO() {
        return new LinhaVivoDAOImpl();
    }

    public static DetalheVivoDAO createDetalheVivoDAO(){
        return new DetalheVivoDAOImpl();
    }

    public static RelatorioVivoDAO createRelatorioVivoDAO(){
        return new RelatorioVivoDAOImpl();
    }

    public static TituloDAO createTituloDAO(){
        return new TituloDAOImpl();
    }

    public static MensagemDAO createMensagemDAO(){
        return new MensagemDAOImpl();
    }

    public static ParcelaDAO createParcelaDAO(){
        return new ParcelaDAOImpl();
    }

    public static ParcelamentoDAO createParcelamentoDAO(){
        return new ParcelamentoDAOImpl();
    }
}
