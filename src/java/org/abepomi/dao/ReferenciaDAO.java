/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public interface ReferenciaDAO extends CrudDAO<Referencia> {

    public List<Referencia> buscaRefSemFaturamento();

    public Referencia findByRef(String ref);

    public List<Referencia> buscaRefSemRemessa();

    public List<Referencia> buscaTodasDecrescente();

    public Referencia buscarUltima();

}
