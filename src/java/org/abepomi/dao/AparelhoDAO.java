/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Aparelho;

/**
 *
 * @author Julianno
 */
public interface AparelhoDAO extends CrudDAO<Aparelho> {

    public List<Aparelho> buscarComEstoque();

}
