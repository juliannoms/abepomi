/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.ExtrasTelefonia;

/**
 *
 * @author Julianno
 */
public interface ExtrasTelefoniaDAO extends CrudDAO<ExtrasTelefonia>{

    public List<ExtrasTelefonia> getPorLinhaSFaturar(String linha);

}
