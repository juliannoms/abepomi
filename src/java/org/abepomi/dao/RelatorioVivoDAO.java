/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.Date;
import java.util.List;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.util.RefVivoRel;

/**
 * org.abepomi.dao.DAOFactory.createRelatorioVivoRel().recuperarLocaisPorLinhaVivo($F{id},$F{linha}, $F{entrega}, $F{devolucao})
 * @author Julianno
 */
public interface RelatorioVivoDAO {

    public List<RefVivoRel> recuperarFaturaPorReferencia(Date vencimento, int ass_id);

    public List<RefVivoRel> recuperarFaturaAssReferencia(int ref_id, int ass_id);

    public JRBeanCollectionDataSource recuperarLinhasPorFaturaVivo(int fatura_id, Date inicio, Date fim);

    public JRBeanCollectionDataSource recuperarPacotesPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao);

    public JRBeanCollectionDataSource recuperarLocaisPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao);

    public JRBeanCollectionDataSource recuperarOutrosPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao);
    
    public JRBeanCollectionDataSource recuperarExtrasPorLinhaVivo(int fatura_id, String linha);

    public JRBeanCollectionDataSource recuperarLctosPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao);

}
