/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Associado;
import org.abepomi.model.cobranca.Parcelamento;

/**
 *
 * @author Julianno
 */
public interface ParcelamentoDAO extends CrudDAO<Parcelamento> {

    public List<Associado> buscarAssociadosParcelamento();

    public List<Parcelamento> buscarParcelamentosAssociados(Associado associado);

}
