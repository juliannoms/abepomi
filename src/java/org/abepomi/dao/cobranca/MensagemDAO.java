/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.cobranca.Mensagem;

/**
 *
 * @author Julianno
 */
public interface MensagemDAO extends CrudDAO<Mensagem>{

}
