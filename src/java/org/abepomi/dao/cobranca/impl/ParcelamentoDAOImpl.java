/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.cobranca.ParcelamentoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Associado;
import org.abepomi.model.cobranca.Parcelamento;

/**
 *
 * @author Julianno
 */
public class ParcelamentoDAOImpl extends CrudDAOJPA<Parcelamento> implements ParcelamentoDAO {

    public List<Associado> buscarAssociadosParcelamento() {
       EntityManager manager = EntityManagerUtil.getEntityManager();
       Query query = manager.createQuery("select distinct p.associado from Parcelamento p order by p.associado.nome");
       return query.getResultList();
    }

    public List<Parcelamento> buscarParcelamentosAssociados(Associado associado) {
       EntityManager manager = EntityManagerUtil.getEntityManager();
       Query query = manager.createQuery("select p from Parcelamento p where p.associado = :associado order by p.id");
       query.setParameter("associado", associado);
       return query.getResultList();
    }

}
