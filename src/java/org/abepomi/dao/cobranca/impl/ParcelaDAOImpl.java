/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.cobranca.ParcelaDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.cobranca.Parcela;

/**
 *
 * @author Julianno
 */
public class ParcelaDAOImpl extends CrudDAOJPA<Parcela> implements ParcelaDAO {

    @Override
    public List<Parcela> parcelasAFaturar(int ass) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Parcela.faturar");
        query.setParameter("ass", ass);
        return query.getResultList();
    }

}
