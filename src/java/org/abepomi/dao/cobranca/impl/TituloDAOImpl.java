/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.cobranca.TituloDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Associado;
import org.abepomi.model.Referencia;
import org.abepomi.model.cobranca.Titulo;

/**
 *
 * @author Julianno
 */
public class TituloDAOImpl extends CrudDAOJPA<Titulo> implements TituloDAO{

    @Override
    public Titulo buscarPorAssRef(Associado associado, Referencia referencia) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.byAssRef");
        query.setParameter("associado", associado);
        query.setParameter("referencia", referencia);
        try{
            return (Titulo) query.getSingleResult();
        }catch(NoResultException ex){
            return null;
        }
    }

    @Override
    public List<Titulo> buscaPorAssEst(Associado associado, Integer estado) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.byAssEst");
        query.setParameter("associado", associado);
        query.setParameter("estado", estado);
        return query.getResultList();
    }

    @Override
    public List<Associado> buscaAssPendente() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.AssPendente");
        return query.getResultList();
    }

    @Override
    public void atualizarTitulosNegociados(List<Titulo> titulos) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.atualizaEstado");
        for(Titulo tit : titulos){
            query.setParameter(1, (Integer)99);
            query.setParameter(2, tit.getId());
            query.executeUpdate();
        }
    }

    @Override
    public int atualizaTituloRetorno(int estado, Date pagamento, BigDecimal pago, String nossoNro) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.atualizaRetorno");
        query.setParameter("estado", estado);
        query.setParameter("pagamento", pagamento);
        query.setParameter("pago", pago);
        query.setParameter("nosso", nossoNro);
        return query.executeUpdate();
    }

    @Override
    public List<Titulo> buscarPorReferencia(Referencia referencia){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("Titulo.buscarPorReferencia");
        query.setParameter("referencia", referencia);
        return query.getResultList();
    }

}
