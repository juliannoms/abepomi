/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.cobranca.Parcela;

/**
 *
 * @author Julianno
 */
public interface ParcelaDAO extends CrudDAO<Parcela> {

    public List<Parcela> parcelasAFaturar(int ass);

}
