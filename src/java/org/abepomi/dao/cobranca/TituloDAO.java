/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.cobranca;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Associado;
import org.abepomi.model.Referencia;
import org.abepomi.model.cobranca.Titulo;

/**
 *
 * @author Julianno
 */
public interface TituloDAO extends CrudDAO<Titulo> {

    public Titulo buscarPorAssRef(Associado associado, Referencia referencia);

    public List<Titulo> buscaPorAssEst(Associado associado, Integer estado);

    public List<Associado> buscaAssPendente();

    public void atualizarTitulosNegociados(List<Titulo> titulos);

    public int atualizaTituloRetorno(int estado, Date pagamento, BigDecimal pago, String nossoNro);

    public List<Titulo> buscarPorReferencia(Referencia referencia);

}
