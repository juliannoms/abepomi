/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.FechamentoTelefonia;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public interface FechamentoTelefoniaDAO extends CrudDAO<FechamentoTelefonia> {

    public void atualizarReferencia(Referencia referencia);

    public void updateWithFlush(FechamentoTelefonia fecha);

}
