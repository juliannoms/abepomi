/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.NotaFiscal;

/**
 *
 * @author Julianno
 */
public interface NotaFiscalDAO extends CrudDAO<NotaFiscal> {

}
