/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo;

import java.util.Date;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Associado;
import org.abepomi.model.vivo.AssociadoVivo;

/**
 *
 * @author Julianno
 */
public interface AssociadoVivoDAO extends CrudDAO<AssociadoVivo>{

    public List<String> buscaLinhaPorAssId(int ass_id);

    public List<Associado> buscaAssociadoComLinha();

    public boolean atualizarDevolucao(int assId, String linha, Date devolucao);

}
