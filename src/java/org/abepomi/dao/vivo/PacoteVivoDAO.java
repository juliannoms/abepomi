/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.vivo.PacoteVivo;

/**
 *
 * @author Julianno
 */
public interface  PacoteVivoDAO extends CrudDAO<PacoteVivo> {

}
