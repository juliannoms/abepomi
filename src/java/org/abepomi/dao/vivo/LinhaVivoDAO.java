/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.vivo.LinhaVivo;

/**
 *
 * @author Julianno
 */
public interface LinhaVivoDAO extends CrudDAO<LinhaVivo> {

    public List<LinhaVivo> buscarLinhasVivoLivres();

}
