/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.vivo.FaturaVivoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.vivo.FaturaVivo;

/**
 *
 * @author Julianno
 */
public class FaturaVivoDAOImpl extends CrudDAOJPA<FaturaVivo> implements FaturaVivoDAO{

    public int processarFaturaVivo(int faturaId) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select detalha_vivo(?)");
        query.setParameter(1, faturaId);
        return (Integer)query.getSingleResult();
    }

    public List<Integer> buscarNaoFaturadas() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select f.id from faturas_vivo f left outer join detalhe_vivo d "
                + "on f.id = d.fatura_id where f.faturada = false and d.id is null order by f.id");
        return query.getResultList();
    }

    public void atualizarNaoFaturadas(int referencia){
        EntityManager manager = EntityManagerUtil.getEntityManager();
        manager.getTransaction().begin();
        Query query = manager.createNativeQuery("update faturas_vivo set faturada = true, referencia_id = :id where faturada = false");
        query.setParameter("id", referencia);
        query.executeUpdate();
        manager.getTransaction().commit();
    }

    public List<Date> buscarFaturas() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("select f.vencimento from FaturaVivo f group by f.vencimento order by f.vencimento desc");
        List<Date> datas = query.getResultList();
        if(null == datas){
            return null;
        }
        return datas;
    }
}
