/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.vivo.impl;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.vivo.ContratoVivoDAO;
import org.abepomi.model.vivo.ContratoVivo;

/**
 *
 * @author Julianno
 */
public class ContratoVivoDAOImpl extends CrudDAOJPA<ContratoVivo> implements ContratoVivoDAO {

    public ContratoVivo buscaPorNroContrato(String contrato) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("ContratoVivo.buscaPorContrato");
        query.setParameter("numero", contrato);
        try {
            return (ContratoVivo) query.getSingleResult();
        } catch (NoResultException nrex) {
            return null;
        }
    }
}
