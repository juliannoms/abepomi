/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao.vivo.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.RelatorioVivoDAO;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.util.PacoteVivoRel;
import org.abepomi.dao.util.RefVivoRel;
import org.abepomi.dao.util.ResumoVivoRel;

/**
 *
 * @author Julianno
 */
public class RelatorioVivoDAOImpl implements RelatorioVivoDAO {

    public List<RefVivoRel> recuperarFaturaPorReferencia(Date vencimento, int ass_id) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<RefVivoRel> lista = new ArrayList();
        String sql = "select a.id, a.nome, av.linha, av.entrega, av.devolucao, fv.id as fatura, min(p.inicio) as inicio, max(p.fim) as fim "
                + "from associados a, associados_vivo av, pacotes_vivo p, faturas_vivo fv "
                + "where a.id = av.ass_id and av.entrega <= fim and (av.devolucao is null or av.devolucao >= inicio) "
                + "and fv.id = p.fatura_id and fv.vencimento = :wp_vencimento and a.id = :wp_ass_id "
                + "group by a.id, a.nome, av.linha, av.entrega, av.devolucao, fv.id "
                + "order by fv.id, a.nome, av.linha";
        Query query = manager.createNativeQuery(sql);
        query.setParameter("wp_vencimento", vencimento);
        query.setParameter("wp_ass_id", ass_id);
        for (Object obj : query.getResultList()) {
                Object[] objeto = (Object[]) obj;
                lista.add(new RefVivoRel("", (Integer)objeto[5],(Date)objeto[6],(Date)objeto[7],(Integer)objeto[0],
                        String.valueOf(objeto[1]),String.valueOf(objeto[2]),(Date)objeto[3],(Date)objeto[4]));
            }
        return lista;
    }

    public List<RefVivoRel> recuperarFaturaAssReferencia(int ref_id, int ass_id) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<RefVivoRel> lista = new ArrayList();
        String sql = "select a.id, a.nome, av.linha, av.entrega, av.devolucao, fv.id as fatura, min(p.inicio) as inicio, max(p.fim) as fim "
                + "from associados a, associados_vivo av, pacotes_vivo p, faturas_vivo fv "
                + "where a.id = av.ass_id and av.entrega <= fim and (av.devolucao is null or av.devolucao >= inicio) "
                + "and fv.id = p.fatura_id and fv.referencia_id = :wp_ref and a.id = :wp_ass_id "
                + "group by a.id, a.nome, av.linha, av.entrega, av.devolucao, fv.id "
                + "order by fv.id, a.nome, av.linha";
        Query query = manager.createNativeQuery(sql);
        query.setParameter("wp_ref", ref_id);
        query.setParameter("wp_ass_id", ass_id);
        for (Object obj : query.getResultList()) {
                Object[] objeto = (Object[]) obj;
                lista.add(new RefVivoRel("", (Integer)objeto[5],(Date)objeto[6],(Date)objeto[7],(Integer)objeto[0],
                        String.valueOf(objeto[1]),String.valueOf(objeto[2]),(Date)objeto[3],(Date)objeto[4]));
            }
        return lista;
    }


    public JRBeanCollectionDataSource recuperarLinhasPorFaturaVivo(int fatura_id, Date inicio, Date fim) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
            String sql = "select NEW org.abepomi.dao.util.AssociadoVivoRel (a.id, a.nome, av.linha, av.entrega, av.devolucao) "
                    + "from Associado a, AssociadoVivo av where a.id = av.ass_id and "
                    + "av.entrega <= :wp_fim and (av.devolucao is null or av.devolucao >= :wp_inicio) "
                    + "and av.linha in (select lv.linha from LctoVivo lv where lv.fatura = :wp_fatura) "
                    + "order by a.nome, av.linha";
            Query query = manager.createQuery(sql);
            query.setParameter("wp_fatura", fatura_id);
            query.setParameter("wp_inicio", inicio);
            query.setParameter("wp_fim", fim);
            return new JRBeanCollectionDataSource(query.getResultList());
    }

    public JRBeanCollectionDataSource recuperarPacotesPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
            List<PacoteVivoRel> lista = new ArrayList();
            String sql = "select p.descricao, p.inicio, p.fim, "
                    + "proporcional_pacotes_vivo(p.inicio, p.fim, :wp_entrega, :wp_devolucao, p.valor) as proporcional  "
                    + "from pacotes_vivo p "
                    + "where p.linha = :wp_linha and p.descricao not like '%LD VIP%' "
                    + "and p.descricao not like '%INTRAGRUPO ZERO LOCAL%' and p.descricao not like '%SMS COMPARTILHADO%' "
                    + "and p.fatura_id = :wp_fatura "
                    + "UNION ALL select 'TAXA DE ADMINISTRAÇÃO', min(p.inicio), max(p.fim), "
                    + "proporcional_pacotes_vivo(min(p.inicio), max(p.fim), :wp_entrega, :wp_devolucao, 8.00) as proporcional "
                    + "from pacotes_vivo p where p.fatura_id = :wp_fatura "
                    + "UNION ALL select 'PACOTE 100 MINUTOS', min(p.inicio), max(p.fim), "
                    + "proporcional_pacotes_vivo(min(p.inicio), max(p.fim), :wp_entrega, :wp_devolucao, 24.00) as proporcional "
                    + "from pacotes_vivo p where p.fatura_id = :wp_fatura "
                    + "order by 1;";
            Query query = manager.createNativeQuery(sql);
            query.setParameter("wp_fatura", fatura_id);
            query.setParameter("wp_linha", linha);
            query.setParameter("wp_entrega", entrega);
            query.setParameter("wp_devolucao", devolucao);
            for (Object obj : query.getResultList()) {
                Object[] objeto = (Object[]) obj;
                lista.add(new PacoteVivoRel(String.valueOf(objeto[0]), (Date) objeto[1], (Date) objeto[2], (Double) objeto[3]));

            }
            return new JRBeanCollectionDataSource(lista);
    }

    public JRBeanCollectionDataSource recuperarLocaisPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<ResumoVivoRel> lista = null;
        String sql = "select NEW org.abepomi.dao.util.ResumoVivoRel( l.secao, 'Para Grupo', sum(l.duracao), sum(l.valor) ) "
                + "from LctoVivo l where l.secao like '%Ligações Locais%' and l.subSecao like '%Grupo%' and l.linha = :wp_linha "
                + "and l.data >= :wp_entrega and l.data <= :wp_devolucao and l.fatura = :wp_fatura "
                + "group by l.linha, l.secao ";
        Query query = manager.createQuery(sql);
        query.setParameter("wp_fatura", fatura_id);
        query.setParameter("wp_entrega", entrega);
        query.setParameter("wp_devolucao", devolucao);
        query.setParameter("wp_linha", linha);
        lista = query.getResultList();
        sql = "select NEW org.abepomi.dao.util.ResumoVivoRel( l.secao, 'Demais Locais', sum(l.duracao), sum(l.valor) ) "
                + "from LctoVivo l where l.secao like '%Ligações Locais%' and l.subSecao not like '%Grupo%' and l.linha = :wp_linha "
                + "and l.data >= :wp_entrega and l.data <= :wp_devolucao and l.fatura = :wp_fatura "
                + "group by l.linha, l.secao ";
        query = manager.createQuery(sql);
        query.setParameter("wp_fatura", fatura_id);
        query.setParameter("wp_entrega", entrega);
        query.setParameter("wp_devolucao", devolucao);
        query.setParameter("wp_linha", linha);
        lista.addAll(query.getResultList());
        return new JRBeanCollectionDataSource(lista);
    }

    public JRBeanCollectionDataSource recuperarOutrosPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
            String sql = "select NEW org.abepomi.dao.util.ResumoVivoRel( l.secao, l.subSecao, sum(l.duracao), sum(l.valor) ) "
                    + "from LctoVivo l where l.secao not like '%Ligações Locais%' and l.linha = :wp_linha "
                    + "and l.data >= :wp_entrega and l.data <= :wp_devolucao and l.fatura = :wp_fatura "
                    + "group by l.linha, l.secao, l.subSecao order by l.subSecao, l.secao";
            Query query = manager.createQuery(sql);
            query.setParameter("wp_fatura", fatura_id);
            query.setParameter("wp_entrega", entrega);
            query.setParameter("wp_devolucao", devolucao);
            query.setParameter("wp_linha", linha);
            return new JRBeanCollectionDataSource(query.getResultList());
    }

    public JRBeanCollectionDataSource recuperarExtrasPorLinhaVivo(int fatura_id, String linha) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        List<ResumoVivoRel> lista = new ArrayList<ResumoVivoRel>();
            String sql = "select ex.descricao, ex.tipo, ex.valor "
                    + "from extras_telefonia ex "
                    + "where ex.linha = :wp_linha and ex.fatura_vivo = :wp_fatura "
                    + "and ex.descricao not like '%SUBSTITUIDA PARA PGTO PARCIAL.%'"
                    + "order by ex.tipo, ex.descricao";
            Query query = manager.createNativeQuery(sql);
            query.setParameter("wp_fatura", fatura_id);
            query.setParameter("wp_linha", linha);
            for (Object obj : query.getResultList()) {
                Object[] objeto = (Object[]) obj;
                lista.add(new ResumoVivoRel(String.valueOf(objeto[0]), String.valueOf(objeto[1]), 0.00,(Double) objeto[2]));
            }
            return new JRBeanCollectionDataSource(lista);
    }

    public JRBeanCollectionDataSource recuperarLctosPorLinhaVivo(int fatura_id, String linha, Date entrega, Date devolucao) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        String sql = "select NEW org.abepomi.dao.util.LctoVivoRel(lv.data, lv.hora, lv.chamado, lv.subSecao, lv.secao, lv.duracao, lv.valor, lv.destino, lv.origem) "
                + "from LctoVivo lv where lv.secao not like '%Ligações Locais%' and lv.fatura = :fatura and lv.linha = :linha "
                + "and lv.data >= :inicio and lv.data <= :fim order by lv.data, lv.hora ";
        Query query = manager.createQuery(sql);
        query.setParameter("fatura", fatura_id);
        query.setParameter("linha", linha);
        query.setParameter("inicio", entrega);
        query.setParameter("fim", devolucao);
        return new JRBeanCollectionDataSource(query.getResultList());
    }
}
