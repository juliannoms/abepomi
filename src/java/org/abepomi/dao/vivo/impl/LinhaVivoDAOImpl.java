/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.vivo.LinhaVivoDAO;
import org.abepomi.model.vivo.LinhaVivo;

/**
 *
 * @author Julianno
 */
public class LinhaVivoDAOImpl extends CrudDAOJPA<LinhaVivo> implements LinhaVivoDAO {

    public List<LinhaVivo> buscarLinhasVivoLivres() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("LinhaVivo.livres");
        return query.getResultList();
    }

}
