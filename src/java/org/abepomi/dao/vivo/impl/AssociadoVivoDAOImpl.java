/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.abepomi.dao.vivo.AssociadoVivoDAO;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.model.Associado;
import org.abepomi.model.vivo.AssociadoVivo;

/**
 *
 * @author Julianno
 */
public class AssociadoVivoDAOImpl extends CrudDAOJPA<AssociadoVivo> implements AssociadoVivoDAO {

    public List<String> buscaLinhaPorAssId(int ass_id) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("AssociadoVivo.buscaLinhaPorAssociadoId");
        query.setParameter("ass_id", ass_id);
        return (List<String>)query.getResultList();
    }

    public List<Associado> buscaAssociadoComLinha() {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNamedQuery("AssociadoVivo.buscarAssComLinha");
        return (List<Associado>) query.getResultList();
    }

    public boolean atualizarDevolucao(int assId, String linha, Date devolucao) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createQuery("update AssociadoVivo av set av.devolucao = :dataDevolucao where av.ass_id = :assId and av.linha = :linha");
        query.setParameter("dataDevolucao", devolucao);
        query.setParameter("assId", assId);
        query.setParameter("linha", linha);
        int result = query.executeUpdate();
        if(result > 0){
            return true;
        }
        return false;
    }

}
