/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo.impl;

import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.abepomi.dao.commons.CrudDAOJPA;
import org.abepomi.dao.commons.EntityManagerUtil;
import org.abepomi.dao.vivo.DetalheVivoDAO;
import org.abepomi.model.vivo.DetalheVivo;

/**
 *
 * @author Julianno
 */
public class DetalheVivoDAOImpl extends CrudDAOJPA<DetalheVivo> implements DetalheVivoDAO {

    public Double totalVivoPorAssociado(int ass_id) {
        EntityManager manager = EntityManagerUtil.getEntityManager();
        Query query = manager.createNativeQuery("select sum(trunc(cast(d.acrescimo + d.administracao - d.desconto + "
                + "d.extras + d.grupo + d.local + d.mensagem + d.outros + d.pacote +d.repasse as numeric),2)) "
                + "from detalhe_vivo d, associados_vivo a, faturas_vivo f where d.linha = a.linha and a.ass_id = :ass "
                + "and d.fatura_id = f.id and f.faturada = false group by a.ass_id;");
        query.setParameter("ass", ass_id);
        try{
            return ((BigDecimal) query.getSingleResult()).doubleValue();
        }catch(NoResultException noex){}
        return 0.00;
    }

}
