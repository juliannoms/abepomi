/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao.vivo;

import java.util.Date;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.util.FaturaUtil;
import org.abepomi.model.vivo.FaturaVivo;

/**
 *
 * @author Julianno
 */
public interface FaturaVivoDAO extends CrudDAO<FaturaVivo> {

    public int processarFaturaVivo(int faturaId);

    public List<Integer> buscarNaoFaturadas();

    public void atualizarNaoFaturadas(int referencia);

    public List<Date> buscarFaturas();

}
