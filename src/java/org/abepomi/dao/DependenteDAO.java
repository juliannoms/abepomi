/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Dependente;

/**
 *
 * @author Julianno
 */
public interface DependenteDAO extends CrudDAO<Dependente>{

}
