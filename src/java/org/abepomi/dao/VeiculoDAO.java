/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Veiculo;

/**
 *
 * @author Julianno
 */
public interface VeiculoDAO extends CrudDAO<Veiculo>{

    public void delete(Veiculo veiculo) throws DAOException;

    public List<Veiculo> listaPorAssociado() throws DAOException;

}
