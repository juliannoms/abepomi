/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Pagina;

/**
 *
 * @author Julianno
 */
public interface PaginaDAO extends CrudDAO<Pagina> {

    public List<Pagina> getPaginasSeguras();

    public List<Pagina> getPaginasLiberadas();
}
