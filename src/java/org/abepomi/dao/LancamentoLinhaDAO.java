/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.dao;

import java.util.Date;
import java.util.List;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.util.LancamentoLinhaTotalUtil;
import org.abepomi.dao.util.LancamentoLinhaUtil;
import org.abepomi.model.LancamentoLinha;

/**
 *
 * @author Julianno
 */
public interface LancamentoLinhaDAO extends CrudDAO<LancamentoLinha> {

    public List<LancamentoLinhaUtil> getLctosPorLinha(String linha, int tipo);

    public List<LancamentoLinhaUtil> getLctosPorInicioTipo(Date data, int tipo);

    public Double valorGastoPorPeriodo(Date inicio, Date fim, String linha, int fatura);

    public LancamentoLinhaTotalUtil getUtilizacaoPorFiltro(String linha, String filtro, int fatura, Date inicio, Date fim);

    public JRBeanCollectionDataSource getLancUtilOutros(String linha, Date inicio, Date fim, Date iniFatura);

    public JRBeanCollectionDataSource getLancUtilMensalidades(String linha, Date inicio, Date fim, Date iniFatura);

}
