/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.AssociadoEmail;
import org.abepomi.model.Associado;

/**
 *
 * @author Julianno
 */
public interface AssociadoDAO extends CrudDAO<Associado>{

    public List<Associado> buscaPorNome(String nome) throws DAOException;

    public List<Associado> buscaPorCpf(String cpf) throws DAOException;

    public List<Associado> findAllByNome() throws DAOException;

    public List<AssociadoEmail> buscaAssociadoEmail();

}
