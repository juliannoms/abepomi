/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.Date;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.util.DetalheTelefoniaUtil;
import org.abepomi.dao.util.DetalheTelefoniaUtilRel;
import org.abepomi.model.DetalheTelefonia;

/**
 *
 * @author Julianno
 */
public interface DetalheTelefoniaDAO extends CrudDAO<DetalheTelefonia> {

    public DetalheTelefoniaUtil getTotalPorReferenciaAssociado(int referencia, int associado);

    public List<DetalheTelefoniaUtilRel> getTotaisLinhaAssReferencia(int ref_id, int ass_id);

    public List<DetalheTelefoniaUtilRel> getTotaisLinhaPorReferencia(Date inicio, int ass_id);

    public List<DetalheTelefonia> getDetalhesSFaturar(int associado);
}
