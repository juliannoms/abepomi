/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.AssociacaoLinha;

/**
 *
 * @author Julianno
 */
public interface AssociacaoLinhaDAO extends CrudDAO<AssociacaoLinha>{

    public List<AssociacaoLinha> linhasUsoAssociacao();


}
