/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.Endereco;

/**
 *
 * @author Julianno
 */
public interface EnderecoDAO extends CrudDAO<Endereco>{

}
