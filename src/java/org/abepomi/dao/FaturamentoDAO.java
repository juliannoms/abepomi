/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.dao.util.FaturamentoRefRel;
import org.abepomi.model.Associado;
import org.abepomi.model.Faturamento;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public interface FaturamentoDAO extends CrudDAO<Faturamento> {

    public Faturamento buscaFatPorAssRef(Associado ass, Referencia ref);

    public List<FaturamentoRefRel> receitaPorReferencia(Referencia referencia);

    public List<FaturamentoRefRel> receitaPorReferencia();

    public List<FaturamentoRefRel> faturadosSemDebito(Referencia ref);

    public List<Faturamento> buscaCobrancaRef(Referencia ref);

    public Boolean isAssociadoCobravelBoleto(int ass, int ref);

    public List<Faturamento> faturamentoAssociadoBoleto(Referencia referencia);

    public List<Faturamento> faturamentoAssociadoNDebitado(Referencia referencia);
}
