/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.dao;

import java.math.BigDecimal;
import java.util.List;
import org.abepomi.dao.commons.CrudDAO;
import org.abepomi.model.MovDA;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public interface MovDADAO extends CrudDAO<MovDA>{

    public void atualizarStatusDA(String status, String associado, BigDecimal valor);

    public MovDA buscaPorAssVencimento(String ass, Double valor);

    public List<MovDA> buscaPorMesRef(Referencia referencia);

    public List<MovDA> buscaDebitados(Referencia referencia);

    public List<MovDA> buscaNDebitados(Referencia referencia);

}
