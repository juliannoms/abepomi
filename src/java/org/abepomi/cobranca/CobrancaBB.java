/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.cobranca;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;

import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Associacao;
import org.abepomi.model.Faturamento;
import org.abepomi.model.util.Validacao;
import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;

/**
 *
 * @author Julianno
 */
public class CobrancaBB {

    public static byte[] gerarCobrancaTitulo(org.abepomi.model.cobranca.Titulo titulo){
        if(null == titulo){
            return null;
        }
        List<org.abepomi.model.cobranca.Titulo> titulos = new ArrayList<org.abepomi.model.cobranca.Titulo>();
        titulos.add(titulo);
        return gerarCobrancaTitulos(titulos);
    }

    public static byte[] gerarCobrancaTitulos(List<org.abepomi.model.cobranca.Titulo> lista){
        if (lista == null || lista.isEmpty()) {
            return null;
        }

        Associacao aso = null;
        try {
            aso = DAOFactory.createAssociacaoDAO().findById(1);
        } catch (DAOException ex) {
            Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, "Erro ao retornar Associação...", ex);
            return null;
        }

        List<Boleto> boletos = new ArrayList();
        Cedente cedente = new Cedente(aso.getNome(), aso.getCnpj());

        for (org.abepomi.model.cobranca.Titulo t : lista) {

            //Date vencimento = new GregorianCalendar(Integer.parseInt(f.getReferencia().getRef().substring(2,6)), Integer.parseInt(f.getReferencia().getRef().substring(0,2))-1, 10).getTime();

            Sacado sacado = new Sacado(t.getAssociado().getNome(),t.getAssociado().getCpf());

            org.jrimum.domkee.comum.pessoa.endereco.Endereco enderecoSac = new org.jrimum.domkee.comum.pessoa.endereco.Endereco();
            enderecoSac.setBairro(t.getAssociado().getEndereco().getBairro());
            enderecoSac.setLogradouro(t.getAssociado().getEndereco().getLogradouro());
            enderecoSac.setCep(t.getAssociado().getEndereco().getCep());
            enderecoSac.setLocalidade(t.getAssociado().getEndereco().getCidade() + "/" + t.getAssociado().getEndereco().getUf().name());

            sacado.addEndereco(enderecoSac);

            ContaBancaria conta = new ContaBancaria(BancosSuportados.BANCO_DO_BRASIL.create());
            conta.setAgencia(new org.jrimum.domkee.financeiro.banco.febraban.Agencia(7037,"8"));
            conta.setNumeroDaConta(new NumeroDaConta(2100597));
            conta.setCarteira(new Carteira(18));

            Titulo titulo = new Titulo(conta, sacado, cedente);
            titulo.setNossoNumero("2100597" + t.getReferencia().getRef().substring(0, 2) + t.getReferencia().getRef().substring(4, 6) + "0" + Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(t.getAssociado().getId()), 5));
            titulo.setValor(t.getValor());
            titulo.setDataDoDocumento(Calendar.getInstance().getTime());
            //titulo.setDataDoVencimento(new Date(Calendar.getInstance().getTime().getTime() + 1000 * 60 * 60 * 24 * 2));
            titulo.setDataDoVencimento(t.getVencimento());
            titulo.setTipoDeDocumento(TipoDeTitulo.RC_RECIBO);
            titulo.setAceite(Aceite.N);
            titulo.setNumeroDoDocumento(t.getSeuNro());


            Boleto boleto = new Boleto(titulo);

            boleto.setLocalPagamento("Pagável em qualquer banco até o vencimento.".toUpperCase());
            boleto.setInstrucaoAoSacado("");

            int i = 1;
            while(i<=8 && i<= t.getMensagens().size()){
                try {
                    Method m = boleto.getClass().getMethod("setInstrucao" + i, String.class);
                    try {
                        m.invoke(boleto, t.getMensagens().get(i-1).getTexto());
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, null, ex);
                }
                i++;

            }

            titulo.getContaBancaria().setNumeroDaConta(new NumeroDaConta(408, "1"));

            boletos.add(boleto);

            Logger.getLogger(CobrancaBB.class.getName()).log(Level.INFO, "Boleto n\u00ba {0} foi emitido.", titulo.getNossoNumero());
        }

        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") ;

        return BoletoViewer.groupInOnePdfWithTemplate(boletos,  path+"/resources/boleto_template.pdf");
    }

    public static byte[] gerarCobranca(List<Faturamento> lista) {

        if (lista == null) {
            return null;
        }

        Associacao aso = null;
        try {
            aso = DAOFactory.createAssociacaoDAO().findById(1);
        } catch (DAOException ex) {
            Logger.getLogger(CobrancaBB.class.getName()).log(Level.SEVERE, "Erro ao retornar Associação...", ex);
            return null;
        }

        List<Boleto> boletos = new ArrayList();
        Cedente cedente = new Cedente(aso.getNome(), aso.getCnpj());

        Date vencimento = new Date(new GregorianCalendar(2012, 5, 15).getTimeInMillis());

        for (Faturamento f : lista) {

            //Date vencimento = new GregorianCalendar(Integer.parseInt(f.getReferencia().getRef().substring(2,6)), Integer.parseInt(f.getReferencia().getRef().substring(0,2))-1, 10).getTime();

            Sacado sacado = new Sacado(f.getAssociado().getNome(),f.getAssociado().getCpf());

            org.jrimum.domkee.comum.pessoa.endereco.Endereco enderecoSac = new org.jrimum.domkee.comum.pessoa.endereco.Endereco();
            enderecoSac.setBairro(f.getAssociado().getEndereco().getBairro());
            enderecoSac.setLogradouro(f.getAssociado().getEndereco().getLogradouro());
            enderecoSac.setCep(f.getAssociado().getEndereco().getCep());
            enderecoSac.setLocalidade(f.getAssociado().getEndereco().getCidade() + "/" + f.getAssociado().getEndereco().getUf().name());

            sacado.addEndereco(enderecoSac);

            ContaBancaria conta = new ContaBancaria(BancosSuportados.BANCO_DO_BRASIL.create());
            conta.setAgencia(new org.jrimum.domkee.financeiro.banco.febraban.Agencia(7037,"8"));
            conta.setNumeroDaConta(new NumeroDaConta(2100597));
            conta.setCarteira(new Carteira(18));

            Titulo titulo = new Titulo(conta, sacado, cedente);
            titulo.setNossoNumero("2100597" + f.getReferencia().getRef().substring(0, 2) + f.getReferencia().getRef().substring(4, 6) + "0" + Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(f.getAssociado().getId()), 5));
            titulo.setValor(BigDecimal.valueOf(f.getExtra() + f.getMensalidade() + f.getServicos() + f.getTelefonia() + f.getVivo()));
            titulo.setDataDoDocumento(Calendar.getInstance().getTime());
            //titulo.setDataDoVencimento(new Date(Calendar.getInstance().getTime().getTime() + 1000 * 60 * 60 * 24 * 2));
            titulo.setDataDoVencimento(vencimento);
            titulo.setTipoDeDocumento(TipoDeTitulo.RC_RECIBO);
            titulo.setAceite(Aceite.N);
            titulo.setNumeroDoDocumento("Ref. " + f.getReferencia().getRef());


            Boleto boleto = new Boleto(titulo);
            boleto.setLocalPagamento("Pagável em qualquer banco até o vencimento.".toUpperCase());
            boleto.setInstrucaoAoSacado("");
            boleto.setInstrucao1("NÃO RECEBER APÓS 25/06/2012.");

            titulo.getContaBancaria().setNumeroDaConta(new NumeroDaConta(408, "1"));
            
            boletos.add(boleto);

            Logger.getLogger(CobrancaBB.class.getName()).log(Level.INFO, "Boleto n\u00ba {0} foi emitido.", titulo.getNossoNumero());
        }

        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") ;

        return BoletoViewer.groupInOnePdfWithTemplate(boletos,  path+"/resources/boleto_template.pdf");

    }
}
