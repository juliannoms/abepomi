/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.filters;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.abepomi.dao.commons.EntityManagerUtil;

/**
 *
 * @author Julianno
 */
public class DAOInterceptor implements Filter {

    private static EntityManagerFactory emf = null;

    /*public boolean isLoggable(LogRecord record) {
    throw new UnsupportedOperationException("Not supported yet.");
    }*/
    public void init(FilterConfig filterConfig) throws ServletException {

        emf = Persistence.createEntityManagerFactory("ABEPOMIPU");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain){

        EntityManager em = null;
        try {
            try {
                em = emf.createEntityManager();
                EntityManagerUtil.ENTITY_MANAGERS.set(em);
                //em.setFlushMode(FlushModeType.COMMIT);
                //em.getTransaction().begin();
                chain.doFilter(request, response);
                //em.getTransaction().commit();
            } catch (Exception e) {
                //em.getTransaction().rollback();
            }
        } finally {
            EntityManagerUtil.ENTITY_MANAGERS.remove();
            if (em != null) {
                em.close();
            }

        }
    }

    public void destroy() {
        if (emf != null) {
            emf.close();
        }
    }
    
}
