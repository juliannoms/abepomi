package org.abepomi.exception;

public class RetornoCobrancaInvalidoException extends RuntimeException {

    public RetornoCobrancaInvalidoException(String msg) {
        super(msg);
    }
}
