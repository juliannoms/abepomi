/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.arqs;

import java.util.List;
import org.abepomi.model.Associado;
import org.abepomi.model.util.Status;

/**
 *
 * @author Julianno
 */
public class ArqEmails {

    public static byte[] exportarCSV(List<Associado> associados) {
        StringBuilder arquivo = new StringBuilder();

        arquivo.append("Nome,Email Primário,Email Secundário");
        arquivo.append("\n");

        for (Associado ass : associados) {
            if (ass.getStatus()==Status.A && (!(ass.getEmail1().equals("") && ass.getEmail2().equals("")))) {
                arquivo.append(ass.getNome());
                arquivo.append(",");
                arquivo.append(ass.getEmail1());
                arquivo.append(",");
                arquivo.append(ass.getEmail2());
                arquivo.append("\n");
            }
        }

        return arquivo.toString().getBytes();
    }
}
