/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.arqs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import org.abepomi.arqs.util.RemoverAcentos;
import org.abepomi.model.MovDA;
import org.abepomi.model.RemDA;
import org.abepomi.model.util.Validacao;

/**
 *
 * @author Julianno
 */
public class ArqRemDA {

    private static String criaHeader(RemDA remessa) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        StringBuilder linha = new StringBuilder();
        linha.append("A1");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, remessa.getConvenio().getConvenio(), 20));
        linha.append(RemoverAcentos.remover(Validacao.validaTamanho(Validacao.ALFA, remessa.getConvenio().getAssociacao().getNome(), 20)));
        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, remessa.getConvenio().getBancoCod(), 3));
        linha.append(RemoverAcentos.remover(Validacao.validaTamanho(Validacao.ALFA, remessa.getConvenio().getBancoNome().toUpperCase(), 20)));
        linha.append(sdf.format(remessa.getDtGeracao()));
        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(remessa.getNsa()), 6));
        linha.append("04");
        linha.append("DEBITO AUTOMATICO");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, "", 52));
        linha.append("\n");
        return linha.toString();
    }

    private static String getTrailler(int registros, Long total) {
        StringBuilder linha = new StringBuilder();
        linha.append("Z");
        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(registros), 6));
        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(total), 17));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, "", 126));
        linha.append("\n");
        return linha.toString();
    }

    public static byte[] gerarRemessaI(RemDA remessa) {
        StringBuilder arquivo = new StringBuilder();
        int registros = 2;
        arquivo.append(criaHeader(remessa));
        for (MovDA mov : remessa.getDebitos()) {
            arquivo.append(criaRegistroI(mov));
            registros++;
        }
        arquivo.append(getTrailler(registros, 0L));
        return arquivo.toString().getBytes();
    }

    private static String criaRegistroI(MovDA mov) {
        StringBuilder linha = new StringBuilder();
        linha.append("I");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getIdenDA(), 25));
        linha.append("2");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getAssociado().getCpf(), 14));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getAssociado().getNome(), 40));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, "PRESIDENTE PRUDENTE", 30));
        linha.append("SP");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, "", 37));
        linha.append("\n");
        return linha.toString();
    }

    public static byte[] gerarRemessa(RemDA remessa) {
        StringBuilder linha = new StringBuilder();
        int registros = 2;
        Long total = 0L;

        //header
        linha.append(criaHeader(remessa));

        //lançamentos
        for (MovDA mov : remessa.getDebitos()) {
            linha.append(criaRegistroE(mov));

            String valor = String.valueOf(new BigDecimal(mov.getValor().toString()).setScale(2, RoundingMode.DOWN)).replace(".", "");
            total = total + Long.parseLong(valor);

            registros++;
        }

        //trailler
        linha.append(getTrailler(registros, total));

        return linha.toString().getBytes();
    }

    private static String criaRegistroE(MovDA mov) {

       if(mov.getAssDA().getAssociado().getId() == 89){
           System.out.println("Chegou no que eu queria!!!");
       }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        StringBuilder linha = new StringBuilder();
        linha.append("E");
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getIdenDA(), 25));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getAgencia(), 4));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, mov.getAssDA().getConta(), 14));
        linha.append(sdf.format(mov.getDtVencimento()));
        String valor = String.valueOf(new BigDecimal(mov.getValor().toString()).setScale(2, RoundingMode.DOWN)).replace(".", "");
        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, valor, 15));
        linha.append("03");
//        linha.append(Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(mov.getRemDA().getNsa()), 10));
//        linha.append(Validacao.validaTamanho(Validacao.ALFA, "", 70));
        linha.append(Validacao.validaTamanho(Validacao.ALFA, "", 80));
        linha.append("0");
        linha.append("\n");
        return linha.toString();
    }
}
