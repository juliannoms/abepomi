package org.abepomi.arqs.cobranca;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.abepomi.exception.RetornoCobrancaInvalidoException;
import org.abepomi.model.cobranca.Header;
import org.abepomi.model.cobranca.Registro;
import org.abepomi.model.util.Validacao;

public class Retorno {

    private final static int HEADER_ARQUIVO = 0;
    private final static int HEADER_LOTE = 1;
    private final static int REGISTRO = 3;
    private final static int TRAILER_LOTE = 5;
    private final static int TRAILER_ARQUIVO = 9;
    private final static String REGISTRO_T = "T";
    private final static String REGISTRO_U = "U";

    public Retorno() {
    }

    public static Header processar(String nomeArquivo) throws IOException, RetornoCobrancaInvalidoException, FileNotFoundException {

        FileReader     reader  = null;
        BufferedReader arquivo = null;
        int qtdeReg = 0;
        Integer posArq = HEADER_ARQUIVO;
        
        try {
            reader = new FileReader(nomeArquivo);
            arquivo = new BufferedReader(reader);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("Arquivo não encontrado!");
        }
        Header header = null;
        if (arquivo.ready()) {
            String linha = arquivo.readLine();
            if (linha.length() != 240) {
                throw new RetornoCobrancaInvalidoException("Tamanho de linha inválido!");
            }
            Long bco = null;
            Long ins = null;
            Long cvn = null;
            Long age = null;
            Long cor = null;
            Date ger = null;
            Long seq = null;
            Integer reg = null;
            try {
                bco = Long.parseLong(linha.substring(0, 3));
                reg = Integer.parseInt(linha.substring(7, 8));
                ins = Long.parseLong(linha.substring(18, 32));
                cvn = Long.parseLong(linha.substring(32, 41));
                age = Long.parseLong(linha.substring(52, 57));
                cor = Long.parseLong(linha.substring(58, 70));
                ger = new SimpleDateFormat("ddMMyyyy").parse(linha.substring(143, 151));
                seq = Long.parseLong(linha.substring(157, 163));
            } catch (NumberFormatException nex) {
                throw new RetornoCobrancaInvalidoException("Campo inválido!" + nex.getMessage());
            } catch (ParseException pex) {
                throw new RetornoCobrancaInvalidoException("Campo data inválido!" + pex.getMessage());
            }
            if (reg != posArq) {
                throw new RetornoCobrancaInvalidoException("Arquivo sem Header!");
            }
            qtdeReg++;
            posArq = HEADER_LOTE;
            header = new Header(bco, ins, cvn, age, cor, ger, seq, 0, null);
            linha = arquivo.readLine();
            reg = Integer.parseInt(linha.substring(7, 8));
            if (reg != posArq) {
                throw new RetornoCobrancaInvalidoException("Arquivo sem Lote!");
            }
            qtdeReg++;
            linha = arquivo.readLine();
            reg = Integer.parseInt(linha.substring(7, 8));
            qtdeReg++;
            while (reg == REGISTRO) {
                Integer codMov = null;
                String nossoNro = null;
                Date dtVencimento = null;
                Date dtMovimento = null;
                BigDecimal valor = null;
                BigDecimal valorPago = null;
                String tpReg = linha.substring(13, 14);
                if (!tpReg.equals(REGISTRO_T)) {
                    throw new RetornoCobrancaInvalidoException("Registro sem 'T'");
                }
                try {
                    codMov = Integer.parseInt(linha.substring(15, 17));
                    nossoNro = linha.substring(37, 57).trim();
                    nossoNro = Validacao.validaTamanho(Validacao.NUMERICO, nossoNro, 20);
                    dtVencimento = new SimpleDateFormat("ddMMyyyy").parse(linha.substring(73, 81));
                    valor = new BigDecimal(linha.substring(81, 94) + "." + linha.substring(94, 96));
                } catch (NumberFormatException nex) {
                    throw new RetornoCobrancaInvalidoException("Campo de registro T inválido!" + nex.getMessage());
                } catch (ParseException pex) {
                    throw new RetornoCobrancaInvalidoException("Campo data de vencimento inválido!" + pex.getMessage());
                }
                linha = arquivo.readLine();
                reg = Integer.parseInt(linha.substring(7, 8));
                qtdeReg++;
                tpReg = linha.substring(13, 14);
                if (!tpReg.equals(REGISTRO_U)) {
                    throw new RetornoCobrancaInvalidoException("Registro sem 'U'");
                }
                try {
                    valorPago = new BigDecimal(linha.substring(77, 90) + "." + linha.substring(90, 92));
                    dtMovimento = new SimpleDateFormat("ddMMyyyy").parse(linha.substring(137, 145));
                } catch (NumberFormatException nex) {
                    throw new RetornoCobrancaInvalidoException("Campo de registro U inválido!" + nex.getMessage());
                } catch (ParseException pex) {
                    throw new RetornoCobrancaInvalidoException("Campo data de movimento inválido!" + pex.getMessage());
                }
                header.adicionaRegistro(new Registro(codMov, nossoNro, dtVencimento, dtMovimento, valor, valorPago));
                linha = arquivo.readLine();
                reg = Integer.parseInt(linha.substring(7, 8));
                qtdeReg++;
            }
            posArq = TRAILER_LOTE;
            if (posArq != reg) {
                throw new RetornoCobrancaInvalidoException("Arquivo sem Trailer de Lote!");
            }
            posArq = TRAILER_ARQUIVO;
            linha = arquivo.readLine();
            reg = Integer.parseInt(linha.substring(7, 8));
            qtdeReg++;
            if (posArq != reg) {
                throw new RetornoCobrancaInvalidoException("Arquivo sem Trailer de Arquivo!");
            }
            int qtdeTotal = Integer.parseInt(linha.substring(23, 29));
            if (qtdeTotal != qtdeReg) {
                throw new RetornoCobrancaInvalidoException("Arquivo corrompido. Qtde de registros inválida!");
            }
        }
        if(!(null==arquivo)){
            arquivo.close();
            reader.close();
            arquivo = null;
            reader = null;
        }
        return header;
    }
}
