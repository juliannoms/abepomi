/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.arqs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julianno
 */
public class TratamentoArquivo {

    //private static final int BUFFER = 4096;
    private RandomAccessFile raf = null;
    private int tamanhoLinha = 0;

    public TratamentoArquivo(File file, int tamLinha) {
        try {
            raf = new RandomAccessFile(file, "r");
            this.tamanhoLinha = tamLinha;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TratamentoArquivo.class.getName()).log(Level.SEVERE, "Erro!", ex);
        }
    }

    public RandomAccessFile getRaf() {
        return raf;
    }

    public void close() {
        try {
            raf.close();
        } catch (IOException ex) {
            Logger.getLogger(TratamentoArquivo.class.getName()).log(Level.SEVERE, "Erro!", ex);
        }
    }

    private Long getTamanho() {
        Long retorno = 0L;
        if (!isNull()) {
            try {
                retorno = raf.length();
            } catch (IOException ex) {
                Logger.getLogger(TratamentoArquivo.class.getName()).log(Level.SEVERE, "Erro!", ex);
            }
        }
        return retorno;
    }

    public int getQtdeLinhas() {
        return (int) (getTamanho() / tamanhoLinha);
    }

    private boolean isNull() {
        return raf == null;
    }

    public String getLinha(int numero) {
        if (!isNull()) {
            try {
                raf.seek((numero * tamanhoLinha) - tamanhoLinha);
                return raf.readLine();
            } catch (IOException ioEx) {
                Logger.getLogger(TratamentoArquivo.class.getName()).log(Level.SEVERE, "Erro!", ioEx);
            }
        }
        return "";
    }

    public String getLinha() {
        if (!isNull()) {
            try {
                return raf.readLine();
            } catch (IOException ioEx) {
                Logger.getLogger(TratamentoArquivo.class.getName()).log(Level.SEVERE, "Erro!", ioEx);
            }
        }
        return "";
    }

    public static boolean isRetDebAutValido(File file) {
        boolean retorno = false;
        FileReader reader = null;
        BufferedReader buffer = null;
        try {
            reader = new FileReader(file);
            buffer = new BufferedReader(reader);
            if (buffer.ready()) {
                String linha = null;
                int linhas = 1;
                linha = buffer.readLine();
                if (linha.startsWith("A2")) {
                    while (buffer.ready()) {
                        linha = buffer.readLine();
                        linhas++;
                    }
                    String qtde = linha.substring(1, 7);
                    if (linha.startsWith("Z") && Integer.parseInt(qtde) == linhas) {
                        retorno = true;
                    }
                }
            }
        } catch (Exception ioex) {}
        finally{
            try {
                buffer.close();
            } catch (IOException ex) {}
            try {
                reader.close();
            } catch (IOException ex) {}
            
        }

        return retorno;
    }
}
