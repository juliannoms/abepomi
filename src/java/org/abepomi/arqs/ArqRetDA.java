/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.arqs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.abepomi.dao.AssDADAO;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.exception.RetornoDebitoInvalidoException;
import org.abepomi.mb.AssociacaoMB;
import org.abepomi.model.AssDA;
import org.abepomi.model.ConvDA;
import org.abepomi.model.RetDA;
import org.abepomi.model.util.EstadoDA;

/**
 *
 * @author Julianno
 */
public class ArqRetDA {

    public static void processarArqRetornoDA(String nomeArquivo) throws DAOException, RetornoDebitoInvalidoException, IOException, FileNotFoundException {

        File file = new File(nomeArquivo);
        FileReader reader = null;
        try {
            reader = new FileReader(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao ler arquivo retorno!", ex);
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Arquivo não encontrado!", null));
//            return "falha";
            throw new FileNotFoundException("Arquivo não encontrado!");
        }
        BufferedReader arqDA = new BufferedReader(reader);
        //TratamentoArquivo ta = new TratamentoArquivo(file, 151);

        if (TratamentoArquivo.isRetDebAutValido(file)) {
            //String linha = ta.getLinha(1);

            String linha = arqDA.readLine();

            Integer nsa = null;
            nsa = DAOFactory.createRetDADAO().getMaxNsa();
            if (nsa == null) {
                nsa = 0;
            }
            ConvDA cda = null;
            cda = DAOFactory.createConvDADAO().findById(1);
            if (cda == null) {
                //ta.close();
                arqDA.close();
                reader.close();
                file = null;
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Convênio não cadastrado!", null));
//                return "falha";
                throw new RetornoDebitoInvalidoException("Convênio não cadastrado!");
            }

            if (cda.getConvenio().equals(linha.substring(2, 22).trim())) {
                if (Integer.parseInt(linha.substring(73, 79)) == nsa + 1) {
                    RetDA ret = new RetDA();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    ret.setNsa(nsa + 1);
                    ret.setConvenio(cda);
                    try {
                        ret.setGeracao(sdf.parse(linha.substring(65, 73)));
                    } catch (ParseException ex) {
                        Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao ler arquivo retorno!", ex);
                        throw new RetornoDebitoInvalidoException("Data de geração do arquivo inválida!");
                    }
                    DAOFactory.createRetDADAO().addEntity(ret);
                    AssDADAO assdao = DAOFactory.createAssDADAO();
                    //for (int i = 1; i < ta.getQtdeLinhas(); i++) {
                    int contador = 1;
                    while (arqDA.ready()) {
                        //linha = ta.getLinha();
                        linha = arqDA.readLine();
                        contador++;
                        if (linha.substring(0, 1).equals("B")) {
                            AssDA ass = assdao.findById(linha.substring(1, 26).trim());
                            if (ass != null) {
                                try {
                                    ass.setDtOpcao(sdf.parse(linha.substring(44, 52)));
                                } catch (ParseException ex) {
                                    Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao ler arquivo retorno!", ex);
                                    throw new RetornoDebitoInvalidoException("Data de opção do registro 'B' inválida! Linha arquivo: " + contador);
                                }
                                if (linha.endsWith("2")) {
                                    ass.setOpcao(EstadoDA.C);
                                    ass.setAgencia(linha.substring(26, 30));
                                    ass.setConta(linha.substring(30, 44).trim());
                                } else {
                                    ass.setOpcao(EstadoDA.X);
                                }
                                assdao.updateEntity(ass);
                            }
                        } else if (linha.substring(0, 1).equals("F")) {
                            String status = linha.substring(67, 69);
                            String ass = linha.substring(1, 26).trim();
                            String valor = (linha.substring(52, 65) + "." + linha.substring(65, 67));
                            DAOFactory.createMovDADAO().atualizarStatusDA(status, ass, new BigDecimal(valor));

                        } else if (linha.substring(0, 1).equals("Z")) {
                            int total = Integer.parseInt(linha.substring(1, 7));
                            if (total != contador) {
                                throw new RetornoDebitoInvalidoException("Quantidade de registros inválida!");
                            }
                        }
                    }

                    ret.setValor(Double.parseDouble(linha.substring(7, 22) + "." + linha.substring(22, 24)));
                    DAOFactory.createRetDADAO().updateEntity(ret);
                } else {
                    //ta.close();
                    arqDA.close();
                    reader.close();
                    file = null;

//                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "O número sequencial do arquivo não é válido!", null));
//                    return "falha";
                    throw new RetornoDebitoInvalidoException("O número sequencial do arquivo é inválido!");
                }
            } else {
                //ta.close();
                arqDA.close();
                reader.close();
                file = null;
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Arquivo não é retorno do convênio cadastrado!", null));
//                return "falha";
                throw new RetornoDebitoInvalidoException("Arquivo não é retorno do convênio cadastrado!");
            }
        } else {
            //ta.close();
            arqDA.close();
            reader.close();
            file = null;
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Arquivo não é retorno de Débito Automático Válido!", null));
//            return "falha";
            throw new RetornoDebitoInvalidoException("Arquivo não é retorno de Débito Automático Válido!");
        }
        //ta.close();
        arqDA.close();
        reader.close();
        file = null;
    }
}
