/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.arqs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.vivo.FaturaVivoDAO;
import org.abepomi.dao.vivo.LctoVivoDAO;
import org.abepomi.dao.vivo.PacoteVivoDAO;
import org.abepomi.dao.vivo.ResumoVivoDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.AssociadoLinhaUtil;
import org.abepomi.dao.util.FaturaUtil;
import org.abepomi.dao.util.LancamentoLinhaTotalUtil;
import org.abepomi.dao.util.LancamentoLinhaUtil;
import org.abepomi.dao.vivo.LinhaVivoDAO;
import org.abepomi.model.Contrato;
import org.abepomi.model.DetalheTelefonia;
import org.abepomi.model.ExtrasTelefonia;
import org.abepomi.model.Fatura;
import org.abepomi.model.FechamentoTelefonia;
import org.abepomi.model.LancamentoLinha;
import org.abepomi.model.Linha;
import org.abepomi.model.Plano;
import org.abepomi.model.Referencia;
import org.abepomi.model.util.TipoLinha;
import org.abepomi.model.vivo.ContratoVivo;
import org.abepomi.model.vivo.FaturaVivo;
import org.abepomi.model.vivo.LctoVivo;
import org.abepomi.model.vivo.LinhaVivo;
import org.abepomi.model.vivo.PacoteVivo;
import org.abepomi.model.vivo.ResumoVivo;

/**
 *
 * @author Julianno
 */
public class FaturaTelefonia {

    private static final String PERIODO = "período de referência: ".toUpperCase();
    private static final String PERIODO_REGEX = "PER.ODO DE REFER.NCIA: .*";
    private static final String SEPARADOR = " a ".toUpperCase();
    private static final String VENCIMENTO = "data de vencimento: ".toUpperCase();
    private static final String VALOR = "valor: r$ ".toUpperCase();
    //private static final String NOTA_FISCAL = "nota fiscal: ".toUpperCase();
    private static final String NOTA_FISCAL = "nota fiscal claro:".toUpperCase();
    private static final String CONTRATO = "Identificação para débito automático: ".toUpperCase();
    private static final String CONTRATO_REGEX = "Identifica..o para d.bito autom.tico: .*".toUpperCase();
    //private static final String DETALHAMENTO = "Tel;Tipo;Data;Hora;Origem(UF)-Destino;Número;Duração/Quantidade;Tarifa;Valor;Valor Cobrado;Nome;CC;Matrícula;Descrição".toUpperCase();
    //private static final String DETALHAMENTO_REGEX = "TEL;TIPO;DATA;HORA;ORIGEM....-DESTINO;N.MERO;DURA..O/QUANTIDADE;TARIFA;VALOR;VALOR COBRADO;NOME;CC;MATR.CULA;DESCRI..O;.*";
    private static final String DETALHAMENTO_REGEX = "TEL;SE..O;DATA;HORA;ORIGEM....-DESTINO;N.MERO;DURA..O/QUANTIDADE;TARIFA;VALOR;VALOR COBRADO;NOME;CC;MATR.CULA;SUB-SE..O;.*";
    //
    private static final String FATURA_VIVO = "00VIVO S/A";

    public static void processar(File file) throws FileNotFoundException, IOException, ParseException, Exception, DAOException {

        FileReader fileReader = new FileReader(file);
        BufferedReader leitor = new BufferedReader(fileReader);

        //List<LancamentoLinha> lctos = new ArrayList<LancamentoLinha>();

        String linha = null;

        if (leitor.ready()) {
            linha = leitor.readLine();
        }

        if (linha.startsWith(FATURA_VIVO)) { //Fatura vivo
            int registro = 0;
            FaturaVivo fatura = null;
            ContratoVivo contrato = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat shf = new SimpleDateFormat("HHmmss");
            FaturaVivoDAO faturaDAO = DAOFactory.createFaturaVivoDAO();
            PacoteVivoDAO pacoteDAO = DAOFactory.createPacoteVivoDAO();
            ResumoVivoDAO resumoDAO = DAOFactory.createResumoVivoDAO();
            LctoVivoDAO lctoDAO = DAOFactory.createLctoVivoDAO();
            //Variável de controle
            String numeroLinha = "";
            String numeroLinhaAntes = "";
            //
            while (leitor.ready()) {
                linha = leitor.readLine();
                registro = Integer.parseInt(linha.substring(0, 2));
                if (registro == 61) {
                    numeroLinha = linha.substring(2, 34).trim();
                }
                if (registro == 62 || registro == 7) {
                    numeroLinha = linha.substring(6, 38).trim();
                }
                if (!numeroLinha.equals(numeroLinhaAntes)) {
                    numeroLinhaAntes = numeroLinha;
                    if (!verificarExistenciaLinhaVivo(numeroLinha)) {
                        LinhaVivo lv = new LinhaVivo();
                        lv.setContrato(contrato);
                        lv.setLinha(numeroLinha);
                        DAOFactory.createLinhaVivoDAO().addEntity(lv);
                    }
                }
                switch (registro) {
                    case 1:
                        contrato = DAOFactory.createContratoVivoDAO().buscaPorNroContrato(linha.substring(2, 17));
                        if (null == contrato) {
                            throw new Exception("Contrato " + linha.substring(2, 17) + " não encontrado!");
                        }
                        break;
                    case 3:
                        if (null == contrato) {
                            throw new Exception("Fatura sem número de contrato!");
                        }
                        if (fatura != null) {
                            throw new Exception("Arquivo com mais de um registro 03!");
                        }
                        fatura = new FaturaVivo();
                        fatura.setEmissao(sdf.parse(linha.substring(2, 10)));
                        fatura.setVencimento(sdf.parse(linha.substring(10, 18)));
                        fatura.setTotal(Double.parseDouble(linha.substring(18, 32)));
                        fatura.setContrato(contrato.getId());
                        faturaDAO.addEntity(fatura);
                        break;
                    case 61:
                        if (fatura == null) {
                            throw new Exception("Arquivo sem regitro 03 para regitro 61!");
                        }
                        PacoteVivo pacote = new PacoteVivo();
                        pacote.setFatura(fatura.getId());
                        pacote.setLinha(linha.substring(2, 34).trim());
                        pacote.setTipo(linha.substring(34, 66).trim());
                        pacote.setDescricao(linha.substring(70, 120).trim());
                        pacote.setValor(Double.parseDouble(linha.substring(120, 134)));
                        try{
                            pacote.setInicio(sdf.parse(linha.substring(134, 142)));
                            pacote.setFim(sdf.parse(linha.substring(142, 150)));
                        }catch (ParseException paex){
                            break;
                        }
                        pacoteDAO.addEntity(pacote);
                        break;
                    case 62:
                        if (fatura == null) {
                            throw new Exception("Arquivo sem regitro 03 para regitro 62!");
                        }
                        ResumoVivo resumo = new ResumoVivo();
                        resumo.setFatura(fatura.getId());
                        resumo.setLinha(linha.substring(6, 38).trim());
                        resumo.setTipo(linha.substring(38, 102).trim());
                        resumo.setItem(linha.substring(102, 136).trim());
                        resumo.setContratado(Double.parseDouble(linha.substring(136, 150)));
                        resumo.setUtilizados(Double.parseDouble(linha.substring(150, 164)));
                        resumo.setExcedentes(Double.parseDouble(linha.substring(164, 178)));
                        resumo.setCobrado(Double.parseDouble(linha.substring(178, 192)));
                        resumoDAO.addEntity(resumo);
                        break;
                    case 7:
                        if (fatura == null) {
                            throw new Exception("Arquivo sem regitro 03 para regitro 07!");
                        }
                        LctoVivo lcto = new LctoVivo();
                        lcto.setFatura(fatura.getId());
                        lcto.setLinha(linha.substring(6, 38).trim());
                        lcto.setSubSecao(linha.substring(38, 72).trim());
                        lcto.setData(sdf.parse(linha.substring(72, 80)));
                        lcto.setHora(shf.parse(linha.substring(80, 86)));
                        lcto.setOrigem(linha.substring(88, 118).trim());
                        lcto.setUfOrigem(linha.substring(118, 120).trim());
                        lcto.setDestino(linha.substring(120, 150).trim());
                        lcto.setUfDestino(linha.substring(150, 152).trim());
                        lcto.setTipo(linha.substring(152, 192).trim());
                        lcto.setChamado(linha.substring(212, 232).trim());
                        lcto.setTarifa(linha.substring(232, 240));
                        lcto.setDuracao(Double.parseDouble(linha.substring(240, 254)));
                        lcto.setValor(Double.parseDouble(linha.substring(254, 268)));
                        lcto.setSecao(linha.substring(268, 332).trim());
                        lctoDAO.addEntity(lcto);
                        break;
                }
            }

            leitor.close();
            fileReader.close();
            return;
        }

        //leitor.reset();
        Fatura fatura = new Fatura();
        int offset = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat shf = new SimpleDateFormat("HH:mm:ss");

        while (leitor.ready()) {
            linha = leitor.readLine().trim().toUpperCase();

            if(linha.matches(PERIODO_REGEX)){
                offset = PERIODO.length();
                fatura.setInicio(sdf.parse(linha.substring(offset, offset + 10)));
                offset = linha.indexOf(SEPARADOR, offset) + SEPARADOR.length();
                fatura.setFim(sdf.parse(linha.substring(offset, offset + 10)));
                //break;
            }

            if (linha.contains(VENCIMENTO)) {
                offset = linha.indexOf(VENCIMENTO, 0) + VENCIMENTO.length();
                fatura.setVencimento(sdf.parse(linha.substring(offset, offset + 10)));
                //break;
            }

            if (linha.contains(VALOR)) {
                offset = linha.indexOf(VALOR, 0) + VALOR.length();
                fatura.setValor(Double.parseDouble(linha.substring(offset).trim().replace(".", "").replace(",", ".")));
                //break;
            }

            if (linha.contains(NOTA_FISCAL)) {
                offset = linha.indexOf(NOTA_FISCAL) + NOTA_FISCAL.length();
                String nota = linha.substring(offset).trim();
                nota = nota.split(" ")[0];
                fatura.setNota(nota);
            }

            //if (linha.contains(CONTRATO)) {
            if(linha.matches(CONTRATO_REGEX)){
                offset = CONTRATO.length();
                fatura.setContrato(buscarContrato(linha.substring(offset).trim()));
                if (fatura.getContrato() == null) {
                    throw new Exception("Contrato não encontrado!");
                }
            }

            if (linha.matches(DETALHAMENTO_REGEX)) {
                break;
            }
        }

        if (DAOFactory.createFaturaDAO().isFaturaCadastrada(fatura.getNota())) {
            leitor.close();
            fileReader.close();
            throw new DAOException("Fatura já processada!!!");
        }

        fatura.setLancamentos(new ArrayList<LancamentoLinha>());
        //DAOFactory.createFaturaDAO().addEntity(fatura);

        int inset = 0;
        int codigo = 1;
        String tel = "";
        while (leitor.ready()) {
            linha = leitor.readLine().toUpperCase();
            inset = 0;
            offset = linha.indexOf(";", inset);
            if(offset < 0){
                tel = "";
            }else{
                tel = linha.substring(inset, offset);
            }
            if (tel.length() > 0) {
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String tipo = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String data = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String hora = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String origem = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String destino = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String qtde = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String tarifa = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String valor = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String cobrado = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String nome = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String cc = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String matricula = linha.substring(inset, offset);
                inset = offset + 1;
                offset = linha.indexOf(";", inset);
                String descricao = null;
                if (offset == -1) {
                    descricao = linha.substring(inset);
                } else {
                    descricao = linha.substring(inset, offset);
                }
                /*}


                if (!tel.equals("")) {*/
                LancamentoLinha lcto = new LancamentoLinha();
                tel = tel.replace(" ", "").replace("-", "");
                if(tel.length()==11){
                    tel = tel.substring(0, 2) + tel.substring(3);
                }
                Linha linhaTelefone = null;
                linhaTelefone = buscarLinha(tel);
                if (null == linhaTelefone) {
                    throw new Exception("Linha não encontrada!");
                    /*linhaTelefone = new Linha();
                    linhaTelefone.setNumero(tel);*/
                }
                lcto.setLinha(linhaTelefone);
                lcto.setCobrado(Double.parseDouble(cobrado.replace(".", "").replace(",", ".")));
                lcto.setContaCorrente(cc.trim());
                lcto.setData(sdf.parse(data));
                lcto.setDescricao(descricao.trim());
                if (qtde.length() > 0) {
                    lcto.setDuracao(horaToDouble(qtde));
                }
                if (hora.length() > 0) {
                    lcto.setHora(shf.parse(hora));
                }
                lcto.setMatricula(matricula.trim());
                lcto.setNome(nome.trim());
                lcto.setNumeroDestino(destino.trim());
                lcto.setOrigem(origem.trim());
                lcto.setTarifa(Double.parseDouble(tarifa.replace(".", "").replace(",", ".")));
                lcto.setTipoLancamento(tipo.trim());
                lcto.setValor(Double.parseDouble(valor.replace(".", "").replace(",", ".")));

                lcto.setCodigo(codigo);
                codigo++;

                lcto.setFatura(fatura);
                //DAOFactory.createLancamentoLinhaDAO().addEntity(lcto);
                fatura.getLancamentos().add(lcto);
                /*if(fatura.getLancamentos().size()%500==0){
                //System.out.println("tamanho: "+fatura.getLancamentos().size());
                DAOFactory.createFaturaDAO().updateWithFlush(fatura);
                }*/
            }
        }

        //fatura.setLancamentos(lctos);
        //DAOFactory.createFaturaDAO().addEntity(fatura);
        leitor.close();
        fileReader.close();
        DAOFactory.createFaturaDAO().addEntity(fatura);
    }

    private static Contrato buscarContrato(String numero) {
        return DAOFactory.createContratoDAO().buscaPorNumero(numero);
    }

    private static Linha buscarLinha(String numero) {
        try {
            return DAOFactory.createLinhaDAO().findById(numero);
        } catch (DAOException ex) {
            Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, "Erro ao buscar linha FaturaTelefonia", ex);
        }
        return null;
    }

    private static Double horaToDouble(String hora) {
        SimpleDateFormat shf = new SimpleDateFormat("HH:mm:ss");
        try {
            return Double.parseDouble(hora.replace(".", "").replace(",", "."));
        } catch (NumberFormatException numberEx) {
            //Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, "Erro no horaToDouble FaturaTelefonia", numberEx);
        }

        try {
            Date data = shf.parse(hora);
            Date dataAux = shf.parse("00:00:00");
            return ((double) (data.getTime() - dataAux.getTime()) / (1000 * 60));
        } catch (ParseException parseEx) {
            //Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, "Erro no horaToDouble FaturaTelefonia", parseEx);
        }

        return null;
    }

    public static void faturar(Referencia referencia) throws DAOException {

        FechamentoTelefonia fecha = new FechamentoTelefonia();
        fecha.setReferencia(referencia);

        //DAOFactory.createFechamentoTelefoniaDAO().addEntity(fecha);

        List<FaturaUtil> faturas = DAOFactory.createFaturaDAO().buscaNFaturados();
        List<Integer> ass = DAOFactory.createAssociadoLinhaDAO().associadosComLinhas();
        List<Plano> planos = DAOFactory.createPlanoDAO().findAll();



        for (int associado : ass) {
            List<AssociadoLinhaUtil> assLinhas = DAOFactory.createAssociadoLinhaDAO().linhasPorAssociado(associado);
            //Vector vetorLinhas = new Vector();
            //Plano plano = null;
            //FaturaUtil fatura = null;

            /*for (AssociadoLinhaUtil assUtil : assLinhas) {
            int codigo = DAOFactory.createFaturaDAO().buscaPorTelefoneSFaturar(assUtil.getLinha());
            assUtil.setFatura(codigo);
            Plano plano = getPlano(planos, assUtil.getPlano());
            FaturaUtil fatura = FaturaUtil.getPorCodigo(faturas, codigo);
            Double proporcao = assUtil.calculaProporcao(fatura.getInicio(), fatura.getFim());
            if (null != proporcao) {
            if (plano.getTipo() == TipoLinha.D) {
            vetorLinhas.add(assUtil);
            } else {
            BigDecimal franquia = new BigDecimal("0.00").setScale(2);
            franquia = franquia.add(new BigDecimal(new Double(proporcao * plano.getFranquia()).toString()).setScale(2, RoundingMode.DOWN));
            Double valor = DAOFactory.createLancamentoLinhaDAO().valorGastoPorPeriodo(assUtil.getEntrega(), assUtil.getDevolucao(), assUtil.getLinha(), fatura.getCodigo());
            BigDecimal valorGasto = new BigDecimal(valor.toString()).setScale(2, RoundingMode.DOWN);
            if (franquia.doubleValue() > valorGasto.doubleValue()) {
            if (plano.isCompartilha()) {
            vetorLinhas.add(0, assUtil);
            } else {
            vetorLinhas.add(assUtil);
            }
            } else {
            vetorLinhas.add(assUtil);
            }
            }
            }
            }*/

            List<DetalheTelefonia> debitos = new ArrayList<DetalheTelefonia>();
            List<DetalheTelefonia> creditos = new ArrayList<DetalheTelefonia>();

            BigDecimal franquiaDoada = new BigDecimal("0.00");
            BigDecimal franquiaRecebida = new BigDecimal("0.00");

            //BigDecimal compartilha = new BigDecimal("0.00");
            for (AssociadoLinhaUtil assUtil : assLinhas /*(List<AssociadoLinhaUtil>) vetorLinhas*/) {
                System.out.println("Número linha: "+assUtil.getLinha());
                int codigo = DAOFactory.createFaturaDAO().buscaPorTelefoneSFaturar(assUtil.getLinha());
                if (codigo != 0) {
                    assUtil.setFatura(codigo);
                    DetalheTelefonia detalhe = new DetalheTelefonia();
                    detalhe.setAssociado(associado);
                    Plano plano = getPlano(planos, assUtil.getPlano());
                    FaturaUtil fatura = FaturaUtil.getPorCodigo(faturas, assUtil.getFatura());
                    if (plano.getTipo() == TipoLinha.D) {
                        List<LancamentoLinhaUtil> lctos = DAOFactory.createLancamentoLinhaDAO().getLctosPorLinha(assUtil.getLinha(), LancamentoLinhaUtil.LCTO_MENSALIDADES);
                        fatura = FaturaUtil.getPorCodigo(faturas, assUtil.getFatura());
                        //Double pacotes = 0.00;
                        BigDecimal pacotes = new BigDecimal("0.00").setScale(2);
                        for (LancamentoLinhaUtil lctoPacote : lctos) {
                            if (!(lctoPacote.getTipoLancamento().contains(LancamentoLinhaUtil.TIPO_PLANO_MODEM)||lctoPacote.getTipoLancamento().contains(LancamentoLinhaUtil.TIPO_PLANO_TABLET))) {

                                pacotes = pacotes.add(new BigDecimal(lctoPacote.getValor().toString()).setScale(2));
                            }
                        }
                        Double proporcao = assUtil.calculaProporcao(fatura.getInicio(), fatura.getFim());
                        if ((null == proporcao) || (proporcao == 0)) {
                            Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, "{0}: propor\u00e7\u00e3o com valor nulo ou zero!!!", assUtil.getLinha());
                            detalhe = null;
                        } else {
                            BigDecimal franquia = new BigDecimal("0.00").setScale(2);
                            franquia = franquia.add(new BigDecimal(new Double(proporcao * plano.getFranquia()).toString()).setScale(2, RoundingMode.DOWN));
                            //Double franquia = plano.getFranquia() * proporcao;
                            //Double outros = 0.00;
                            BigDecimal outros = new BigDecimal("0.00").setScale(2);
                            lctos = DAOFactory.createLancamentoLinhaDAO().getLctosPorLinha(assUtil.getLinha(), LancamentoLinhaUtil.LCTO_UTILIZACAO);
                            for (LancamentoLinhaUtil utilizacao : lctos) {
                                if (utilizacao.getData().getTime() >= assUtil.getEntrega().getTime()
                                        && (null == assUtil.getDevolucao() || utilizacao.getData().getTime() <= assUtil.getDevolucaoD1().getTime())) {
                                    outros = outros.add(new BigDecimal(utilizacao.getCobrado().toString()).setScale(2));
                                }
                            }

                            detalhe.setOutros(outros.doubleValue());
                            detalhe.setLinha(assUtil.getLinha());
                            detalhe.setPlano(new BigDecimal(new Double(plano.total() * proporcao).toString()).setScale(2, RoundingMode.DOWN).doubleValue());
                            detalhe.setFranquia(franquia.doubleValue());
                            detalhe.setFatura(fatura.getCodigo());
                            detalhe.setPacotes(pacotes.doubleValue());
                            detalhe.setDescricaoPlano(plano.getDescricao());

                            Date devol = assUtil.getDevolucao() == null ? fatura.getFim() : assUtil.getDevolucaoD1();
                            LancamentoLinhaTotalUtil totalLocais = DAOFactory.createLancamentoLinhaDAO().getUtilizacaoPorFiltro(assUtil.getLinha(), LancamentoLinhaTotalUtil.FILTRO_LOCAIS, assUtil.getFatura(), assUtil.getEntrega(), devol);
                            if (null == totalLocais) {
                                detalhe.setMinutosLocais(new Double("0.00"));
                            } else {
                                detalhe.setMinutosLocais(totalLocais.getTotal());
                            }
                        }

                    } else {

                        List<LancamentoLinhaUtil> lctos = DAOFactory.createLancamentoLinhaDAO().getLctosPorLinha(assUtil.getLinha(), LancamentoLinhaUtil.LCTO_MENSALIDADES);
                        //fatura = FaturaUtil.getPorCodigo(faturas, lctos.get(0).getFatura());
                        //Double pacotes = 0.00;
                        BigDecimal pacotes = new BigDecimal("0.00").setScale(2);
                        for (LancamentoLinhaUtil lctoPacote : lctos) {
                            if (!(lctoPacote.getTipoLancamento().contains(LancamentoLinhaUtil.TIPO_ASSINATURA)
                                    || lctoPacote.getTipoLancamento().contains(LancamentoLinhaUtil.TIPO_GESTOR)
                                    || lctoPacote.getTipoLancamento().contains(LancamentoLinhaUtil.TIPO_TARZERO))) {
                                pacotes = pacotes.add(new BigDecimal(lctoPacote.getValor().toString()).setScale(2));
                            }
                        }
                        Double proporcao = assUtil.calculaProporcao(fatura.getInicio(), fatura.getFim());
                        if ((null == proporcao || proporcao == 0.00)) {
                            Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, "{0}: propor\u00e7\u00e3o com valor nulo ou zero!!!", assUtil.getLinha());
                            detalhe = null;
                        } else {
                            //Double franquia = plano.getFranquia() * proporcao;
                            BigDecimal franquia = new BigDecimal("0.00").setScale(2);
                            //BigDecimal franquiaFinal = new BigDecimal("0.00").setScale(2);
                            franquia = franquia.add(new BigDecimal(new Double(proporcao * plano.getFranquia()).toString()).setScale(2, RoundingMode.DOWN));
                            //franquiaFinal = franquia;
                            Double valor = DAOFactory.createLancamentoLinhaDAO().valorGastoPorPeriodo(assUtil.getEntrega(), assUtil.getDevolucao(), assUtil.getLinha(), fatura.getCodigo());
                            BigDecimal valorGasto = new BigDecimal(valor.toString()).setScale(2, RoundingMode.DOWN);
                            if (franquia.doubleValue() > valorGasto.doubleValue()) {
                                if (plano.isCompartilha()) {
                                    detalhe.setDebito(franquia.subtract(valorGasto).doubleValue());
                                    franquiaDoada = franquiaDoada.add(franquia.subtract(valorGasto));
                                    //compartilha = compartilha.add(new BigDecimal(detalhe.getDebito().toString()).setScale(2, RoundingMode.DOWN));
                                }
                            } else {
                                /*                        if (plano.isCompartilha()) {
                                BigDecimal auxiliar = valorGasto.subtract(franquia);
                                if (compartilha.doubleValue() > 0.00) {
                                if (compartilha.doubleValue() > auxiliar.doubleValue()) {
                                compartilha = compartilha.subtract(auxiliar);
                                franquia = franquia.add(auxiliar);
                                detalhe.setCredito(auxiliar.doubleValue());
                                } else {
                                franquia = franquia.add(compartilha);
                                detalhe.setCredito(compartilha.doubleValue());
                                compartilha = new BigDecimal("0.00").setScale(2);
                                }
                                }
                                }*/
                                valorGasto = new BigDecimal("0.00").setScale(2);
                                /*Double excedente = 0.00;
                                Double outros = 0.00;*/
                                BigDecimal excedente = new BigDecimal("0.00").setScale(2);
                                BigDecimal outros = new BigDecimal("0.00").setScale(2);
                                lctos = DAOFactory.createLancamentoLinhaDAO().getLctosPorLinha(assUtil.getLinha(), LancamentoLinhaUtil.LCTO_UTILIZACAO);
                                for (LancamentoLinhaUtil utilizacao : lctos) {
                                    if (utilizacao.getData().getTime() >= assUtil.getEntrega().getTime()
                                            && (null == assUtil.getDevolucao() || utilizacao.getData().getTime() <= assUtil.getDevolucaoD1().getTime())) {
                                        if (valorGasto.doubleValue() + utilizacao.getValor() <= franquia.doubleValue()) {
                                            valorGasto = valorGasto.add(new BigDecimal(utilizacao.getValor().toString()).setScale(2));
                                        } else {
                                            if (valorGasto.doubleValue() < franquia.doubleValue()) {
                                                BigDecimal aux = franquia.subtract(valorGasto);
                                                valorGasto = franquia;
                                                BigDecimal sobra = new BigDecimal(utilizacao.getValor().toString()).setScale(2).subtract(aux);
                                                if (utilizacao.getDescricao().contains(LancamentoLinhaUtil.DESCRICAO_LOCAIS)) {
                                                    excedente = excedente.add(sobra.divide(new BigDecimal("0.20"), 2, RoundingMode.DOWN));
                                                } else {
                                                    outros = outros.add(sobra);
                                                }
                                            } else {
                                                if (utilizacao.getDescricao().contains(LancamentoLinhaUtil.DESCRICAO_LOCAIS)) {
                                                    excedente = excedente.add(new BigDecimal(utilizacao.getDuracao().toString())).setScale(2);
                                                } else {
                                                    outros = outros.add(new BigDecimal(utilizacao.getValor().toString())).setScale(2);
                                                }
                                            }
                                        }
                                    }
                                }
                                detalhe.setExcedidos(excedente.doubleValue());
                                detalhe.setOutros(outros.doubleValue());
                                if (plano.isCompartilha()) {
                                    detalhe.setCredito(excedente.multiply(new BigDecimal(plano.getAdicional().toString())).setScale(2, RoundingMode.UP).doubleValue());
                                    detalhe.setCredito(outros.add(new BigDecimal(detalhe.getCredito().toString())).setScale(2, RoundingMode.UP).doubleValue());
                                    franquiaRecebida = franquiaRecebida.add(new BigDecimal(detalhe.getCredito().toString()).setScale(2));
                                }
                            }

                            detalhe.setLinha(assUtil.getLinha());
                            detalhe.setPlano(new BigDecimal(plano.total() * proporcao).setScale(2, RoundingMode.DOWN).doubleValue());
                            //detalhe.setFranquia(franquiaFinal.doubleValue());
                            detalhe.setFranquia(franquia.doubleValue());
                            detalhe.setFatura(fatura.getCodigo());
                            detalhe.setPacotes(pacotes.doubleValue());
                            detalhe.setValor_minuto(plano.getAdicional());
                            detalhe.setDescricaoPlano(plano.getDescricao());

                            Date devol = assUtil.getDevolucao() == null ? fatura.getFim() : assUtil.getDevolucaoD1();
                            LancamentoLinhaTotalUtil totalLocais = DAOFactory.createLancamentoLinhaDAO().getUtilizacaoPorFiltro(assUtil.getLinha(), LancamentoLinhaTotalUtil.FILTRO_LOCAIS, assUtil.getFatura(), assUtil.getEntrega(), devol);
                            if (null == totalLocais) {
                                detalhe.setMinutosLocais(new Double("0.00"));
                            } else {
                                detalhe.setMinutosLocais(totalLocais.getTotal());
                            }
                        }
                    }


                    if (null != detalhe) {
                        boolean parcela = true;
                        boolean parcelaHabil = true;
                        if (null == assUtil.getDevolucao()) {
                            List<ExtrasTelefonia> extrasTel = DAOFactory.createExtrasTelefoniaDAO().getPorLinhaSFaturar(detalhe.getLinha());

                            for (ExtrasTelefonia extra : extrasTel) {
                                boolean atualiza = false;
                                if (parcela && extra.getTipo().equals(ExtrasTelefonia.TIPO_APARELHO)) {
                                    parcela = false;
                                    atualiza = true;
                                    detalhe.setAparelho(new BigDecimal(extra.getValor().toString()).setScale(2, RoundingMode.DOWN).doubleValue());
                                } else if (parcelaHabil && extra.getTipo().equals(ExtrasTelefonia.TIPO_HABILITACAO)) {
                                    parcelaHabil = false;
                                    atualiza = true;
                                    detalhe.setHabilitacao(new BigDecimal(extra.getValor().toString()).setScale(2, RoundingMode.DOWN).doubleValue());
                                } else if (extra.getTipo().equals(ExtrasTelefonia.TIPO_DESCONTO)) {
                                    atualiza = true;
                                    detalhe.setDesconto(new BigDecimal(detalhe.getDesconto().toString()).setScale(2, RoundingMode.DOWN).
                                            add(new BigDecimal(extra.getValor().toString()).setScale(2, RoundingMode.DOWN)).doubleValue());
                                } else if (extra.getTipo().equals(ExtrasTelefonia.TIPO_ACRESCIMO)) {
                                    atualiza = true;
                                    detalhe.setAcrescimo(new BigDecimal(detalhe.getAcrescimo().toString()).setScale(2, RoundingMode.DOWN).
                                            add(new BigDecimal(extra.getValor().toString()).setScale(2, RoundingMode.DOWN)).doubleValue());
                                }
                                if (atualiza) {
                                    extra.setFatura(detalhe.getFatura());
                                    extra.setFaturada(true);
                                    DAOFactory.createExtrasTelefoniaDAO().updateEntity(extra);
                                }
                            }
                        }
                        detalhe.setFechamento(fecha);
                        //DAOFactory.createDetalheDAO().addEntity(detalhe);
                        fecha.getDetalhamento().add(detalhe);
                        /*if(fecha.getDetalhamento().size()%50==0){
                        DAOFactory.createFechamentoTelefoniaDAO().updateWithFlush(fecha);
                        }*/

                        if (getPlano(planos, assUtil.getPlano()).isCompartilha()) {
                            if (detalhe.getDebito() > 0.00) {
                                debitos.add(detalhe);
                            } else {
                                creditos.add(detalhe);
                            }
                        }

                    }
                }

            }


            if (franquiaDoada.doubleValue() < franquiaRecebida.doubleValue()) {
                for (DetalheTelefonia detalhe : creditos) {
                    if (detalhe.getCredito() <= franquiaDoada.doubleValue()) {
                        franquiaDoada.subtract(new BigDecimal(detalhe.getCredito().toString()).setScale(2));
                    } else {
                        detalhe.setCredito(franquiaDoada.doubleValue());
                        franquiaDoada = new BigDecimal("0.00");
                    }
                }
            } else if (franquiaDoada.doubleValue() > franquiaRecebida.doubleValue()) {
                for (DetalheTelefonia detalhe : debitos) {
                    if (detalhe.getDebito() <= franquiaRecebida.doubleValue()) {
                        franquiaRecebida.subtract(new BigDecimal(detalhe.getDebito().toString()).setScale(2));
                    } else {
                        detalhe.setDebito(franquiaRecebida.doubleValue());
                        franquiaRecebida = new BigDecimal("0.00");
                    }
                }
            }


        }
        for (FaturaUtil faturamento : faturas) {
            Fatura fatTel = DAOFactory.createFaturaDAO().findById(faturamento.getCodigo());
            fatTel.setFaturada(true);
            DAOFactory.createFaturaDAO().updateEntity(fatTel);
        }

        //System.out.println("Faturar antes: "+new Date());
        //DAOFactory.createFechamentoTelefoniaDAO().addEntity(fecha);
        DAOFactory.createFechamentoTelefoniaDAO().addEntity(fecha);
        //System.out.println("Faturar depois: "+new Date());
    }

    private static Plano getPlano(List<Plano> planos, int codigo) {
        for (Plano plano : planos) {
            if (plano.getCodigo() == codigo) {
                return plano;
            }
        }
        return null;
    }

    private static boolean verificarExistenciaLinhaVivo(String linha) {
        LinhaVivo l = null;
        LinhaVivoDAO lDAO = DAOFactory.createLinhaVivoDAO();
        try {
            l = lDAO.findById(linha);
        } catch (DAOException ex) {
            Logger.getLogger(FaturaTelefonia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null == l ? false : true;
    }

    public static void faturarVivo(){
        FaturaVivoDAO fatDAO = DAOFactory.createFaturaVivoDAO();
        List<Integer> faturas = fatDAO.buscarNaoFaturadas();
        for (Integer fatura : faturas){
            fatDAO.processarFaturaVivo(fatura);
        }
    }
}
