/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mail;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.abepomi.dao.util.AssociadoEmail;
import org.abepomi.mb.AssociadoMB;
import org.abepomi.mb.util.GeraMd5;
import org.abepomi.model.Referencia;
import org.abepomi.model.util.Validacao;

/**
 *
 * @author Julianno
 */
public class EmailMensalThread extends Thread {

    Map<AssociadoEmail, byte[]> map;
    Referencia referencia;

    public EmailMensalThread(Map<AssociadoEmail, byte[]> map, Referencia referencia) {
        this.map = map;
        this.referencia = referencia;
    }

    @Override
    public void run() {
        EmailAbepomi email = new EmailAbepomi();
        StringBuilder relatorio = new StringBuilder();
        relatorio.append("Resumo de emails enviados:\n");
        relatorio.append("Início: "+new Date()+"\n");
        relatorio.append("Nome do Associado;Emails\n");

        for (AssociadoEmail assoc : map.keySet()) {

            String assunto = "Fatura ABEPOMI - " + referencia.getRef();
            String texto = "Prezado(a) Sr(a) " + assoc.getNome().toUpperCase()
                    + ",\n\nConfira no link abaixo seu extrato mensal e o Boleto Bancário para quem ainda não tem o cadastro de débito automático.\n";
            String ran = String.valueOf(new Random().nextInt(100000));
            String cpf = assoc.getCpf();
            String ass = Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(assoc.getId()), 5);
            String ref = Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(referencia.getId()), 5);
            String has = null;
            try {
                has = GeraMd5.md5(cpf + ass + ref + ran);
            } catch (Exception e) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Falha ao gerar hash ao enviar emails: public String enviarEmailFatura()", e);
                return;
            }
            String link = "http://www.abepomi.com.br/sistema/faturaweb.jsf?cpf=" + cpf + "&ass=" + ass + "&ref=" + ref + "&ran=" + ran + "&has=" + has;
            texto = texto + link
                    + "\n\nAssociado(a) coopere com a Abepomi faça seu cadastro para pagamento através de débito automático,\n"
                    + "para quem ainda não autorizou o débito automático informamos que a partir de junho de 2012 estaremos\n"
                    + " cobrando multa e juros para quem pagar com atraso, portanto pague em dia seu boleto e economize dinheiro."
                    + "\n\nAtenciosamente,\n\nABEPOMI.";


            if (assoc.getEmails().equals(",")) {
                email.sendMail("abepomi@gmail.com,juliannoms@gmail.com", assunto, texto, map.get(assoc));

            } else {
                email.sendMail(assoc.getEmails(), assunto, texto, map.get(assoc));
                relatorio.append(assoc.getNome()+";"+assoc.getEmails()+"\n");
            }
            try {
                Thread.sleep(1000 * 30);
            } catch (InterruptedException ex) {
                Logger.getLogger(EmailMensalThread.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        relatorio.append("Fim: "+new Date()+"\n");
        email.sendMail("abepomi@gmail.com,juliannoms@gmail.com", "Envio de emails mensais Ref: "+referencia.getRef(), relatorio.toString());
    }
}
