/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mail;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Julianno
 */
public class EmailAbepomi {

    private static String username = "sistema@abepomi.com.br";
    private static String password = "abepomiSis!23";
    @Resource(name = "mail:sistema")
    private Session mailSession;

    public boolean sendMail(String recipient, String subject, String text){
        return sendMail(recipient, subject, text, null);
    }

    public boolean sendMail(String recipient, String subject, String text, byte[] anexo) {
        boolean exe = false;
        InitialContext c = null;
        try {
            c = new InitialContext();
            mailSession = (javax.mail.Session) c.lookup("sistema");
        } catch (NamingException ex) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMail()", ex);
            return false;
        }

        Transport transport = null;
        try {
            MimeMessage msg = new MimeMessage(mailSession);
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setFrom(InternetAddress.getLocalAddress(mailSession));
            //msg.saveChanges();
            /**/
            if (null != anexo) {
                msg.addHeader("Content-Type", "application/pdf");
                MimeBodyPart partPhoto = new MimeBodyPart();
                partPhoto.setDataHandler(new DataHandler(new ByteArrayDataSource(anexo, "application/pdf")));
                partPhoto.setFileName("boleto.pdf");
                MimeBodyPart partText = new MimeBodyPart();
                partText.setText(text);
                Multipart corpo = new MimeMultipart();
                corpo.addBodyPart(partText);
                corpo.addBodyPart(partPhoto);
                msg.setContent(corpo);
            }else{
                msg.setText(text);
            }
            /**/
            msg.saveChanges();
            transport = mailSession.getTransport("smtp");
            transport.connect(username, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            exe = true;
        } catch (AddressException e) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMail()", e);
            return false;
        } catch (MessagingException e) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMail()", e);
            return false;
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException me) {
                Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMail()", me);
            } catch (Exception e) {
                Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMail()", e);
            }
        }
        return exe;
    }

    public boolean sendMails(List<String[]> emails) {
        boolean exe = false;
        InitialContext c = null;
        try {
            c = new InitialContext();
            mailSession = (javax.mail.Session) c.lookup("java:comp/env/sistema");
        } catch (NamingException ex) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMails()", ex);
            return false;
        }
        Transport transport = null;
        try {
            transport = mailSession.getTransport("smtp");
            transport.connect(username, password);
            for (String[] email : emails) {
                Message msg = new MimeMessage(mailSession);
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email[0]));
                msg.setSubject(email[1]);
                msg.setText(email[2]);
                msg.setSentDate(new Date());
                msg.setFrom(InternetAddress.getLocalAddress(mailSession));
                msg.saveChanges();

                transport.sendMessage(msg, msg.getAllRecipients());
            }
            transport.close();
            exe = true;
        } catch (AddressException e) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMails()", e);
            return false;
        } catch (MessagingException e) {
            Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMails()", e);
            return false;
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException me) {
                Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMails()", me);
            } catch (Exception e) {
                Logger.getLogger(EmailAbepomi.class.getName()).log(Level.SEVERE, "org.abepomi.mail.EmailAbepomi.sendMails()", e);
            }
        }
        return exe;
    }
}
