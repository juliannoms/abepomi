/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.pdf;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.ByteArrayOutputStream;
import java.util.GregorianCalendar;

import org.abepomi.model.AssDA;
import org.abepomi.model.Associado;

/**
 *
 * @author Julianno
 */
public class PdfUtil {

    private static String[] meses = {"JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"};

    public static void gerarAutDebito(AssDA ass, String caminho, ByteArrayOutputStream arquivo) {
        PdfReader reader;
        AcroFields fields;
        PdfStamper stamp;
        try {
            //System.out.print(caminho);
            reader = new PdfReader(caminho+"/resources/autDeb.pdf");
            stamp = new PdfStamper(reader, arquivo);
            fields = stamp.getAcroFields();
            fields.setField("txtIdentificador", ass.getIdenDA());
            fields.setField("txtNome", ass.getAssociado().getNome());
            fields.setField("txtCpf", ass.getAssociado().getCpf());
            fields.setField("txtRg", ass.getAssociado().getRg());
            stamp.setFormFlattening(true);
            stamp.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void gerarTermoAnuencia(Associado ass, String caminho, ByteArrayOutputStream arquivo){
        PdfReader reader;
        AcroFields fields;
        PdfStamper stamp;
        GregorianCalendar data = new GregorianCalendar();
        try {
            //System.out.print(caminho);
            reader = new PdfReader(caminho+"/resources/termo_anuencia.pdf");
            stamp = new PdfStamper(reader, arquivo);
            fields = stamp.getAcroFields();
            fields.setField("txtNome", ass.getNome());
            fields.setField("txtNome2", ass.getNome());
            fields.setField("txtCpf", ass.getCpf().substring(0, 3)+"."+ass.getCpf().substring(3, 6)+"."+ass.getCpf().substring(6, 9)+"-"+ass.getCpf().substring(9, 11));
            fields.setField("txtRg", ass.getRg());
            fields.setField("txtRe", ass.getRe());
            fields.setField("txtEstadoCivil", ass.getEstado().getEstadoCivil().toUpperCase());
            fields.setField("txtLogradouro", ass.getEndereco().getLogradouro());
            fields.setField("txtProfissao", ass.getPosto());
            fields.setField("txtCidade", ass.getEndereco().getCidade()+" - "+ass.getEndereco().getUf().name());
            fields.setField("txtLocal", "PRESIDENTE PRUDENTE");
            fields.setField("txtDia", String.valueOf(data.get(GregorianCalendar.DAY_OF_MONTH)));
            fields.setField("txtAno", String.valueOf(data.get(GregorianCalendar.YEAR)));
            fields.setField("txtMes", meses[data.get(GregorianCalendar.MONTH)]);

            stamp.setFormFlattening(true);
            stamp.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
