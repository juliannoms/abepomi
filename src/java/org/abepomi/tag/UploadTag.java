/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.tag;

/**
 *
 * Retirado do capítulo 13 do Core JavaServer Faces 2ª Edição de David Geary e Cay Horstmann
 */
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.webapp.UIComponentELTag;

public class UploadTag extends UIComponentELTag{
   private ValueExpression value;
   private ValueExpression target;

   public String getRendererType() { return "org.abepomi.renderer.UploadRenderer"; }
   public String getComponentType() { return "org.abepomi.tag.UploadTag"; }

   public void setValue(ValueExpression newValue) { value = newValue; }
   public void setTarget(ValueExpression newValue) { target = newValue; }

   @Override
   public void setProperties(UIComponent component) {
      super.setProperties(component);
      component.setValueExpression("target", target);
      component.setValueExpression("value", value);
   }

   @Override
   public void release() {
      super.release();
      value = null;
      target = null;
   }
}
