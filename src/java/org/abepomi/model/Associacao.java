/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */

@Entity
@SequenceGenerator(name="seqaso", sequenceName="seq_associacao", allocationSize=1)
@Table(name="associacao", schema="public")
public class Associacao implements Serializable {

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqaso")
    private int id;

    @Column(name="nome", length=50)
    private String nome;

    @Column(name="cnpj", length=14)
    private String cnpj;

    @Column(name="ie", length=14)
    private String ie;

    @Column(name="telefone", length=10)
    private String telefone;

    @Column(name="contato", length=50)
    private String contato;

    @Column(name="uc", precision=10, scale=2)
    private Double unContribuicao;

    @Column(name="mult", precision=10, scale=2)
    private Double multContribuicao;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="endereco", referencedColumnName="id")
    private Endereco endereco = new Endereco();

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="associacao")
    private List<ConvDA> conveniosDA = new ArrayList<ConvDA>();

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="associacao")
    private List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();

    public Associacao() {
    }

    public Double getMultContribuicao() {
        return multContribuicao;
    }

    public void setMultContribuicao(Double multContribuicao) {
        this.multContribuicao = multContribuicao;
    }

    public List<Fornecedor> getFornecedores() {
        return fornecedores;
    }

    public void setFornecedores(List<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

    public List<ConvDA> getConveniosDA() {
        return conveniosDA;
    }

    public void setConveniosDA(List<ConvDA> conveniosDA) {
        this.conveniosDA = conveniosDA;
    }



    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Double getUnContribuicao() {
        return unContribuicao;
    }

    public void setUnContribuicao(Double unContribuicao) {
        this.unContribuicao = unContribuicao;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Associacao other = (Associacao) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Associacao";
    }



}
