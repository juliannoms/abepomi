/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="associacao_linhas", schema="public")
//@NamedQuery(name="AssociacaoLinhas.linhasNaoDevolvidas", query="select aso from AssociacaoLinha aso where aso.devolucao is null order by aso.linha.numero")

@SequenceGenerator(name="seq_associacao_linhas", sequenceName="seq_asolinhas", allocationSize=1)
public class AssociacaoLinha implements Serializable{

    @Id
    @GeneratedValue(generator="seq_associacao_linhas", strategy=GenerationType.SEQUENCE)
    private int id;

    @Column(name="favorecido", length=50)
    private String favorecido;

    @Temporal(TemporalType.DATE)
    @Column(name="entrega", nullable=false)
    private Date entrega;

    @Temporal(TemporalType.DATE)
    @Column(name="devolucao")
    private Date devolucao;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="numero", referencedColumnName="numero")
    private Linha linha;

    public AssociacaoLinha() {
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public String getFavorecido() {
        return favorecido;
    }

    public void setFavorecido(String favorecido) {
        this.favorecido = favorecido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssociacaoLinha other = (AssociacaoLinha) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "AssociacaoLinha{" + "id=" + id + '}';
    }

    

}
