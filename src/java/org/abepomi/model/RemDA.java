/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */

@Entity
@Table(name="aut_remessa", schema="public")
@SequenceGenerator(name="seqrem", sequenceName="seq_remessa", allocationSize=1)
@NamedQuery(name="MaxRemNSA", query="select max(r.nsa) from RemDA r")
public class RemDA implements Serializable {

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqrem")
    private int id;

    @Column(name="nsa")
    private int nsa;

    @Temporal(TemporalType.DATE)
    @Column(name="dtgeracao")
    private Date dtGeracao;

    @Column(name="valor", precision=10, scale=2)
    private Double valor;

    @ManyToOne(cascade=CascadeType.ALL ,fetch=FetchType.EAGER)
    @JoinColumn(name="aut_id")
    private ConvDA convenio;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="remDA")
    private List<MovDA> debitos = new ArrayList<MovDA>();

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="ref_id", referencedColumnName="id")
    private Referencia ref = new Referencia();

    public RemDA() {
    }

    public Referencia getRef() {
        return ref;
    }

    public void setRef(Referencia ref) {
        this.ref = ref;
    }



    public ConvDA getConvenio() {
        return convenio;
    }

    public void setConvenio(ConvDA convenio) {
        this.convenio = convenio;
    }

    public Date getDtGeracao() {
        return dtGeracao;
    }

    public void setDtGeracao(Date dtGeracao) {
        this.dtGeracao = dtGeracao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNsa() {
        return nsa;
    }

    public void setNsa(int nsa) {
        this.nsa = nsa;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<MovDA> getDebitos() {
        return debitos;
    }

    public void setDebitos(List<MovDA> debitos) {
        this.debitos = debitos;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RemDA other = (RemDA) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.RemDA";
    }



}
