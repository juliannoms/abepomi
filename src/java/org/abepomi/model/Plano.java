/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.abepomi.model.util.Operadora;
import org.abepomi.model.util.TipoLinha;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="planos", schema="public")
@SequenceGenerator(name="seqplano", sequenceName="seq_planos", allocationSize=1)
public class Plano implements Serializable {

    public Plano() {
    }

    @Id
    @Column(name="codigo")
    @GeneratedValue(generator="seqplano", strategy=GenerationType.SEQUENCE)
    private int codigo = 0;

    @Column(name="descricao", length=50)
    private String descricao = "";

    @Enumerated(EnumType.STRING)
    @Column(name="tipo", length=1)
    private TipoLinha tipo = null;

    @Column(name="assinatura", precision=10, scale=2)
    private Double assinatura = 0.00;

    @Column(name="intragrupo", precision=10, scale=2)
    private Double intra = 0.00;

    @Column(name="gestor", precision=10, scale=2)
    private Double gestor = 0.00;

    @Column(name="franquia", precision=10, scale=2)
    private Double franquia = 0.00;

    @Column(name="adm_terceiro", precision=10, scale=2)
    private Double terceiro = 0.00;

    @Column(name="adm_associacao", precision=10, scale=2)
    private Double administracao = 0.00;

    @Column(name="minutos")
    private int minutos = 0;

    @Column(name="adicional", precision=10, scale=2)
    private Double adicional = 0.00;

    @Column(name="compartilha")
    private boolean compartilha = false;

    /*@Enumerated(EnumType.STRING)
    @Column(name="operadora", length=1)
    private Operadora operadora;*/

    public boolean isCompartilha() {
        return compartilha;
    }

    public void setCompartilha(boolean compartilha) {
        this.compartilha = compartilha;
    }

    public Double getAdicional() {
        return adicional;
    }

    public void setAdicional(Double adicional) {
        this.adicional = adicional;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public Double getAdministracao() {
        return administracao;
    }

    public void setAdministracao(Double administracao) {
        this.administracao = administracao;
    }

    public Double getAssinatura() {
        return assinatura;
    }

    public void setAssinatura(Double assinatura) {
        this.assinatura = assinatura;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getFranquia() {
        return franquia;
    }

    public void setFranquia(Double franquia) {
        this.franquia = franquia;
    }

    public Double getGestor() {
        return gestor;
    }

    public void setGestor(Double gestor) {
        this.gestor = gestor;
    }

    public Double getIntra() {
        return intra;
    }

    public void setIntra(Double intra) {
        this.intra = intra;
    }

    public Double getTerceiro() {
        return terceiro;
    }

    public void setTerceiro(Double terceiro) {
        this.terceiro = terceiro;
    }

    public TipoLinha getTipo() {
        return tipo;
    }

    public void setTipo(TipoLinha tipo) {
        this.tipo = tipo;
    }

    public Double total(){
        return assinatura + intra + terceiro + administracao + gestor;
    }

    /*public Operadora getOperadora() {
        return operadora;
    }

    public void setOperadora(Operadora operadora) {
        this.operadora = operadora;
    }*/


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plano other = (Plano) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.codigo;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Plano{" + "codigo=" + codigo + "descricao=" + descricao + "tipo=" + tipo + "assinatura=" + assinatura + "intra=" + intra + "gestor=" + gestor + "franquia=" + franquia + "terceiro=" + terceiro + "administracao=" + administracao + "minutos=" + minutos + "adicional=" + adicional + "compartilha=" + compartilha + '}';
    }


}
