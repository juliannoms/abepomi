/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name = "extras", schema = "public")
@IdClass(ExtrasPk.class)
public class Extras implements Serializable {

    public Extras() {
    }

    public Extras(String descricao, Double valor) {
        this.descricao = descricao;
        this.valor = valor;
    }

    

    @Id
    private int id;

    @Id
    private Associado associado;

    @Id
    private Referencia referencia;

    @Column(name="descricao")
    private String descricao;

    @Column(name="valor", precision=10, scale=2)
    private Double valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Extras other = (Extras) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.associado != other.associado && (this.associado == null || !this.associado.equals(other.associado))) {
            return false;
        }
        if (this.referencia != other.referencia && (this.referencia == null || !this.referencia.equals(other.referencia))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        hash = 79 * hash + (this.associado != null ? this.associado.hashCode() : 0);
        hash = 79 * hash + (this.referencia != null ? this.referencia.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Extras{" + "id=" + id + "associado=" + associado + "referencia=" + referencia + "descricao=" + descricao + "valor=" + valor + '}';
    }

}
