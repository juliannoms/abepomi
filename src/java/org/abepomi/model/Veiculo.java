/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@SequenceGenerator(name="seqvei", sequenceName="seq_veiculos", allocationSize=1)
@NamedQueries({
    @NamedQuery(name="VeiculosPorAssociado", query="select v from Veiculo v where v.associado.status = org.abepomi.model.util.Status.A order by v.associado.nome")
})
@Table(name="veiculos", schema="public")
public class Veiculo implements Serializable{

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqvei")
    private int id;

    @Column(name="placas", length=7)
    private String placas;

    @Column(name="marca", length=30)
    private String marca;

    @Column(name="modelo", length=30)
    private String modelo;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ass_id", referencedColumnName="id")
    private Associado associado;

    public Veiculo() {
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Veiculo other = (Veiculo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        return hash;
    }



    @Override
    public String toString() {
        return "org.abepomi.model.Veiculo";
    }



}
