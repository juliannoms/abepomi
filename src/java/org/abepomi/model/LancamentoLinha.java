/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="lancamentos", schema="public")
@NamedQueries({

})
@IdClass(org.abepomi.model.LancamentoLinhaPK.class)
public class LancamentoLinha implements Serializable{

    public LancamentoLinha() {
    }

    @EmbeddedId
    private Linha linha;

    @EmbeddedId
    private Fatura fatura;

    @EmbeddedId
    private int codigo;

    @Column(name="tipo", length=100)
    private String tipoLancamento = "";

    @Temporal(TemporalType.DATE)
    @Column(name="data")
    private Date data = null;

    @Temporal(TemporalType.TIME)
    @Column(name="hora")
    private Date hora = null;

    @Column(name="origem_destino", length=100)
    private String origem = "";

    @Column(name="nro_destino", length=30)
    private String numeroDestino = "";

    @Column(name="duracao_qtde", precision=10, scale=2)
    private Double duracao = 0.00;

    @Column(name="tarifa", precision=10, scale=2)
    private Double tarifa = 0.00;

    @Column(name="valor", precision=10, scale=2)
    private Double valor = 0.00;

    @Column(name="cobrado", precision=10, scale=2)
    private Double cobrado = 0.00;

    @Column(name="nome", length=50)
    private String nome = "";

    @Column(name="cc", length=50)
    private String contaCorrente = "";

    @Column(name="matricula", length=50)
    private String matricula = "";

    @Column(name="descricao", length=100)
    private String descricao = "";

    public Double getCobrado() {
        return cobrado;
    }

    public void setCobrado(Double cobrado) {
        this.cobrado = cobrado;
    }

   public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(String contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getDuracao() {
        return duracao;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public Fatura getFatura() {
        return fatura;
    }

    public void setFatura(Fatura fatura) {
        this.fatura = fatura;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroDestino() {
        return numeroDestino;
    }

    public void setNumeroDestino(String numeroDestino) {
        this.numeroDestino = numeroDestino;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Double getTarifa() {
        return tarifa;
    }

    public void setTarifa(Double tarifa) {
        this.tarifa = tarifa;
    }

    public String getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(String tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LancamentoLinha other = (LancamentoLinha) obj;
        if (this.linha != other.linha && (this.linha == null || !this.linha.equals(other.linha))) {
            return false;
        }
        if (this.fatura != other.fatura && (this.fatura == null || !this.fatura.equals(other.fatura))) {
            return false;
        }
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.linha != null ? this.linha.hashCode() : 0);
        hash = 71 * hash + (this.fatura != null ? this.fatura.hashCode() : 0);
        hash = 71 * hash + this.codigo;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.LancamentoLinha{" + "linha=" + linha + "fatura=" + fatura + "codigo=" + codigo + "tipoLancamento=" + tipoLancamento + "data=" + data + "hora=" + hora + "origem=" + origem + "numeroDestino=" + numeroDestino + "duracao=" + duracao + "tarifa=" + tarifa + "valor=" + valor + "cobrado=" + cobrado + "nome=" + nome + "contaCorrente=" + contaCorrente + "matricula=" + matricula + "descricao=" + descricao + '}';
    }

}
