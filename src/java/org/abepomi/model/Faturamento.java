/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name = "faturamento", schema = "public")
@IdClass(FaturamentoPk.class)
public class Faturamento implements Serializable {

    public Faturamento() {
    }

    @EmbeddedId
    private Associado associado;
    
    @EmbeddedId
    private Referencia referencia;

    @Column(name="valor_servicos", precision=10, scale=2 )
    private Double servicos;

    @Column(name="valor_mensalidade", precision=10, scale=2)
    private Double mensalidade;

    @Column(name="valor_telefonia", precision=10, scale=2)
    private Double telefonia;

    @Column(name="valor_extra", precision=10, scale=2)
    private Double extra;

    @Column(name="valor_vivo")
    private Double vivo;

    @Column(name="valor_parcelas")
    private BigDecimal parcelas = new BigDecimal(BigInteger.ZERO);

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public Double getExtra() {
        return extra;
    }

    public void setExtra(Double extra) {
        this.extra = extra;
    }

    public Double getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(Double mensalidade) {
        this.mensalidade = mensalidade;
    }

    public Double getServicos() {
        return servicos;
    }

    public void setServicos(Double servicos) {
        this.servicos = servicos;
    }

    public Double getTelefonia() {
        return telefonia;
    }

    public void setTelefonia(Double telefonia) {
        this.telefonia = telefonia;
    }

    public Double getVivo() {
        return vivo;
    }

    public void setVivo(Double vivo) {
        this.vivo = vivo;
    }

    public BigDecimal getParcelas() {
        return parcelas;
    }

    public void setParcelas(BigDecimal parcelas) {
        this.parcelas = parcelas;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Faturamento other = (Faturamento) obj;
        if (this.associado != other.associado && (this.associado == null || !this.associado.equals(other.associado))) {
            return false;
        }
        if (this.referencia != other.referencia && (this.referencia == null || !this.referencia.equals(other.referencia))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.associado != null ? this.associado.hashCode() : 0);
        hash = 97 * hash + (this.referencia != null ? this.referencia.hashCode() : 0);
        return hash;
    }

    
   

    @Override
    public String toString() {
        return "org.abepomi.model.Faturamento{" + "associado=" + associado + "referencia=" + referencia + "servicos=" +
                servicos + "mensalidade=" + mensalidade + "telefonia=" + telefonia + "extra=" + extra + "vivo = "+ vivo +"}";
    }


}
