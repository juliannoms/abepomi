/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 *
 * @author Julianno
 */
@Entity
@Table(name="detalhe", schema="public")
@SequenceGenerator(name="seqdetalhe", sequenceName="seq_detalhe", allocationSize=1)
public class DetalheTelefonia implements Serializable {

    public DetalheTelefonia() {
    }

    @Id
    @GeneratedValue(generator="seqdetalhe", strategy=GenerationType.SEQUENCE)
    private int id = 0;

    @Column(name="linha", length=10)
    private String linha = "";

    @Column(name="fatura")
    private Integer fatura = null;

    @Column(name="associado")
    private Integer associado = null;
    
    @Column(name="plano", precision=10, scale=2)
    private Double plano = 0.00;

    @Column(name="pacotes", precision=10, scale=2)
    private Double pacotes = 0.00;

    @Column(name="outros", precision=10, scale=2)
    private Double outros = 0.00;

    @Column(name="franquia", precision=10, scale=2)
    private Double franquia = 0.00;

    @Column(name="excedidos", precision=10, scale=2)
    private Double excedidos = 0.00;

    @Column(name="valor_minuto", precision=10, scale=2)
    private Double valor_minuto = 0.00;

    @Column(name="credito", precision=10, scale=2)
    private Double credito = 0.00;

    @Column(name="debito", precision=10, scale=2)
    private Double debito = 0.00;

    @Column(name="aparelho", precision=10, scale=2)
    private Double aparelho = 0.00;

    @Column(name="habilitacao", precision=10, scale=2)
    private Double habilitacao = 0.00;

    @Column(name="desc_plano", length=50)
    private String descricaoPlano = "";

    @Column(name="min_locais", precision=10, scale=2)
    private Double minutosLocais = 0.00;

    @Column(name="desconto", precision=10, scale=2)
    private Double desconto = 0.00;

    @Column(name="acrescimo", precision=10, scale=2)
    private Double acrescimo = 0.00;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="fechamento", referencedColumnName="id")
    private FechamentoTelefonia fechamento = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getFatura() {
        return fatura;
    }

    public void setFatura(Integer fatura) {
        this.fatura = fatura;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Integer getAssociado() {
        return associado;
    }

    public void setAssociado(Integer associado) {
        this.associado = associado;
    }

    public Double getCredito() {
        return credito;
    }

    public void setCredito(Double credito) {
        this.credito = credito;
    }

    public Double getDebito() {
        return debito;
    }

    public void setDebito(Double debito) {
        this.debito = debito;
    }

    public Double getExcedidos() {
        return excedidos;
    }

    public void setExcedidos(Double excedidos) {
        this.excedidos = excedidos;
    }

    public Double getFranquia() {
        return franquia;
    }

    public void setFranquia(Double franquia) {
        this.franquia = franquia;
    }

    public Double getOutros() {
        return outros;
    }

    public void setOutros(Double outros) {
        this.outros = outros;
    }

    public Double getPacotes() {
        return pacotes;
    }

    public void setPacotes(Double pacotes) {
        this.pacotes = pacotes;
    }

    public Double getPlano() {
        return plano;
    }

    public void setPlano(Double plano) {
        this.plano = plano;
    }

    public Double getValor_minuto() {
        return valor_minuto;
    }

    public void setValor_minuto(Double valor_minuto) {
        this.valor_minuto = valor_minuto;
    }

    public FechamentoTelefonia getFechamento() {
        return fechamento;
    }

    public void setFechamento(FechamentoTelefonia fechamento) {
        this.fechamento = fechamento;
    }

    public Double getAparelho() {
        return aparelho;
    }

    public void setAparelho(Double aparelho) {
        this.aparelho = aparelho;
    }

    public Double getHabilitacao() {
        return habilitacao;
    }

    public void setHabilitacao(Double habilitacao) {
        this.habilitacao = habilitacao;
    }

    public String getDescricaoPlano() {
        return descricaoPlano;
    }

    public void setDescricaoPlano(String descricaoPlano) {
        this.descricaoPlano = descricaoPlano;
    }

    public Double getMinutosLocais() {
        return minutosLocais;
    }

    public void setMinutosLocais(Double minutosLocais) {
        this.minutosLocais = minutosLocais;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Double getAcrescimo() {
        return acrescimo;
    }

    public void setAcrescimo(Double acrescimo) {
        this.acrescimo = acrescimo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalheTelefonia other = (DetalheTelefonia) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.DetalheTelefonia{" + "id=" + id +'}';
    }
    
}
