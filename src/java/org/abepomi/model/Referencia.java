/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="referencia", schema="public")
@SequenceGenerator(name="seqref", sequenceName="seq_referencia")
@NamedQueries({
    @NamedQuery(name="RefSemFaturamento", query="select r from Referencia r where r.faturado = 'false'"),
    @NamedQuery(name="findByRef", query="select r from Referencia r where r.ref = :referencia"),
    @NamedQuery(name="RefSemRemessa", query="select r from Referencia r where r.faturado = 'true' and r not in (select rem.ref from RemDA rem)"),
    @NamedQuery(name="RefTodasDesc", query="select r from Referencia r order by r.id desc"),
    @NamedQuery(name="Referencia.buscarUltima", query="select r from Referencia r where r.id = (select MAX(ref.id) from Referencia ref)")
})
public class Referencia implements Serializable {

    public Referencia() {
    }

    @Id @GeneratedValue(generator="seqref", strategy=GenerationType.AUTO)
    private int id;

    @Column(name="mesref", length=6)
    private String ref;

    @Column(name="dtgeracao")
    @Temporal(TemporalType.DATE)
    private Date geracao;

    @Column(name="faturado")
    private boolean faturado = false;

    public boolean isFaturado() {
        return faturado;
    }

    public void setFaturado(boolean faturado) {
        this.faturado = faturado;
    }

    public Date getGeracao() {
        return geracao;
    }

    public void setGeracao(Date geracao) {
        this.geracao = geracao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Referencia other = (Referencia) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Referencia";
    }



}
