/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.abepomi.model.util.TipoLinha;

/**
 *
 * @author Julianno
 */
@Entity
@NamedQueries({
    @NamedQuery(name="LinhaVivo.livres", query="select l from LinhaVivo l where l.linha not in (select av.linha from AssociadoVivo av where av.devolucao is null)")
})
@Table(name="linhas_vivo", schema="public")
public class LinhaVivo implements Serializable{

    @Id
    private String linha = null;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "contrato_id", referencedColumnName = "id")
    private ContratoVivo contrato = null;

    @Enumerated(EnumType.STRING)
    private TipoLinha tipo = null;

    public ContratoVivo getContrato() {
        return contrato;
    }

    public void setContrato(ContratoVivo contrato) {
        this.contrato = contrato;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public TipoLinha getTipo() {
        return tipo;
    }

    public void setTipo(TipoLinha tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LinhaVivo other = (LinhaVivo) obj;
        if ((this.linha == null) ? (other.linha != null) : !this.linha.equals(other.linha)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.linha != null ? this.linha.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.vivo.LinhaVivo{" + "linha=" + linha + "contrato=" + contrato.getNumero() + "tipo=" + tipo + '}';
    }



}
