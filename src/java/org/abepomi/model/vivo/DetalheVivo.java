/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="detalhe_vivo", schema="public")
@SequenceGenerator(name="seq_detalhevivo", sequenceName="seq_detalhe_vivo", allocationSize=1, initialValue=1)
public class DetalheVivo implements Serializable {

    @Id
    @GeneratedValue(generator="seq_detalhevivo", strategy=GenerationType.SEQUENCE)
    private int id;
    private Double grupo = null;
    private Double local = null;
    private Double mensagem = null;
    private Double outros = null;
    private Double administracao = null;
    private Double repasse = null;
    private Double pacote = null;
    private Double extras = null;
    private String linha = null;
    @Column(name="fatura_id")
    private Integer fatura = null;

    public Double getAdministracao() {
        return administracao;
    }

    public void setAdministracao(Double administracao) {
        this.administracao = administracao;
    }

    public Double getExtras() {
        return extras;
    }

    public void setExtras(Double extras) {
        this.extras = extras;
    }

    public Integer getFatura() {
        return fatura;
    }

    public void setFatura(Integer fatura) {
        this.fatura = fatura;
    }

    public Double getGrupo() {
        return grupo;
    }

    public void setGrupo(Double grupo) {
        this.grupo = grupo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Double getLocal() {
        return local;
    }

    public void setLocal(Double local) {
        this.local = local;
    }

    public Double getMensagem() {
        return mensagem;
    }

    public void setMensagem(Double mensagem) {
        this.mensagem = mensagem;
    }

    public Double getOutros() {
        return outros;
    }

    public void setOutros(Double outros) {
        this.outros = outros;
    }

    public Double getPacote() {
        return pacote;
    }

    public void setPacote(Double pacote) {
        this.pacote = pacote;
    }

    public Double getRepasse() {
        return repasse;
    }

    public void setRepasse(Double repasse) {
        this.repasse = repasse;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalheVivo other = (DetalheVivo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.vivo.DetalheVivo{" + "id=" + id + "grupo=" + grupo + "local=" + local + "mensagem=" + mensagem + "outros=" + outros + "administracao=" + administracao + "repasse=" + repasse + "pacote=" + pacote + "extras=" + extras + "linha=" + linha + "fatura=" + fatura + '}';
    }



}
