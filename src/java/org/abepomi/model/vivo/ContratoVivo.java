/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="contratos_vivo", schema="public")
@NamedQuery(name="ContratoVivo.buscaPorContrato", query="select c from ContratoVivo c where c.numero like :numero")
@SequenceGenerator(name="seq_contratosvivo", sequenceName="seq_contratos_vivo", allocationSize=1, initialValue=1)
public class ContratoVivo implements Serializable {

    @Id
    @GeneratedValue(generator="seq_contratosvivo", strategy=GenerationType.SEQUENCE)
    private int id;
    private String numero = null;
    private String descricao = null;
    private int ciclo;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContratoVivo other = (ContratoVivo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.vivo.ContratoVivo{" + "id=" + id + "numero=" + numero + "descricao=" + descricao + '}';
    }



}
