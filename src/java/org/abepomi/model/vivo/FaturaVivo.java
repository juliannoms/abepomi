/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="faturas_vivo", schema="public")
@SequenceGenerator(name="seq_faturasvivo", allocationSize=1, initialValue=1, sequenceName="seq_faturas_vivo")
public class FaturaVivo implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_faturasvivo")
    private int id;

    @Temporal(TemporalType.DATE)
    private Date emissao = null;

    @Temporal(TemporalType.DATE)
    private Date vencimento = null;
    private Double total = null;

    @Column(name="contrato_id")
    private Integer contrato = null;

    private boolean faturada = false;

    @Column(name="referencia_id")
    Integer referencia = null;

    public FaturaVivo() {
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Integer getContrato() {
        return contrato;
    }

    public void setContrato(Integer contrato) {
        this.contrato = contrato;
    }

    public boolean isFaturada() {
        return faturada;
    }

    public void setFaturada(boolean faturada) {
        this.faturada = faturada;
    }

    public Integer getReferencia() {
        return referencia;
    }

    public void setReferencia(Integer referencia) {
        this.referencia = referencia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FaturaVivo other = (FaturaVivo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.vivo.FaturaVivo{" + "id=" + id + "emissao=" + emissao + "vencimento=" + vencimento + "total=" + total + "contrato=" + contrato + "faturada=" + faturada + "referencia=" + referencia + '}';
    }
}
