/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(schema="public", name="associados_vivo")
@NamedQueries({
    @NamedQuery(name="AssociadoVivo.buscaLinhaPorAssociadoId", query="select a.linha from AssociadoVivo a where a.ass_id = :ass_id and a.devolucao is null"),
    @NamedQuery(name="AssociadoVivo.buscarAssComLinha", query="select distinct a from Associado a, AssociadoVivo av where a.id = av.ass_id and av.devolucao is null order by a.nome")
})
@SequenceGenerator(name="seq_assvivo", sequenceName="seq_associados_vivo", allocationSize=1, initialValue=1)
public class AssociadoVivo implements Serializable {

    @Id
    @GeneratedValue(generator="seq_assvivo", strategy=GenerationType.SEQUENCE)
    private int id;
    private int ass_id;
    private String linha;

    @Temporal(TemporalType.DATE)
    private Date entrega;

    @Temporal(TemporalType.DATE)
    private Date devolucao;

    public AssociadoVivo() {
        this.ass_id = 0;
        this.linha = null;
        this.entrega = null;
        this.devolucao = null;
    }

    public int getAss_id() {
        return ass_id;
    }

    public void setAss_id(int ass_id) {
        this.ass_id = ass_id;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssociadoVivo other = (AssociadoVivo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.vivo.AssociadoVivo{" + "id=" + id + "ass_id=" + ass_id + "linha=" + linha + "entrega=" + entrega + "devolucao=" + devolucao + '}';
    }


}
