/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.vivo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="resumos_vivo", schema="public")
@SequenceGenerator(name="seq_resumosvivo", sequenceName="seq_resumos_vivo", allocationSize=1,initialValue=1)
public class ResumoVivo implements Serializable {

    @Id
    @GeneratedValue(generator="seq_resumosvivo",strategy=GenerationType.SEQUENCE)
    private int id;
    private String linha  = null;
    private String tipo = null;
    private String item = null;
    private Double contratado = null;
    private Double utilizados = null;
    private Double excedentes = null;
    private Double cobrado = null;

    @Column(name="fatura_id")
    private int fatura;

    public ResumoVivo() {
    }

    public Double getCobrado() {
        return cobrado;
    }

    public void setCobrado(Double cobrado) {
        this.cobrado = cobrado;
    }

    public Double getContratado() {
        return contratado;
    }

    public void setContratado(Double contratado) {
        this.contratado = contratado;
    }

    public Double getExcedentes() {
        return excedentes;
    }

    public void setExcedentes(Double excedentes) {
        this.excedentes = excedentes;
    }

    public int getFatura() {
        return fatura;
    }

    public void setFatura(int fatura) {
        this.fatura = fatura;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getUtilizados() {
        return utilizados;
    }

    public void setUtilizados(Double utilizados) {
        this.utilizados = utilizados;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResumoVivo other = (ResumoVivo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + this.id;
        return hash;
    }
    

}
