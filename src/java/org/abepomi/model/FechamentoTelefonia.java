/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="fechamento", schema="public")
@SequenceGenerator(name="seqfechamento", sequenceName="seq_fechamento", allocationSize=1)
public class FechamentoTelefonia implements Serializable{

    public FechamentoTelefonia() {
    }

    @Id
    @GeneratedValue(generator="seqfechamento",  strategy=GenerationType.SEQUENCE)
    private int id = 0;

    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="referencia", referencedColumnName="id")
    private Referencia referencia = null;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="fechamento")
    private List<DetalheTelefonia> detalhamento = new ArrayList<DetalheTelefonia>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<DetalheTelefonia> getDetalhamento() {
        return detalhamento;
    }

    public void setDetalhamento(List<DetalheTelefonia> detalhamento) {
        this.detalhamento = detalhamento;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FechamentoTelefonia other = (FechamentoTelefonia) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.FechamentoTelefonia{" + "id=" + id + "referencia=" + referencia + "detalhamento=" + detalhamento + '}';
    }

    



}
