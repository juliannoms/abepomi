/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */

@Entity
@NamedQuery(name="MaxNSA", query="select max(r.nsa) from RetDA r")
@SequenceGenerator(name="seqret", sequenceName="seq_retorno", allocationSize=1)
@Table(name="aut_retorno", schema="public")
public class RetDA implements Serializable {

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqret")
    private int id;

    @Column(name="nsa")
    private int nsa;

    @Temporal(TemporalType.DATE)
    @Column(name="dtgeracao")
    Date geracao;

    @Column(name="valor")
    Double valor = 0.00;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="aut_id")
    ConvDA convenio;

    public RetDA() {
    }

    public ConvDA getConvenio() {
        return convenio;
    }

    public void setConvenio(ConvDA convenio) {
        this.convenio = convenio;
    }

    public Date getGeracao() {
        return geracao;
    }

    public void setGeracao(Date geracao) {
        this.geracao = geracao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNsa() {
        return nsa;
    }

    public void setNsa(int nsa) {
        this.nsa = nsa;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RetDA other = (RetDA) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.RetDA";
    }


    
}
