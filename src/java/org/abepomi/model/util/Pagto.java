/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum Pagto {

    D("Débito Automático");

    String pagto;

    Pagto(String pagto){
        this.pagto = pagto;
    }

    public String getPagto() {
        return pagto;
    }

    @Override
    public String toString() {
        return this.pagto;
    }



}
