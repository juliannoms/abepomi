/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum TipoPessoa {

    /*
     * public enum TipoPessoa{Fisica, Juridica}
     */

    F("Física"),
    J("Jurídica");

    String tipoPessoa;

    TipoPessoa(String tipoPessoa){
        this.tipoPessoa = tipoPessoa;
    }

    public String getTipoPessoa(){
        return this.tipoPessoa;
    }

    @Override
    public String toString() {
        return this.tipoPessoa;
    }



}
