/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum EstadoDA {
 //public enum Opcao{Emitido, Enviado, Cadastrado, Excluido, Recusado}

    E("Emitido"),
    N("Enviado"),
    C("Cadastrado"),
    X("Excluído"),
    R("Recusado");

    String estadoDA;

    EstadoDA(String estadoDA){
        this.estadoDA = estadoDA;
    }

    public String getEstadoDA(){
        return this.estadoDA;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.util.EstadoDA";
    }



}
