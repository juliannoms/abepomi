/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public class Validacao {

    public static final int NUMERICO = 0;
    public static final int ALFA = 1;

    public static String validaTamanho(int tipo, String campo, int tamanho) {
        if (campo.length() > tamanho) {
            return campo.substring(0, tamanho);
        } else {
            int cont = tamanho - campo.length();
            if (tipo == NUMERICO) {
                for (int i = 0; i < cont; i++) {
                    campo = "0" + campo;
                }
            } else {
                if (tipo == ALFA) {
                    for (int i = 0; i < cont; i++) {
                        campo = campo + " ";
                    }
                }
            }
            return campo;
        }
    }

    public static String geraDigitoVerificador(String id) {
        int dv = 0;

        if (id.length() == 5) {
            int i = 0;
            while (i < 5) {
                dv = dv + Integer.parseInt(id.substring(i, i + 1)) * (9 - i);
                i++;
            }
            dv = dv % 11;
            if (dv == 11) {
                dv = 0;
            } else if (dv == 10) {
                dv = 1;
            }
        }

        return String.valueOf(dv);
    }

    //Valida o CPF
    public static boolean validaCpf(String cpf) {
        int soma = 0;
        if (cpf.length() == 11) {
            for (int i = 0; i < 9; i++) {
                soma += (10 - i) * (cpf.charAt(i) - '0');
            }
            soma = 11 - (soma % 11);
            if (soma > 9) {
                soma = 0;
            }
            if (soma == (cpf.charAt(9) - '0')) {
                soma = 0;
                for (int i = 0; i < 10; i++) {
                    soma += (11 - i) * (cpf.charAt(i) - '0');
                }
                soma = 11 - (soma % 11);
                if (soma > 9) {
                    soma = 0;
                }
                if (soma == (cpf.charAt(10) - '0')) {
                    //JOptionPane.showMessageDialog(null, "CPF Válido.","Confirmação",JOptionPane.INFORMATION_MESSAGE);
                    return true;
                }
            }
        }
        //JOptionPane.showMessageDialog(null, "CPF Inválido.","Erro",JOptionPane.ERROR_MESSAGE);
        return false;
    }

    //Valida CNPJ
    public static boolean validaCnpj(String cnpj) {
        int soma = 0;

        if (cnpj.length() == 14) {
            for (int i = 0, j = 5; i < 12; i++) {
                soma += j-- * (cnpj.charAt(i) - '0');
                if (j < 2) {
                    j = 9;
                }
            }
            soma = 11 - (soma % 11);
            if (soma > 9) {
                soma = 0;
            }
            if (soma == (cnpj.charAt(12) - '0')) {
                soma = 0;
                for (int i = 0, j = 6; i < 13; i++) {
                    soma += j-- * (cnpj.charAt(i) - '0');
                    if (j < 2) {
                        j = 9;
                    }
                }
                soma = 11 - (soma % 11);
                if (soma > 9) {
                    soma = 0;
                }
                if (soma == (cnpj.charAt(13) - '0')) {
                    //JOptionPane.showMessageDialog(null, "CNPJ Válido.","Confirmação",JOptionPane.INFORMATION_MESSAGE);
                    return true;
                }
            }
        }
        //JOptionPane.showMessageDialog(null, "CNPJ Inválido.","Erro",JOptionPane.ERROR_MESSAGE);
        return false;
    }
}
