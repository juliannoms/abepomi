/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum Parentesco{
    C("Conjuge"),
    F("Filho(a)"),
    P("Pai"),
    M("Mãe"),
    E("Enteado(a)"),
    S("Sogro(a)");

    private String parentesco;

    Parentesco(String parentesco){
        this.parentesco = parentesco;
    }

    public String getParentesco(){
        return this.parentesco;
    }

    @Override
    public String toString(){
        return this.parentesco;
    }

}