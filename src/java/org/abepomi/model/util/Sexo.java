/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum Sexo {
    M("Masculino"),
    F("Feminino");

    String sexo;

    Sexo(String sexo){
        this.sexo = sexo;
    }

    public String getSexo(){
        return this.sexo;
    }

    @Override
    public String toString(){
        return this.sexo;
    }
}
