/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public class Mascarar {

    public static String colocaMascara(String mascara, String valor){
        if(valor == null || valor.equals("")){
            return "";
        }
        String novo = "";
        int v = 0;
        for (int i=0; i<mascara.length(); i++){
            if(mascara.substring(i, i+1).equals("#")){
                novo = novo + valor.substring(v, v+1);
                v++;
            }else{
                novo = novo + mascara.substring(i, i+1);
            }
        }
        return novo;
    }

}
