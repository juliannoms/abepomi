/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum Categoria {

    B("Benemérito"),
    C("Convidado"),
    F("Fundador"),
    T("Titular"),
    O("Colaborador"),
    V("Conveniado");
    


    String categoria;

    Categoria(String categoria){
        this.categoria = categoria;
    }

    public String getCategoria(){
        return this.categoria;
    }

    @Override
    public String toString(){
        return this.categoria;
    }

    //public enum Categoria{Titular, Fundador, Benemerito, Convidado}

}
