/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum Operadora {

    C("Claro"),
    V("Vivo");


    String operadora;

    Operadora(String operadora){
        this.operadora = operadora;
    }

    public String getOperadora(){
        return this.operadora;
    }

    @Override
    public String toString(){
        return this.operadora;
    }

}