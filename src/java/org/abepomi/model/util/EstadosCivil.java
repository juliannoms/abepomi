/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum EstadosCivil {
    C("Casado(a)"),
    D("Divorciado(a)"),
    P("Separado(a)"),
    J("Separado(a) Judicialmente"),
    S("Solteiro(a)"),
    U("União Estável"),
    V("Viúvo(a)");

    String estadoCivil;
    EstadosCivil(String estadoCivil){
        this.estadoCivil = estadoCivil;
    }

    public String getEstadoCivil(){
        return this.estadoCivil;
    }

    @Override
    public String toString(){
        return this.estadoCivil;
    }
}
