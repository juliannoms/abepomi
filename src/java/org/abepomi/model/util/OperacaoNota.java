/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum OperacaoNota {

    E("Entrada"),
    S("Saída");

    String operacaoNota;

    OperacaoNota(String operacaoNota){
        this.operacaoNota = operacaoNota;
    }

    public String getOperacaoNota() {
        return operacaoNota;
    }

    @Override
    public String toString() {
        return operacaoNota;
    }



}
