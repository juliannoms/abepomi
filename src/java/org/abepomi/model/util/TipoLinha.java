/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.util;

/**
 *
 * @author Julianno
 */
public enum TipoLinha {

    D("DADOS"),
    V("VOZ");

    String tipoLinha;

    TipoLinha(String tipoLinha){
        this.tipoLinha = tipoLinha;
    }

    public String getTipoLinha(){
        return this.tipoLinha;
    }

    @Override
    public String toString(){
        return getTipoLinha();
    }

}
