/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name = "aparelhos", schema = "public")
@SequenceGenerator(name = "seqaparelhos", sequenceName = "seq_aparelhos", allocationSize = 1)
@NamedQueries({
    @NamedQuery(name = "Aparelho.estoque", query = "select a from Aparelho a where a.estoque > 0 order by a.nome")
})
public class Aparelho implements Serializable {

    @Id
    @GeneratedValue(generator = "seqaparelhos", strategy = GenerationType.SEQUENCE)
    private int id;
    @Column(name = "nome", length = 50)
    private String nome = null;
    @Column(name = "estoque")
    private int estoque = 0;
    @Column(name = "preco_medio")
    private Double valorMedio = null;
    @Column(name = "preco_venda")
    private Double valorVenda = null;

    public Aparelho() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public Double getValorMedio() {
        return valorMedio;
    }

    public void setValorMedio(Double valorMedio) {
        this.valorMedio = valorMedio;
    }

    public Double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(Double valorVenda) {
        this.valorVenda = valorVenda;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aparelho other = (Aparelho) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Aparelho{" + "id=" + id + "nome=" + nome + "estoque=" + estoque + "valorMedio=" + valorMedio + "valorVenda=" + valorVenda + '}';
    }
}
