/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="faturas", schema="public")
@NamedQueries({
    @NamedQuery(name="buscaNFaturados", query="select new org.abepomi.dao.util.FaturaUtil(fat.codigo, fat.inicio, fat.fim) from Fatura fat where fat.faturada = 'false'"),
    @NamedQuery(name="semFaturar", query="select count(fat) from Fatura fat where fat.faturada = 'false'"),
    @NamedQuery(name="Fatura.custoPorLinha", query="select new org.abepomi.dao.util.FaturaUtilRelCusto(l.linha.numero, f.codigo, f.inicio, f.fim, f.nota, f.vencimento, sum(l.cobrado))"+
                    "from Fatura f, LancamentoLinha l where f = l.fatura group by l.linha.numero, f.codigo, f.inicio, f.fim, f.nota, f.vencimento"
                    + " order by f, l.linha")
})
@SequenceGenerator(name="seqfatura", sequenceName="seq_faturas", allocationSize=1)
public class Fatura implements Serializable{

    public Fatura() {
    }

    @Id @GeneratedValue(generator="seqfatura", strategy=GenerationType.AUTO)
    @Column(name="codigo")
    private int codigo = 0;

    @Column(name="vencimento")
    @Temporal(TemporalType.DATE)
    private Date vencimento = null;

    @Column(name="inicio")
    @Temporal(TemporalType.DATE)
    private Date inicio = null;

    @Column(name="fim")
    @Temporal(TemporalType.DATE)
    private Date fim = null;

    @Column(name="notafiscal", length=20)
    private String nota = "";

    @Column(name="valor", precision=10, scale=2)
    private Double valor = 0.00;

    @Column(name="faturada")
    private boolean faturada = false;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name="contrato", referencedColumnName="codigo")
    private Contrato contrato = null;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="fatura")
    private List<LancamentoLinha> lancamentos = null;

    public boolean isFaturada() {
        return faturada;
    }

    public void setFaturada(boolean faturada) {
        this.faturada = faturada;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<LancamentoLinha> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(List<LancamentoLinha> lancamentos) {
        this.lancamentos = lancamentos;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fatura other = (Fatura) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.codigo;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Fatura";
    }


}
