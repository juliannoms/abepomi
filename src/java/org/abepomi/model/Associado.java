/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import org.abepomi.model.util.EstadosCivil;
import org.abepomi.model.util.Categoria;
import org.abepomi.model.util.Sexo;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.util.Pagto;
import org.abepomi.model.util.Status;

/**
 *
 * @author Julianno
 */
@Entity
@SequenceGenerator(name = "seqass", sequenceName = "seq_associados", allocationSize = 1)
@Table(name = "associados", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Associado.findAllByNome", query = "select associado from Associado associado order by associado.nome"),
    @NamedQuery(name="Associado.assEmail", query="select NEW org.abepomi.dao.util.AssociadoEmail(ass.id, ass.nome, ass.cpf, ass.email1||','||ass.email2) from Associado ass where ass.status = 'A' order by ass.id")
})
public class Associado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "seqass")
    private int id;
    @Column(name = "nome", length = 50)
    private String nome;
    @Column(name = "cpf", length = 11)
    private String cpf;
    @Column(name = "rg", length = 14)
    private String rg;
    @Temporal(TemporalType.DATE)
    @Column(name = "nascimento")
    private Date nascimento;
    @Column(name = "filhos")
    private int qtdeFilhos;
    @Column(name = "posto", length = 50)
    private String posto;
    @Column(name = "opm", length = 50)
    private String opm;
    @Temporal(TemporalType.DATE)
    @Column(name = "admissao")
    private Date admissao;
    @Column(name = "re", length = 10)
    private String re;
    @Column(name = "telefone", length = 10)
    private String telefone;
    @Column(name = "telefone_opm", length = 10)
    private String telefoneOpm;
    @Column(name = "celular_1", length = 10)
    private String celular1;
    @Column(name = "celular_2", length = 10)
    private String celular2;
    @Column(name = "email_1", length = 50)
    private String email1;
    @Column(name = "email_2", length = 50)
    private String email2;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "estadocivil")
    private EstadosCivil estado;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "categoria")
    private Categoria categoria;
    @Enumerated(EnumType.STRING)
    @Column(name = "pgto", length = 1)
    private Pagto formaPgto;
    @Enumerated(EnumType.STRING)
    @Column(name = "sexo", length = 1)
    private Sexo sexo;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "end_opm", referencedColumnName = "id")
    private Endereco endOpm = new Endereco();
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "end_residencial", referencedColumnName = "id")
    private Endereco endereco = new Endereco();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "associado")
    private List<Dependente> dependentes = new ArrayList<Dependente>();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "associado")
    private List<Veiculo> veiculos = new ArrayList<Veiculo>();
    @ManyToMany
    @JoinTable(name = "ass_serv", joinColumns = {
        @JoinColumn(name = "ass_id")}, inverseJoinColumns = {
        @JoinColumn(name = "ser_id")})
    private List<Servico> servicos = new ArrayList<Servico>();
    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 1)
    private Status status = Status.A;
    @Temporal(TemporalType.DATE)
    @Column(name = "inclusao")
    private Date inclusao = new Date();
    @Temporal(TemporalType.DATE)
    @Column(name = "exclusao")
    private Date exclusao = null;

    @Column(name="conveniado")
    private boolean conveniado = false;

    public Associado() {
    }

    public Date getAdmissao() {
        return admissao;
    }

    public void setAdmissao(Date admissao) {
        this.admissao = admissao;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getCelular1() {
        return celular1;
    }

    public void setCelular1(String celular1) {
        this.celular1 = celular1;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String celular2) {
        this.celular2 = celular2;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public Endereco getEndOpm() {
        return endOpm;
    }

    public void setEndOpm(Endereco endOpm) {
        this.endOpm = endOpm;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public EstadosCivil getEstado() {
        return estado;
    }

    public void setEstado(EstadosCivil estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getOpm() {
        return opm;
    }

    public void setOpm(String opm) {
        this.opm = opm;
    }

    public String getPosto() {
        return posto;
    }

    public void setPosto(String posto) {
        this.posto = posto;
    }

    public int getQtdeFilhos() {
        return qtdeFilhos;
    }

    public void setQtdeFilhos(int qtdeFilhos) {
        this.qtdeFilhos = qtdeFilhos;
    }

    public String getTelefoneOpm() {
        return telefoneOpm;
    }

    public void setTelefoneOpm(String telefoneopm) {
        this.telefoneOpm = telefoneopm;
    }

    public String getRe() {
        return re;
    }

    public void setRe(String re) {
        this.re = re;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Pagto getFormaPgto() {
        return formaPgto;
    }

    public void setFormaPgto(Pagto formaPgto) {
        this.formaPgto = formaPgto;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public List<Dependente> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<Dependente> dependentes) {
        this.dependentes = dependentes;
    }

    public List<Veiculo> getVeiculos() {
        return veiculos;
    }

    public void setVeiculos(List<Veiculo> veiculos) {
        this.veiculos = veiculos;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public Date getExclusao() {
        return exclusao;
    }

    public void setExclusao(Date exclusao) {
        this.exclusao = exclusao;
    }

    public Date getInclusao() {
        return inclusao;
    }

    public void setInclusao(Date inclusao) {
        this.inclusao = inclusao;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isAtivo() {
        return exclusao == null;
    }

    public boolean isSalvo() {
        return id != 0;
    }

    public boolean isConveniado() {
        return conveniado;
    }

    public void setConveniado(boolean conveniado) {
        this.conveniado = conveniado;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Associado other = (Associado) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Associado";
    }
}
