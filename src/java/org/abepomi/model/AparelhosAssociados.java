/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="aparelhos_associados", schema="public")
@SequenceGenerator(name="seqaparelhosassociados", sequenceName="seq_apaass", allocationSize=1)
public class AparelhosAssociados implements Serializable {

    @Id
    @GeneratedValue(generator="seqaparelhosassociados", strategy=GenerationType.SEQUENCE)
    private int id;

    @Temporal(TemporalType.DATE)
    @Column(name="data")
    private Date data = null;

    @Column(name="imei", length=15)
    private String imei = "";

    @ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JoinColumn(name="ass_id", referencedColumnName="id")
    private Associado associado = null;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="ap_id", referencedColumnName="id")
    private Aparelho aparelho = null;

    private Double valor = 0.00;

    private int parcelas = 0;

    public AparelhosAssociados() {

    }

    public Aparelho getAparelho() {
        return aparelho;
    }

    public void setAparelho(Aparelho aparelho) {
        this.aparelho = aparelho;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public int getParcelas() {
        return parcelas;
    }

    public void setParcelas(int parcelas) {
        this.parcelas = parcelas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AparelhosAssociados other = (AparelhosAssociados) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.AparelhosAssociados{" + "id=" + id + "data=" + data + "imei=" + imei + "associado=" + associado.getId() + "aparelho=" + aparelho.getId() + "valor=" + valor + "parcelas=" + parcelas + '}';
    }





}
