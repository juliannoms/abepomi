/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name = "mov_aut", schema = "public")
@NamedQueries({
    @NamedQuery(name="AtualizarStatus", query="UPDATE MovDA mov SET mov.status = :status where mov.assDA.idenDA = :associado and mov.valor = :valor and mov.status = 'AB'"),
    @NamedQuery(name="PesquisaByAssValor", query="select mov from MovDA mov where mov.assDA.idenDA = :associado and mov.valor = :valor and mov.status = 'AB'"),
    @NamedQuery(name="PorMesRef", query="select mov from MovDA mov where mov.remDA.ref = :referencia order by mov.assDA.associado.id"),
    @NamedQuery(name="Debitados", query="select mov from MovDA mov where mov.remDA.ref = :referencia and (mov.status = '00' or mov.status = '31') order by mov.assDA.associado.id "),
    @NamedQuery(name="NDebitados", query="select mov from MovDA mov where mov.remDA.ref = :referencia and mov.status != '00' and mov.status != '31' order by mov.assDA.associado.id ")
})
@IdClass(MovDAPk.class)
public class MovDA implements Serializable {

   // private MovDAPk pk = new MovDAPk();

    @Id
    //@AssociationOverride(name="assDA", joinColumns= @JoinColumn(name="ass_id"))
    //@AttributeOverride(name="assDA", column=@Column(name="ass_id"))
    private AssDA assDA = new AssDA();

    @Id
    //@AssociationOverride(name="remDA", joinColumns= @JoinColumn(name="rem_id"))
    //@AttributeOverride(name="remDA", column=@Column(name="rem_id"))
    private RemDA remDA = new RemDA();

    @Temporal(value = TemporalType.DATE)
    @Column(name = "dtvencimento")
    private Date dtVencimento;

//    @Column(name = "valor", precision = 10, scale = 2)
//    private Double valor;
    private BigDecimal valor;

    @Column(name="status", length=2)
    private String status;

    public MovDA() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AssDA getAssDA() {
        return assDA;
    }

    public void setAssDA(AssDA assDA) {
        this.assDA = assDA;
    }

    public RemDA getRemDA() {
        return remDA;
    }

    public void setRemDA(RemDA remDA) {
        this.remDA = remDA;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    //@EmbeddedId
    /*@AssociationOverrides(
        { @AssociationOverride(name="pk.assDA", joinColumns= @JoinColumn(name="ass_id", referencedColumnName="id")),
          @AssociationOverride(name="pk.remDA", joinColumns = @JoinColumn(name = "rem_id", referencedColumnName="id")) })*/
    /*@AttributeOverrides(
        { @AttributeOverride(name = "assDA", column =  @Column(name = "ass_id", nullable = false, length=6)),
          @AttributeOverride(name =  "remDA", column = @Column(name = "rem_id", nullable = false)) })
    public MovDAPk getPk() {
        return pk;
    }

    public void setPk(MovDAPk pk) {
        this.pk = pk;
    }*/

//    public Double getValor() {
//        return valor;
//    }
//
//    public void setValor(Double valor) {
//        this.valor = valor;
//    }
/*
    @Transient
    public RemDA getRemDA() {
        return this.getPk().getRemDA();
    }

    @Transient
    public AssDA getAssDA() {
        return this.getPk().getAssDA();
    }
*/


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MovDA other = (MovDA) obj;
        if (this.assDA != other.assDA && (this.assDA == null || !this.assDA.equals(other.assDA))) {
            return false;
        }
        if (this.remDA != other.remDA && (this.remDA == null || !this.remDA.equals(other.remDA))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.assDA != null ? this.assDA.hashCode() : 0);
        hash = 79 * hash + (this.remDA != null ? this.remDA.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.MovDA";
    }
}
