/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.abepomi.model.util.TipoLinha;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="linhas", schema="public")
@NamedQueries({
    @NamedQuery(name="linhasLivres", query="select linha from Linha linha where linha.livre = 'true'"),
    @NamedQuery(name="linhasOcupadas", query="select linha from Linha linha where linha.livre = 'false'")
})

public class Linha implements Serializable{

    public Linha() {
    }

    @Id
    @Column(name="numero", length=10)
    private String numero = "";

    @Column(name="livre")
    private boolean livre = true;

    @Enumerated(EnumType.STRING)
    @Column(name="tipo", length=1)
    private TipoLinha tipo = null;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name="contrato", referencedColumnName="codigo")
    private Contrato contrato = null;

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public boolean isLivre() {
        return livre;
    }

    public void setLivre(boolean livre) {
        this.livre = livre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TipoLinha getTipo() {
        return tipo;
    }

    public void setTipo(TipoLinha tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linha other = (Linha) obj;
        if ((this.numero == null) ? (other.numero != null) : !this.numero.equals(other.numero)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.numero != null ? this.numero.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Linha{" + "numero=" + numero + "livre=" + livre + "tipo=" + tipo + "contrato=" + contrato + '}';
    }



}
