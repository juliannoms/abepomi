/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import org.abepomi.model.util.Estados;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@SequenceGenerator(name="seqend", sequenceName="seq_enderecos", allocationSize=1)
@Table(name="enderecos", schema="public")
public class Endereco implements Serializable{

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqend")
    private int id;

    @Column(name="logradouro", length=50)
    private String logradouro;

    @Column(name="bairro", length=50)
    private String bairro;

    @Column(name="cidade", length=50)
    private String cidade;

    @Enumerated(EnumType.STRING)
    @Column(name="uf", length=2)
    private Estados uf;

    @Column(name="cep", length=8)
    private String cep;

    public Endereco(){};

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Estados getUf() {
        return uf;
    }

    public void setUf(Estados uf) {
        this.uf = uf;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Endereco other = (Endereco) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }



    @Override
    public String toString() {
        return "org.abepomi.model.Endereco";
    }


    

}
