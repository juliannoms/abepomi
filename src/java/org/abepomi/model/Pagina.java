/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */

@Entity
@Table(name="paginas", schema="public")
@NamedQueries({
    @NamedQuery(name="paginasSeguras", query="select page from Pagina page where page.liberada = 'false'"),
    @NamedQuery(name="paginasLiberadas", query="select page from Pagina page where page.liberada = 'true'")
})
@SequenceGenerator(name="seqpaginas", sequenceName="seq_paginas")
public class Pagina implements Serializable {

    public Pagina(){}

    @Id @GeneratedValue(generator="seqpaginas", strategy=GenerationType.AUTO)
    @Column(name="id")
    private int id;

    @Column(name="pagina", length=50)
    private String pagina;

    @Column(name="descricao", length=50)
    private String descricao;

    @Column(name="liberada")
    private Boolean liberada;

    public Boolean isLiberada() {
        return liberada;
    }

    public void setLiberada(Boolean liberada) {
        this.liberada = liberada;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pagina other = (Pagina) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Pagina{" + "id=" + id + "pagina=" + pagina + '}';
    }



}
