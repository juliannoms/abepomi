/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */

@Entity
@Table(schema="public", name="servicos_modem")
public class ServicoModem extends Servico {

    @Column(name="imei", length=15, nullable=false)
    private String imei;

    @Column(name="linha", length=10, nullable=false)
    private String linha;

    public ServicoModem() {
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }
    
}
