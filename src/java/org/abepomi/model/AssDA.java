/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.util.EstadoDA;

/**
 *
 * @author Julianno
 */

@Entity
@Table(name="ass_aut", schema="public")
@NamedQueries({
    @NamedQuery(name="DAPorEstado", query="select da from AssDA da where da.opcao = :opcao order by da.associado.id "),
    @NamedQuery(name="DAPorNaoEstado", query="select da from AssDA da where da.opcao != :opcao order by da.associado.id ")
})
public class AssDA implements Serializable {

    @Id @Column(name="id", length=6)
    private String idenDA;

    @Column(name="agencia", length=4)
    private String agencia;

    @Column(name="conta", length=14)
    private String conta;

    @Temporal(TemporalType.DATE)
    @Column(name="dtopcao")
    private Date dtOpcao;

    @Enumerated(EnumType.STRING)
    @Column(name="opcao")
    private EstadoDA opcao;

    @OneToOne(cascade=CascadeType.REFRESH, fetch=FetchType.EAGER)
    @JoinColumn(name="associado", referencedColumnName="id")
    private Associado associado;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="automatico", referencedColumnName="id")
    private ConvDA convenio;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="assDA")
    private List<MovDA> debitos = new ArrayList<MovDA>();

    public AssDA() {
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public ConvDA getConvenio() {
        return convenio;
    }

    public void setConvenio(ConvDA convenio) {
        this.convenio = convenio;
    }

    public Date getDtOpcao() {
        return dtOpcao;
    }

    public void setDtOpcao(Date dtOpcao) {
        this.dtOpcao = dtOpcao;
    }

    public String getIdenDA() {
        return idenDA;
    }

    public void setIdenDA(String idenDA) {
        this.idenDA = idenDA;
    }

    public EstadoDA getOpcao() {
        return opcao;
    }

    public void setOpcao(EstadoDA opcao) {
        this.opcao = opcao;
    }

    public List<MovDA> getDebitos() {
        return debitos;
    }

    public void setDebitos(List<MovDA> debitos) {
        this.debitos = debitos;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssDA other = (AssDA) obj;
        if ((this.idenDA == null) ? (other.idenDA != null) : !this.idenDA.equals(other.idenDA)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.idenDA != null ? this.idenDA.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.AssDA";
    }



}
