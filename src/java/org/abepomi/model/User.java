/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="usuarios")
@NamedQuery(name="buscarPorLogin", query="select distinct user from User user left join fetch user.paginas where user.login like :login")
public class User implements Serializable {

    @Id
    @Column(name="login", length=20)
    private String login;

    @Column(name="senha", length=20)
    private String senha;

    @Column(name="nome", length=50)
    private String nome;

    @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name="usuarios_paginas",
        joinColumns={@JoinColumn(name="usuario_id")},
        inverseJoinColumns={@JoinColumn(name="pagina_id")})
    private List<Pagina> paginas = new ArrayList();


    public User(){
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Pagina> getPaginas() {
        return paginas;
    }

    public void setPaginas(List<Pagina> paginas) {
        this.paginas = paginas;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if ((this.login == null) ? (other.login != null) : !this.login.equals(other.login)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.login != null ? this.login.hashCode() : 0);
        return hash;
    }

    

    @Override
    public String toString() {
        return "org.abepomi.model.User";
    }

    public boolean isAutorizada(String pagina){
        for(Pagina pag : paginas){
            if(pagina.equals(pag.getPagina())){
                return true;
            }
        }
        return false;
    }


}
