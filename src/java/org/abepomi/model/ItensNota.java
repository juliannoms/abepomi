/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="itens_nota", schema="public")
@IdClass(ItensNotaPk.class)
public class ItensNota implements Serializable{

    @Id
    private NotaFiscal notaFiscal;
    
    @Id
    private Aparelho aparelho;
    
    @Column(name="preco", precision=10, scale=2)
    private Double preco = 0.00;

    @Column(name="qtde")
    private int qtde = 0;

    public ItensNota() {
    }

    public int getQtde() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde = qtde;
    }

    public Aparelho getAparelho() {
        return aparelho;
    }

    public void setAparelho(Aparelho aparelho) {
        this.aparelho = aparelho;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItensNota other = (ItensNota) obj;
        if (this.notaFiscal != other.notaFiscal && (this.notaFiscal == null || !this.notaFiscal.equals(other.notaFiscal))) {
            return false;
        }
        if (this.aparelho != other.aparelho && (this.aparelho == null || !this.aparelho.equals(other.aparelho))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.notaFiscal != null ? this.notaFiscal.hashCode() : 0);
        hash = 73 * hash + (this.aparelho != null ? this.aparelho.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.ItensNota{" + "notaFiscal=" + notaFiscal + "aparelho=" + aparelho + "preco=" + preco + "qtde=" + qtde + '}';
    }



}
