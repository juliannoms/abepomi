/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Julianno
 */
@Embeddable

public class MovDAPk implements Serializable {

    
    
    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="ass_id", referencedColumnName="id")
    private AssDA assDA = new AssDA();

    
    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="rem_id", referencedColumnName="id")
    private RemDA remDA = new RemDA();

    public MovDAPk() {
    }

    public AssDA getAssDA() {
        return assDA;
    }

    public void setAssDA(AssDA assDA) {
        this.assDA = assDA;
    }

    public RemDA getRemDA() {
        return remDA;
    }

    public void setRemDA(RemDA remDA) {
        this.remDA = remDA;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MovDAPk other = (MovDAPk) obj;
        if (this.assDA != other.assDA && (this.assDA == null || !this.assDA.equals(other.assDA))) {
            return false;
        }
        if (this.remDA != other.remDA && (this.remDA == null || !this.remDA.equals(other.remDA))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.assDA != null ? this.assDA.hashCode() : 0);
        hash = 37 * hash + (this.remDA != null ? this.remDA.hashCode() : 0);
        return hash;
    }


    @Override
    public String toString() {
        return "org.abepomi.model.MovDAPk";
    }

   

}
