/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Julianno
 */
public class ItensNotaPk implements Serializable {

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="nf_id", referencedColumnName="id")
    private NotaFiscal notaFiscal;

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="ap_id", referencedColumnName="id")
    private Aparelho aparelho;

    public Aparelho getAparelho() {
        return aparelho;
    }

    public void setAparelho(Aparelho aparelho) {
        this.aparelho = aparelho;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItensNotaPk other = (ItensNotaPk) obj;
        if (this.notaFiscal != other.notaFiscal && (this.notaFiscal == null || !this.notaFiscal.equals(other.notaFiscal))) {
            return false;
        }
        if (this.aparelho != other.aparelho && (this.aparelho == null || !this.aparelho.equals(other.aparelho))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.notaFiscal != null ? this.notaFiscal.hashCode() : 0);
        hash = 71 * hash + (this.aparelho != null ? this.aparelho.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.ItensNotaPk{" + "notaFiscal=" + notaFiscal + "aparelho=" + aparelho + '}';
    }



}
