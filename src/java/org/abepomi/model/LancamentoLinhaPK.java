/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Julianno
 */
@Embeddable
public class LancamentoLinhaPK implements Serializable{

    @Column(name="codigo")
    private int codigo = 0;

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="fatura", referencedColumnName="codigo")
    private Fatura fatura = null;

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="numero", referencedColumnName="numero")
    private Linha linha = null;

    public LancamentoLinhaPK() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }


    public Fatura getFatura() {
        return fatura;
    }

    public void setFatura(Fatura fatura) {
        this.fatura = fatura;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LancamentoLinhaPK other = (LancamentoLinhaPK) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        if (this.fatura != other.fatura && (this.fatura == null || !this.fatura.equals(other.fatura))) {
            return false;
        }
        if (this.linha != other.linha && (this.linha == null || !this.linha.equals(other.linha))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.codigo;
        hash = 59 * hash + (this.fatura != null ? this.fatura.hashCode() : 0);
        hash = 59 * hash + (this.linha != null ? this.linha.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.LancamentoLinhaPK{" + "codigo=" + codigo + "fatura=" + fatura + "linha=" + linha + '}';
    }

    

}
