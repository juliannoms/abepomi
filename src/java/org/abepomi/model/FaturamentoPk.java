/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Julianno
 */
@Embeddable
public class FaturamentoPk implements Serializable {

    public FaturamentoPk() {
    }

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="ass_id", referencedColumnName="id")
    private Associado associado = new Associado();

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="ref_id", referencedColumnName="id")
    private Referencia referencia = new Referencia();

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FaturamentoPk other = (FaturamentoPk) obj;
        if (this.associado != other.associado && (this.associado == null || !this.associado.equals(other.associado))) {
            return false;
        }
        if (this.referencia != other.referencia && (this.referencia == null || !this.referencia.equals(other.referencia))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.associado != null ? this.associado.hashCode() : 0);
        hash = 37 * hash + (this.referencia != null ? this.referencia.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.FaturamentoPk{" + "associado=" + associado + "referencia=" + referencia + '}';
    }


}
