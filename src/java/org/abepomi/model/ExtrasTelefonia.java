/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="extras_telefonia", schema="public")
@SequenceGenerator(name="seqextrastel", sequenceName="seq_extrastel", allocationSize=1)
@NamedQueries({
    @NamedQuery(name="extrasSFaturarPorLinha", query="select extra from ExtrasTelefonia extra where extra.faturada = 'false' and extra.linha = :linha order by extra.id")
})
public class ExtrasTelefonia implements Serializable {

    public static final String TIPO_HABILITACAO = "H";
    public static final String TIPO_HABILITACAO_VIVO = "X";
    public static final String TIPO_APARELHO = "A";
    public static final String TIPO_APARELHO_VIVO = "Z";
    public static final String TIPO_DIVERSOS = "D";
    public static final String TIPO_DESCONTO = "S";
    public static final String TIPO_DESCONTO_VIVO = "Y";
    public static final String TIPO_ACRESCIMO = "C";
    public static final String TIPO_ACRESCIMO_VIVO = "W";
    public static final String TIPO_TRANSFERIDO = "T";

    public ExtrasTelefonia() {
    }

    @Id
    @GeneratedValue(generator="seqextrastel", strategy=GenerationType.SEQUENCE)
    private int id = 0;

    @Column(name="descricao", length=50)
    private String descricao = "";

    @Column(name="valor", precision=10, scale=2)
    private Double valor = 0.00;

    @Column(name="linha", length=10)
    private String linha = "";

    @Column(name="tipo", length=1)
    private String tipo = "";

    @Column(name="faturada")
    private boolean faturada = false;

    @Column(name="fatura")
    private Integer fatura = null;

    public boolean isFaturada() {
        return faturada;
    }

    public void setFaturada(boolean faturada) {
        this.faturada = faturada;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getFatura() {
        return fatura;
    }

    public void setFatura(Integer fatura) {
        this.fatura = fatura;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExtrasTelefonia other = (ExtrasTelefonia) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.ExtrasTelefonia{" + "id=" + id + "descricao=" + descricao + "valor=" + valor + "linha=" + linha + "tipo=" + tipo + "faturada=" + faturada + "fatura=" + fatura + '}';
    }

}
