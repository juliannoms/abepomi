/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="associados_linhas",schema="public")
@SequenceGenerator(name="seq_associados_linhas", sequenceName="seq_asslinhas", allocationSize=1)
@NamedQueries({
    @NamedQuery(name="linhasPorAssociado", query="select new org.abepomi.dao.util.AssociadoLinhaUtil(assLinha.associado.id, assLinha.linha.numero,"
    + " assLinha.entrega, assLinha.devolucao, assLinha.plano.codigo) "
    + " from AssociadoLinha assLinha where assLinha.associado.id = :associado"),
    @NamedQuery(name="associadosComTelefone", query="select associado.id from Associado associado, AssociadoLinha linha where associado = linha.associado group by associado order by associado.id"),
    @NamedQuery(name="AssociadoLinha.linhasAtivasPorAssociado", query="select linha from AssociadoLinha linha where linha.devolucao is null and linha.associado = :ass order by linha.linha.numero"),
    @NamedQuery(name="AssociadoLinha.associadoLinhaPorId", query="select linha from AssociadoLinha linha where linha.id = :id and linha.devolucao is null"),
    @NamedQuery(name="AssociadoLinha.linhasPorAssociado", query="select new org.abepomi.dao.util.AssociadoLinhaUtilRel(ass.id, ass.nome, li.linha.numero, li.entrega, li.devolucao, li.plano.descricao, li.linha.tipo) "
    + "from Associado ass, AssociadoLinha li where li.associado = ass order by ass.nome, li.entrega")
})
//@IdClass(org.abepomi.model.AssociadoLinhaPK.class)
public class AssociadoLinha implements Serializable{

    @Id
    @GeneratedValue(generator="seq_associados_linhas", strategy=GenerationType.SEQUENCE)
    private int id;

    //@EmbeddedId
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "associado", referencedColumnName = "id")
    private Associado associado;

    //@EmbeddedId
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "linha", referencedColumnName = "numero")
    private Linha linha;

    @Temporal(TemporalType.DATE)
    @Column(name="entrega")
    private Date entrega = null;

    @Temporal(TemporalType.DATE)
    @Column(name="devolucao")
    private Date devolucao = null;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="plano", referencedColumnName="codigo")
    private Plano plano = null;

    public AssociadoLinha() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssociadoLinha other = (AssociadoLinha) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }


    @Override
    public String toString() {
        return "org.abepomi.model.AssociadoLinha{id = "+this.id+"}";
    }


}
