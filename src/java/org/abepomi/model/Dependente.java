/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.util.Parentesco;

/**
 *
 * @author Julianno
 */
@Entity
@SequenceGenerator(name="seqdep", sequenceName="seq_dependentes", allocationSize=1)
@Table(name="dependentes", schema="public")
public class Dependente implements Serializable {

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqdep")
    private int id;

    @Column(name="nome", length=50)
    private String nome;

    @Temporal(TemporalType.DATE)
    @Column(name="nascimento")
    private Date nascimento;

    @Column(name="cpf", length=11)
    private String cpf;

    @Column(name="rg", length=14)
    private String rg;

    @Column(name="numero")
    private int numero;

    @Enumerated(EnumType.STRING)
    @Column(name="parentesco")
    private Parentesco parentesco;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ass_id", referencedColumnName="id")
    private Associado associado;

    public Dependente(){}

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Parentesco getParentesco() {
        return parentesco;
    }

    public void setParentesco(Parentesco parentesco) {
        this.parentesco = parentesco;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dependente other = (Dependente) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        return hash;
    }



    @Override
    public String toString() {
        return "org.abepomi.model.Dependente";
    }



    
}
