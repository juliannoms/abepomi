/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.util.OperacaoNota;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="notafiscal", schema="public")
@SequenceGenerator(name="seqnotafiscal", sequenceName="seq_notafiscal", allocationSize=1)
public class NotaFiscal implements Serializable{

    @Id
    @GeneratedValue(generator="seqnotafiscal", strategy=GenerationType.SEQUENCE)
    private int id;

    @Temporal(TemporalType.DATE)
    @Column(name="data")
    private Date data;

    @Column(name="numero", length=15)
    private String numero;

    @Column(name="valor")
    private Double valor = 0.00;

    @Enumerated(EnumType.STRING)
    @Column(name="operacao", length=1)
    private OperacaoNota operacao;

    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="for_id", referencedColumnName="id")
    private Fornecedor fornecedor = new Fornecedor();

    @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="notaFiscal")
    private List<ItensNota> itens = new ArrayList<ItensNota>();

    

    public NotaFiscal() {
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public OperacaoNota getOperacao() {
        return operacao;
    }

    public void setOperacao(OperacaoNota operacao) {
        this.operacao = operacao;
    }

    public List<ItensNota> getItens() {
        return itens;
    }

    public void setItens(List<ItensNota> itens) {
        this.itens = itens;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotaFiscal other = (NotaFiscal) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.NotaFiscal{" + "id=" + id + "data=" + data + "numero=" + numero + "valor=" + valor + "operacao=" + operacao + "fornecedor=" + fornecedor + '}';
    }



}
