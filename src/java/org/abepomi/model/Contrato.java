/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.util.Operadora;

/**
 *
 * @author Julianno
 */

@Entity
@Table(name="contratos_claro", schema="public")
@SequenceGenerator(name="seqcontrato", sequenceName="seq_contratos", allocationSize=1)
@NamedQueries({
    @NamedQuery(name="buscaPorNumero", query="select contrato from Contrato contrato where contrato.numero = :numero"),
    @NamedQuery(name="qtdeContratos", query="select count(con) from Contrato contrato")
})
public class Contrato implements Serializable {

    public Contrato() {
    }

    @Id
    @Column(name="codigo")
    @GeneratedValue(generator="seqcontrato", strategy=GenerationType.SEQUENCE)
    private int id = 0;

    @Column(name="numero", length=20)
    private String numero = "";

    @Column(name="ciclo")
    private int ciclo = 0;

    @Column(name="data")
    @Temporal(TemporalType.DATE)
    private Date data = null;

    @Column(name="descricao", length=100)
    private String descricao = "";

    /*@Enumerated(EnumType.STRING)
    @Column(name="operadora", length=1)
    private Operadora operadora;*/

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="associacao", referencedColumnName="id")
    private Associacao associacao = null;

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    /*public Operadora getOperadora() {
        return operadora;
    }

    public void setOperadora(Operadora operadora) {
        this.operadora = operadora;
    }*/
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contrato other = (Contrato) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.Contrato{" + "id=" + id + "numero=" + numero + "ciclo=" + ciclo + "data=" + data + "descricao=" + descricao + "associacao=" + associacao + '}';
    }



}
