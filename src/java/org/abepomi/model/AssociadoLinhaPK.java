/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Julianno
 */
@Embeddable
public class AssociadoLinhaPK implements Serializable{
/*
    public AssociadoLinhaPK() {
    }

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="associado", referencedColumnName="id")
    private Associado associado = null;

    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="linha", referencedColumnName="numero")
    private Linha linha = null;

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssociadoLinhaPK other = (AssociadoLinhaPK) obj;
        if (this.associado != other.associado && (this.associado == null || !this.associado.equals(other.associado))) {
            return false;
        }
        if (this.linha != other.linha && (this.linha == null || !this.linha.equals(other.linha))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.associado != null ? this.associado.hashCode() : 0);
        hash = 59 * hash + (this.linha != null ? this.linha.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.AssociadoLinhaPK{" + "associado=" + associado + "linha=" + linha + '}';
    }

    */

}
