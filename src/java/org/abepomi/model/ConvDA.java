/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@SequenceGenerator(name="seqaut", sequenceName="seq_aut", allocationSize=1)
@Table(name="automaticos")
public class ConvDA implements Serializable{

    @Id @GeneratedValue(strategy=GenerationType.AUTO, generator="seqaut")
    private int id;

    @Column(name="convenio", length=20)
    private String convenio;

    @Column(name="codbanco", length=3)
    private String bancoCod;

    @Column(name="nomebanco", length=20)
    private String bancoNome;

    @Column(name="descricao", length=100)
    private String descricao;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="associacao", referencedColumnName="id")
    private Associacao associacao;

    public ConvDA() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }

    public String getBancoCod() {
        return bancoCod;
    }

    public void setBancoCod(String bancoCod) {
        this.bancoCod = bancoCod;
    }

    public String getBancoNome() {
        return bancoNome;
    }

    public void setBancoNome(String bancoNome) {
        this.bancoNome = bancoNome;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConvDA other = (ConvDA) obj;
        if ((this.convenio == null) ? (other.convenio != null) : !this.convenio.equals(other.convenio)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.convenio != null ? this.convenio.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "org.abepomi.model.ConvDA";
    }


}
