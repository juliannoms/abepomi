/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.cobranca;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.abepomi.model.Associado;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="titulos", schema="public")
@SequenceGenerator(name="Titulo.sequence", sequenceName="seq_titulos", allocationSize=1, initialValue=1)
@NamedQueries({
    @NamedQuery(name="Titulo.atualizaRetorno", query="update Titulo t set t.estado = :estado, t.pagamento = :pagamento, t.valorPago = :pago where t.nossoNro = :nosso"),
    @NamedQuery(name="Titulo.byAssRef", query="select t from Titulo t where t.associado = :associado and t.referencia = :referencia"),
    @NamedQuery(name="Titulo.byAssEst", query="select t from Titulo t join fetch t.mensagens join fetch t.referencia where t.associado = :associado and t.estado = :estado order by t.vencimento"),
    @NamedQuery(name="Titulo.AssPendente", query="select distinct t.associado from Titulo t join fetch t.associado.endereco where t.estado = 11 order by t.associado.nome "),
    @NamedQuery(name="Titulo.atualizaEstado", query="update Titulo t set t.estado = ?1 where t.id = ?2"),
    @NamedQuery(name="Titulo.buscarPorReferencia", query="select t from Titulo t where t.referencia = :referencia order by t.pagamento,t.associado.nome")
})
public class Titulo implements Serializable {

    public Titulo(){
    }

    @Id
    @GeneratedValue(generator="Titulo.sequence", strategy=GenerationType.SEQUENCE)
    private Integer id;

    @Column(name="seu_nro")
    private String seuNro;

    @Column(name="nosso_nro")
    private String nossoNro;

    @Temporal(TemporalType.DATE)
    private Date emissao;

    @Temporal(TemporalType.DATE)
    private Date vencimento;

    private BigDecimal valor;

    private Integer estado;

    @Temporal(TemporalType.DATE)
    private Date pagamento;

    @Column(name="pago")
    private BigDecimal valorPago;

    @ManyToOne
    @JoinColumn(name="ass_id")
    private Associado associado;

    @ManyToOne
    @JoinColumn(name="ref_id")
    private Referencia referencia;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="titulo")
    private List<Mensagem> mensagens;

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Date getEmissao() {
        return emissao;
    }

    public void setEmissao(Date emissao) {
        this.emissao = emissao;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNossoNro() {
        return nossoNro;
    }

    public void setNossoNro(String nossoNro) {
        this.nossoNro = nossoNro;
    }

    public Date getPagamento() {
        return pagamento;
    }

    public void setPagamento(Date pagamento) {
        this.pagamento = pagamento;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public String getSeuNro() {
        return seuNro;
    }

    public void setSeuNro(String seuNro) {
        this.seuNro = seuNro;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Mensagem> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Titulo other = (Titulo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Titulo{" + "id=" + id + "seuNro=" + seuNro + "nossoNro=" + nossoNro + "emissao=" + emissao + "vencimento=" + vencimento + "valor=" + valor + "estado=" + estado + "pagamento=" + pagamento + "valorPago=" + valorPago + "associado=" + associado + "referencia=" + referencia + "mensagens=" + mensagens.size() + '}';
    }


}
