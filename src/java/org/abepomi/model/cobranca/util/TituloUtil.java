/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.cobranca.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Faturamento;
import org.abepomi.model.cobranca.Mensagem;
import org.abepomi.model.cobranca.Titulo;
import org.abepomi.model.util.Validacao;

/**
 *
 * @author Julianno
 */
public class TituloUtil {

    public static void gerarTitulosFaturamentos(List<Faturamento> fat, Date vencimento) throws DAOException{
        for (Faturamento f : fat){
            Titulo titulo = new Titulo();
            titulo.setAssociado(f.getAssociado());
            titulo.setReferencia(f.getReferencia());
            titulo.setValor(BigDecimal.valueOf(f.getExtra()).add(BigDecimal.valueOf(f.getMensalidade()).add(BigDecimal.valueOf(f.getServicos())
                    .add(BigDecimal.valueOf(f.getTelefonia()).add(BigDecimal.valueOf(f.getVivo()))))));
            titulo.setValor(titulo.getValor().add(f.getParcelas()));
            titulo.setEmissao(new Date());
            titulo.setVencimento(vencimento);
            titulo.setNossoNro("0002100597" + f.getReferencia().getRef().substring(0, 2) + f.getReferencia().getRef().substring(4, 6) + "0" + Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(f.getAssociado().getId()), 5));
            titulo.setSeuNro("Ref "+f.getReferencia().getRef());
            titulo.setEstado(11);
            Mensagem msg = new Mensagem();
            msg.setSequencia(1);
            msg.setTexto("referente ao mês ".toUpperCase()+f.getReferencia().getRef()+".");
            msg.setTitulo(titulo);
            titulo.setMensagens(new ArrayList<Mensagem>());
            titulo.getMensagens().add(msg);
            /**/
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(vencimento);
            Mensagem msg2 = new Mensagem();
            msg2.setSequencia(2);
            msg2.setTexto("Após ".toUpperCase()+calendar.get(GregorianCalendar.DAY_OF_MONTH)+"/"+(calendar.get(GregorianCalendar.MONTH)+1)+"/"
                    +calendar.get(GregorianCalendar.YEAR)+" será cobrado juros e multa na próxima fatura.".toUpperCase());
            msg2.setTitulo(titulo);
            titulo.getMensagens().add(msg2);
            /**/
            /**/
            Mensagem msg1 = new Mensagem();
            msg1.setSequencia(3);
            msg1.setTexto("não receber após 25/".toUpperCase()+f.getReferencia().getRef().substring(0, 2)+"/"+f.getReferencia().getRef().substring(2)+".");
            msg1.setTitulo(titulo);
            titulo.getMensagens().add(msg1);
            /**/
            DAOFactory.createTituloDAO().addEntity(titulo);
        }
    }

}
