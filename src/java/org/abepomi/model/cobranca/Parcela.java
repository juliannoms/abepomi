/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.cobranca;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="parcelas", schema="public")
@NamedQueries({
    @NamedQuery(name="Parcela.faturar", query="select p from Parcela p inner join p.parcelamento l where p.numero = "
                                                + "(select min(pa.numero) from Parcela pa where pa.parcelamento = l "
                                                + "and pa.situacao = 11) and l.associado.id = :ass")
})
@SequenceGenerator(name="Parcela.sequence", sequenceName="seq_parcelas", allocationSize=1)
public class Parcela implements Serializable{

    public Parcela() {
    }

    @Id @GeneratedValue(generator="Parcela.sequence",strategy=GenerationType.SEQUENCE)
    private Integer id;

    private Integer numero;

    private BigDecimal valor;

    private Integer situacao;

    @ManyToOne
    @JoinColumn(name="parcelamentos_id", referencedColumnName="id")
    private Parcelamento parcelamento;

    @OneToOne
    @JoinColumn(name="referencia_id", referencedColumnName="id")
    private Referencia referencia = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Parcelamento getParcelamento() {
        return parcelamento;
    }

    public void setParcelamento(Parcelamento parcelamento) {
        this.parcelamento = parcelamento;
    }

    public Integer getSituacao() {
        return situacao;
    }

    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parcela other = (Parcela) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Parcela{" + "id=" + id + "numero=" + numero + "valor=" + valor + "situacao=" + situacao + "parcelamento=" + parcelamento.getId() + '}';
    }


}
