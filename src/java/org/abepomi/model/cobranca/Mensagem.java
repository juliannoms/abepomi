/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.cobranca;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="msg_titulos", schema="public")
@SequenceGenerator(name="Mensagem.sequence", sequenceName="seq_msg_titulos", allocationSize=1, initialValue=1)
public class Mensagem implements Serializable {

    public Mensagem() {
    }

    @Id
    @GeneratedValue(generator="Mensagem.sequence", strategy=GenerationType.SEQUENCE)
    private Integer id;

    private String texto;

    private Integer sequencia;

    @ManyToOne()
    @JoinColumn(name="titulos_id")
    private Titulo titulo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSequencia() {
        return sequencia;
    }


    public void setSequencia(Integer sequencia) {
        this.sequencia = sequencia;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mensagem other = (Mensagem) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
