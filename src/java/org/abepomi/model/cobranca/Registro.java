package org.abepomi.model.cobranca;

import java.math.BigDecimal;
import java.util.Date;

public class Registro {

    private Integer codMov;
    private String nossoNro;
    private Date dtVencimento;
    private Date dtMovimento;
    private BigDecimal valor;
    private BigDecimal valorPago;

    public Registro() {
    }

    public Registro(Integer codMov, String nossoNro, Date dtVencimento, Date dtMovimento, BigDecimal valor,
            BigDecimal valorPago) {
        super();
        this.codMov = codMov;
        this.nossoNro = nossoNro;
        this.dtVencimento = dtVencimento;
        this.dtMovimento = dtMovimento;
        this.valor = valor;
        this.valorPago = valorPago;
    }

    public Integer getCodMov() {
        return codMov;
    }

    public void setCodMov(Integer codMov) {
        this.codMov = codMov;
    }

    public String getNossoNro() {
        return nossoNro;
    }

    public void setNossoNro(String nossoNro) {
        this.nossoNro = nossoNro;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtMovimento() {
        return dtMovimento;
    }

    public void setDtMovimento(Date dtMovimento) {
        this.dtMovimento = dtMovimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    @Override
    public String toString() {
        return "Registro [codMov=" + codMov + ", nossoNro=" + nossoNro + ", dtVencimento=" + dtVencimento
                + ", dtMovimento=" + dtMovimento + ", valor=" + valor + ", valorPago=" + valorPago + "]";
    }
}
