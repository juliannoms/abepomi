/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model.cobranca;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.abepomi.model.Associado;

/**
 *
 * @author Julianno
 */
@Entity
@Table(name="parcelamentos", schema="public")
@SequenceGenerator(name="Parcelamento.sequence", sequenceName="seq_parcelamentos", allocationSize=1)
public class Parcelamento implements Serializable {

    public Parcelamento() {
    }

    @Id @GeneratedValue(generator="Parcelamento.sequence", strategy=GenerationType.SEQUENCE)
    private Integer id;

    @Column(name="parcelas")
    private Integer qtde;

    private BigDecimal total;

    @ManyToOne
    @JoinColumn(name="ass_id",referencedColumnName="id")
    private Associado associado;

    @OneToMany(mappedBy="parcelamento", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
    private List<Parcela> parcelas;

    @ManyToMany
    @JoinTable(name="parcelamento_titulos", schema="public", 
        joinColumns={@JoinColumn(name="parcelamentos_id")}, 
        inverseJoinColumns={@JoinColumn(name="titulos_id")}
    )
    private List<Titulo> titulos;

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<Parcela> parcelas) {
        this.parcelas = parcelas;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public List<Titulo> getTitulos() {
        return titulos;
    }

    public void setTitulos(List<Titulo> titulos) {
        this.titulos = titulos;
    }

    public void gerarParcelas(){
        this.setParcelas(new ArrayList<Parcela>());
        BigDecimal qtdeParcelas = new BigDecimal(qtde);
        BigDecimal valor = total.divide(qtdeParcelas, 2, RoundingMode.DOWN);
        BigDecimal aux = new BigDecimal("0.01").setScale(2);
        BigDecimal resto = total.subtract(valor.multiply(qtdeParcelas));
        for(int i = 0; i < qtde; i++){
            Parcela parcela = new Parcela();
            parcela.setNumero(i);
            if(resto.doubleValue()>0.00){
                parcela.setValor(valor.add(aux));
                resto = resto.subtract(aux);
            }else{
                parcela.setValor(valor);
            }
            parcela.setSituacao(11);
            parcela.setParcelamento(this);
            this.getParcelas().add(parcela);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parcelamento other = (Parcelamento) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Parcelamento{" + "id=" + id + "qtde=" + qtde + "total=" + total + "associado=" + associado.getId() + "parcelas=" + parcelas.size() + "titulos=" + titulos.size() + '}';
    }
}
