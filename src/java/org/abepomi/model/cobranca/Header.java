package org.abepomi.model.cobranca;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Header {

    private Long codBanco;
    private Long codInscr;
    private Long codCobr;
    private Long codAge;
    private Long codConta;
    private Date dtGeracao;
    private Long nroSeq;
    private Integer qtdeReg;
    private List<Registro> registros;

    public Header() {
    }

    public Header(Long codBanco, Long codInscr, Long codCobr, Long codAge, Long codConta, Date dtGeracao, Long nroSeq,
            Integer qtdeReg, List<Registro> registros) {
        super();
        this.codBanco = codBanco;
        this.codInscr = codInscr;
        this.codCobr = codCobr;
        this.codAge = codAge;
        this.codConta = codConta;
        this.dtGeracao = dtGeracao;
        this.nroSeq = nroSeq;
        this.qtdeReg = qtdeReg;
        this.registros = registros;
    }

    public Long getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(Long codBanco) {
        this.codBanco = codBanco;
    }

    public Long getCodInscr() {
        return codInscr;
    }

    public void setCodInscr(Long codInscr) {
        this.codInscr = codInscr;
    }

    public Long getCodCobr() {
        return codCobr;
    }

    public void setCodCobr(Long codCobr) {
        this.codCobr = codCobr;
    }

    public Long getCodAge() {
        return codAge;
    }

    public void setCodAge(Long codAge) {
        this.codAge = codAge;
    }

    public Long getCodConta() {
        return codConta;
    }

    public void setCodConta(Long codConta) {
        this.codConta = codConta;
    }

    public Date getDtGeracao() {
        return dtGeracao;
    }

    public void setDtGeracao(Date dtGeracao) {
        this.dtGeracao = dtGeracao;
    }

    public Long getNroSeq() {
        return nroSeq;
    }

    public void setNroSeq(Long nroSeq) {
        this.nroSeq = nroSeq;
    }

    public List<Registro> getRegistros() {
        return registros;
    }

    public void adicionaRegistro(Registro r) {
        if (null == this.registros) {
            this.registros = new ArrayList<Registro>();
        }
        this.registros.add(r);
        qtdeReg++;
    }

    @Override
    public String toString() {
        return "Header [codBanco=" + codBanco + ", codInscr=" + codInscr + ", codCobr=" + codCobr + ", codAge="
                + codAge + ", codConta=" + codConta + ", dtGeracao=" + dtGeracao + ", nroSeq=" + nroSeq + ", qtdeReg="
                + qtdeReg + ", registros=" + registros + "]";
    }
}
