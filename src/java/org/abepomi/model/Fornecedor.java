/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.abepomi.model.util.TipoPessoa;

/**
 *
 * @author Julianno
 */

@Entity
@Table(name="fornecedores", schema="public")
public class Fornecedor implements Serializable{
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name="nome", length=50)
    private String nome;

    @Column(name="rgie", length=14)
    private String rgie;

    @Column(name="cpfcnpj", length=14)
    private String cpfcnpj;

    @Column(name="telefone", length=10)
    private String telefone;

    @Column(name="celular", length=10)
    private String celular;

    @Column(name="contato", length=50)
    private String contato;

    @Enumerated(EnumType.STRING)
    @Column(name="tipo")
    private TipoPessoa tipo;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="endereco", referencedColumnName="id")
    private Endereco endereco = new Endereco();

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="fornecedor")
    private List<Servico> servicos = new ArrayList<Servico>();

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="associacao")
    private Associacao associacao;

    public Fornecedor() {
    }

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getCpfcnpj() {
        return cpfcnpj;
    }

    public void setCpfcnpj(String cpfcnpj) {
        this.cpfcnpj = cpfcnpj;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRgie() {
        return rgie;
    }

    public void setRgie(String rgie) {
        this.rgie = rgie;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public TipoPessoa getTipo() {
        return tipo;
    }

    public void setTipo(TipoPessoa tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fornecedor other = (Fornecedor) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        return hash;
    }



    @Override
    public String toString() {
        return "org.abepomi.model.Fornecedor";
    }


}
