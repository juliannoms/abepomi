/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.AssociacaoLinha;

/**
 *
 * @author Julianno
 */
public class AssociacaoLinhaConverter implements Converter{

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(null == value)
            return null;

        if(value.length() == 0)
            return null;

        int id = 0;
        try{
            id = Integer.parseInt(value);
        }catch(NumberFormatException ex){
            return null;
        }

        try {
            return DAOFactory.createAssociacaoLinhaDAO().findById(id);
        } catch (DAOException ex) {
            Logger.getLogger(LinhaConverter.class.getName()).log(Level.SEVERE, "Erro: linha não encontrada!", ex);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(null == value)
            return null;

        if(!(value instanceof AssociacaoLinha))
            return null;

        return String.valueOf(((AssociacaoLinha)value).getId());
    }

}
