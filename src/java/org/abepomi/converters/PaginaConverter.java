/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Pagina;

/**
 *
 * @author Julianno
 */
public class PaginaConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value.equals("")){
            return null;
        }
        try{
            return DAOFactory.createPaginaDAO().findById(Integer.parseInt(value));
        }catch(DAOException daoex){
            Logger.getLogger(ReferenciaConverter.class.getName()).log(Level.SEVERE, "Erro PaginaConverter", daoex);
            return null;
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if((null!=value) && (value instanceof Pagina)){
            Pagina pagina = (Pagina) value;
            return String.valueOf(pagina.getId());
        }
        return "";
    }

}
