/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Contrato;

/**
 *
 * @author Julianno
 */
public class ContratoConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        if (null == value) {
            return null;
        }
        if (value.length() == 0) {
            return null;
        }
        int id = 0;
        try {
            id = Integer.parseInt(value);
        } catch (NumberFormatException numberex) {
            //Logger.getLogger(ContratoConverter.class.getName()).log(Level.SEVERE, "Erro ao converter Contrato: value não é inteiro!", numberex);
            return null;
        }
        try {
            return DAOFactory.createContratoDAO().findById(id);
        } catch (DAOException ex) {
            Logger.getLogger(ContratoConverter.class.getName()).log(Level.SEVERE, "Erro ao converter Contrato!", ex);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(null == value)
            return null;

        if(!(value instanceof Contrato))
            return null;

        return String.valueOf(((Contrato)value).getId());
    }
}
