/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Referencia;

/**
 *
 * @author Julianno
 */
public class ReferenciaConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value.equals("")){
            return null;
        }
        try {
            return DAOFactory.createReferenciaDAO().findById(Integer.parseInt(value));
        } catch (DAOException ex) {
            Logger.getLogger(ReferenciaConverter.class.getName()).log(Level.SEVERE, "Erro ReferenciaConverter", ex);
            return null;
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if((null != value) && (value instanceof Referencia)){
            Referencia ref = (Referencia)value;
            return String.valueOf(ref.getId());
        }
        return "";
    }

}
