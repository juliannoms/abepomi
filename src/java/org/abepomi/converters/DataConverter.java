/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Julianno
 */
public class DataConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try{
            return new SimpleDateFormat("dd/MM/yyyy").parse(value);
        }catch(ParseException parseEx){
            parseEx.toString();
        }
        return null;

    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value instanceof Date){
            return new SimpleDateFormat("dd/MM/yyyy").format(value);
        }
        return "";
    }

}
