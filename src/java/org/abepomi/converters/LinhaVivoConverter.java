/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.abepomi.model.vivo.LinhaVivo;

/**
 *
 * @author Julianno
 */
@FacesConverter(forClass=org.abepomi.model.vivo.LinhaVivo.class)
public class LinhaVivoConverter implements Converter {
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(null == value)
            return null;

        if(value.length() == 0)
            return null;

        return value;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(null == value)
            return null;

        if(!(value instanceof LinhaVivo))
            return null;

        return ((LinhaVivo)value).getLinha();
    }
}
