/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Associacao;

/**
 *
 * @author Julianno
 */
public class AsoConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (null == value) {
            return null;
        }
        if (value.length() == 0) {
            return null;
        }
        int id = 0;
        try {
            id = Integer.parseInt(value);
        } catch (NumberFormatException numberex) {
            //Logger.getLogger(AsoConverter.class.getName()).log(Level.SEVERE,"Erro ao converter Associação: value não é inteiro!", numberex);
            return null;
        }
        try {
            return DAOFactory.createAssociacaoDAO().findById(id);
        } catch (DAOException ex) {
            Logger.getLogger(AsoConverter.class.getName()).log(Level.SEVERE, "Erro ao converter Associação!", ex);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }

        if (!(value instanceof Associacao)) {
            return null;
        }

        return String.valueOf(((Associacao) value).getId());
    }
}
