/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Julianno
 */
public class RetiraMascaraString implements Converter{

    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        if(value != null){
            value = value.trim();
            value = value.replace(".", "").replace("-", "").replace("(", "").replace(")", "").replace(" ", "").replace("/", "");
        }
        return value;

    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value == null){
            return "";
        }
        if(value instanceof String){
            return (String)value;
        }
        return null;
    }

}
