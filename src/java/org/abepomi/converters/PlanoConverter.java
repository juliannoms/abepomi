/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Plano;

/**
 *
 * @author Julianno
 */
public class PlanoConverter implements Converter{

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(null == value)
            return null;

        if(value.length() == 0)
            return null;

        int id = 0;
            try {
                id = Integer.parseInt(value);
            } catch (NumberFormatException numberex) {
                //Logger.getLogger(AsoConverter.class.getName()).log(Level.SEVERE,"Erro ao converter Associação: value não é inteiro!", numberex);
                return null;
            }

        try {
            return DAOFactory.createPlanoDAO().findById(id);
        } catch (DAOException ex) {
            Logger.getLogger(LinhaConverter.class.getName()).log(Level.SEVERE, "Erro: plano não encontrado!", ex);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(null == value)
            return null;

        if(!(value instanceof Plano))
            return null;

        return String.valueOf(((Plano)value).getCodigo());
    }



}
