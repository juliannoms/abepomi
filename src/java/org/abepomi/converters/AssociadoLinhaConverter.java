/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.AssociadoLinha;

/**
 *
 * @author Julianno
 */
public class AssociadoLinhaConverter implements Converter{

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(null == value)
            return null;

        if(value.length() == 0)
            return null;

        int id = 0;
        try{
            id = Integer.parseInt(value);
        }catch(NumberFormatException ex){
            return null;
        }

        try {
            return DAOFactory.createAssociadoLinhaDAO().associadoLinhaPorId(id);
        } catch (DAOException ex) {
            Logger.getLogger(LinhaConverter.class.getName()).log(Level.SEVERE, "Erro: linha não encontrada!", ex);
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(null == value)
            return null;

        if(!(value instanceof AssociadoLinha))
            return null;

        return String.valueOf(((AssociadoLinha)value).getId());
    }

}
