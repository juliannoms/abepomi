/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.abepomi.arqs.ArqRemDA;
import org.abepomi.arqs.ArqRetDA;
import org.abepomi.dao.AssociacaoDAO;
import org.abepomi.dao.ConvDADAO;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.RemDADAO;
import org.abepomi.dao.RetDADAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.exception.RetornoDebitoInvalidoException;
import org.abepomi.mb.util.DownloadArq;
import org.abepomi.model.AssDA;
import org.abepomi.model.Associacao;
import org.abepomi.model.ConvDA;
import org.abepomi.model.Faturamento;
import org.abepomi.model.MovDA;
import org.abepomi.model.Referencia;
import org.abepomi.model.RemDA;
import org.abepomi.model.util.EstadoDA;
import org.abepomi.model.util.Estados;
import org.abepomi.model.util.Status;

/**
 *
 * @author Julianno
 */
public class AssociacaoMB {

    private Associacao associacao;
    private ConvDA convDA;
    //private String arquivoDA;
    private String arquivoDA;
    private List<String[]> arquivosDA;
    private String arquivo;
    private RemDA remessa;
    private Referencia referencia;
    private Date vencimento;
    private Map estados;
    private static AssociacaoDAO asoDAO;
    private static RetDADAO retDAO;
    private static ConvDADAO convDADAO;
    private static RemDADAO remDAO;

    public AssociacaoMB() {
        convDA = new ConvDA();
        arquivoDA = null;
        remessa = new RemDA();
        referencia = new Referencia();
        estados = null;
        arquivosDA = carregarArquivos();

        asoDAO = DAOFactory.createAssociacaoDAO();
        retDAO = DAOFactory.createRetDADAO();
        convDADAO = DAOFactory.createConvDADAO();
        remDAO = DAOFactory.createRemDADAO();
        carregarAssociacao();
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    //Verificar
    public List<Referencia> getReferencias() {
        return DAOFactory.createReferenciaDAO().buscaRefSemRemessa();
    }

    private void cabecalhoRemessa() {
        try {
            Integer nsa = remDAO.getMaxNsa();
            if (null == nsa) {
                nsa = 0;
            }
            remessa.setNsa(nsa + 1);
            remessa.setConvenio(convDADAO.findById(1));
            remessa.setRef(referencia);
            remessa.setDtGeracao(new Date());
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: cabecalhoRemessa()", ex);
        }
    }

    public String configuraRemessa() {
        cabecalhoRemessa();
        try {
            List<AssDA> listaAss = DAOFactory.createAssDADAO().buscaPorEstado(EstadoDA.C);
            if (!(null == listaAss)) {
                BigDecimal valor = new BigDecimal("0.00").setScale(2);
                Faturamento fat;
                for (AssDA ass : listaAss) {
                    if (!ass.getAssociado().getStatus().equals(Status.I)) {
                        MovDA mov = new MovDA();
                        mov.setRemDA(remessa);
                        mov.setAssDA(ass);
                        mov.setStatus("AB");
                        fat = DAOFactory.createFaturamentoDAO().buscaFatPorAssRef(ass.getAssociado(), referencia);
                        valor = new BigDecimal(fat.getExtra().toString()).add(new BigDecimal(fat.getMensalidade().toString()).add(new BigDecimal(fat.getServicos().toString()).add(new BigDecimal(fat.getTelefonia().toString()).add(new BigDecimal(fat.getVivo().toString())))));
                        mov.setValor(valor.add(fat.getParcelas()));
                        mov.setDtVencimento(vencimento);
                        remessa.getDebitos().add(mov);
                        //remessa.setValor(remessa.getValor()+mov.getValor());
                    }

                }

            }
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: configuraRemessa()", ex);
            return "falha";
        }
        //remessa.setDtGeracao(new Date());
        try {
            remDAO.addEntity(remessa);
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: gravarRemessa", ex);
            return "falha";
        }
        try {
            DownloadArq.download(ArqRemDA.gerarRemessa(remessa), "Remessa" + remessa.getNsa() + ".rem");
        } catch (Exception ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: download", ex);
        }

        //CobrancaBB.gerarCobranca(DAOFactory.createFaturamentoDAO().buscaCobrancaRef(referencia));

        return "sucesso";
    }

    public String configuraRemessaAtual() {
        remessa = new RemDA();
        cabecalhoRemessa();
        remessa.setRef(null);
        //remessa.setDebitos(null);

        try {
            AssDA ass = DAOFactory.createAssDADAO().buscaPorAssociado(DAOFactory.createAssociadoDAO().findById(1));
            MovDA mov = new MovDA();
            mov.setAssDA(ass);
            mov.setDtVencimento(new Date());
            mov.setValor(new BigDecimal(0.00));
            mov.setRemDA(remessa);
            remessa.getDebitos().add(mov);
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao gerar arquivo.", ex);
            return "falha";
        }


        try {
            DownloadArq.download(ArqRemDA.gerarRemessa(remessa), "Remessa" + remessa.getNsa() + ".rem");
        } catch (Exception ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao fazer download...", ex);
            return "falha";
        }

        try {
            remDAO.addEntity(remessa);
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar Remessa atualização!", ex);
            return "falha";
        }


        return "sucesso";

    }

    public String configuraRemessaIncentivo() {
        remessa = new RemDA();
        cabecalhoRemessa();
        try {
            List<AssDA> listaAss = DAOFactory.createAssDADAO().buscaPorEstado(EstadoDA.E);
            MovDA lcto = null;
            for (AssDA ass : listaAss) {
                lcto = new MovDA();
                lcto.setAssDA(ass);
                remessa.getDebitos().add(lcto);
            }
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao gerar remessa incentvo!", ex);
            return "falha";
        }
        try {
            DownloadArq.download(ArqRemDA.gerarRemessaI(remessa), "Remessa" + remessa.getNsa() + ".rem");
        } catch (Exception ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro no download do arquivo!", ex);
            return "falha";
        }

        remessa.setRef(null);
        remessa.setDebitos(null);

        try {
            remDAO.addEntity(remessa);
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar Remessa atualização!", ex);
            return "falha";
        }

        return "sucesso";
    }

    private void carregarAssociacao() {
        try {
            associacao = asoDAO.findById(1);
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: carregarAssociacao()", ex);
        }

        if (associacao == null) {
            associacao = new Associacao();
        }
    }

    /*public boolean isNullRemDA(){
    return remessa == null;
    }*/
    public RemDA getRemessa() {
        return remessa;
    }

    public void setRemessa(RemDA remessa) {
        this.remessa = remessa;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public ConvDA getConvDA() {
        return convDA;
    }

    public void setConvDA(ConvDA convenio) {
        this.convDA = convenio;
    }

    public Associacao getAssociacao() {
        return associacao;
    }

    public void setAssociacao(Associacao associacao) {
        this.associacao = associacao;
    }

    public String salvar() {
        String retorno;
        try {
            if (associacao.getId() == 0) {
                asoDAO.addEntity(associacao);
            } else {
                asoDAO.updateEntity(associacao);
            }
            retorno = "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: salvar()", ex);
            retorno = "falha";
        }
        return retorno;
    }

    private Map carregarEstados() {
        Map<Estados, String> est = new LinkedHashMap<Estados, String>();
        for (Estados type : Estados.values()) {
            est.put(type, type.name());
        }
        return est;
    }

    public Map getEstados() {
        if (null == estados) {
            estados = carregarEstados();
        }
        return estados;
    }

    public String cadastrarDA() {
        carregarAssociacao();
        if (!(null == associacao.getConveniosDA()) && !(associacao.getConveniosDA().isEmpty())) {
            convDA = associacao.getConveniosDA().get(0);
        }
        return "cadastroDA";
    }

    public String salvarDA() {
        try {
            /*if (associacao.getConveniosDA().isEmpty()) {
            associacao.getConveniosDA().add(convDA);
            convDA.setAssociacao(associacao);
            }
            asoDAO.updateEntity(associacao);*/
            convDA.setAssociacao(associacao);
            if (convDA.getId() == 0) {
                DAOFactory.createConvDADAO().addEntity(convDA);
            } else {
                DAOFactory.createConvDADAO().updateEntity(convDA);
            }
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro: salvarDA()", ex);
        }
        return "falha";
    }

    public boolean isAsoCadastrada() {
        return (!(null == associacao) || (associacao.getId() != 0));
    }

    public String getArquivoDA() {
        //arquivoDA = "";
        arquivoDA = "/upload/debitoautomatico/";//DBT" + String.valueOf(new Date().getTime()) + ".ret";
        setArquivo(arquivoDA);
        return arquivoDA;
    }

    private List<String[]> carregarArquivos() {
        File[] files = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/debitoautomatico") + "/").listFiles();
        List<String[]> lista = new ArrayList<String[]>();
        //String[] arq = {"","",""};
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for (File file : files) {
            String[] arq = {"", "", ""};
            arq[0] = file.getName();
            arq[1] = String.valueOf(sdf.format(new Date(file.lastModified())));
            arq[2] = String.valueOf(file.length()) + " bytes";
            lista.add(arq);
        }
        return lista;
    }

    public List<String[]> getArquivosDA() {
        return arquivosDA;
    }

    public String salvarArquivo() {
        arquivosDA = carregarArquivos();
        return "updebaut";
    }

    public String processarArquivoRetorno() {
        try {
            String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/debitoautomatico") + "/" + getArquivo();
            ArqRetDA.processarArqRetornoDA(caminho);
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao ler arquivo retorno!", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        } catch (RetornoDebitoInvalidoException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        } catch (IOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entre em contato com o Administrador!", null));
        }
        return "falha";
    }

    public String apagarArqDA() {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/debitoautomatico");
        File file = new File(caminho + "/" + getArquivo());
        //System.out.println(caminho+"/"+getArquivo());

        if (file.delete()) {
            return "sucesso";
        }

        return "falha";

    }

    public List<Associacao> getAssociacoes() {
        try {
            return asoDAO.findAll();
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao retornar associações!", ex);
        }
        return null;
    }
}
