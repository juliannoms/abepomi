/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.validator.ValidatorException;
import javax.faces.component.UIComponent;
import javax.faces.application.FacesMessage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.arqs.cobranca.Retorno;
import org.abepomi.cobranca.CobrancaBB;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.cobranca.TituloDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.exception.RetornoCobrancaInvalidoException;
import org.abepomi.mb.util.DownloadArq;
import org.abepomi.model.Associado;
import org.abepomi.model.Referencia;
import org.abepomi.model.cobranca.Header;
import org.abepomi.model.cobranca.Parcelamento;
import org.abepomi.model.cobranca.Registro;
import org.abepomi.model.cobranca.Titulo;

/**
 *
 * @author Julianno
 */
@ManagedBean(name = "cobraMB")
@ViewScoped
public class CobrancaMB implements Serializable {

    private static String LOCAL_ARQUIVO = "/upload/cobranca/";

    public CobrancaMB() {
        this.associados = DAOFactory.createTituloDAO().buscaAssPendente();
    }
    private Titulo titulo;
    private Associado associado;
    private List<Associado> associados;
    private List<Titulo> titulos;
    private Map<Integer, Boolean> checked = new HashMap<Integer, Boolean>();
    private BigDecimal total = new BigDecimal(0.00);
    private Integer parcelas = 1;
    private Boolean selecionado = false;
    private String arquivoSelecionado = "";
    //
    private Boolean baixaManual = false;
    private Integer motivoBaixa = null;
    private Date    dataBaixa   = new Date();
    //
    private List<Associado> associadosParcelamento = new ArrayList();

    private Referencia referencia;

    /**
     *  Getters and Setters
     */
    public List<Associado> getAssociados() {
        return associados;
    }

    public void setAssociados(List<Associado> associados) {
        this.associados = associados;
    }

    public List<Titulo> getTitulos() {
        return titulos;
    }

    public void setTitulos(List<Titulo> titulos) {
        this.titulos = titulos;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    public Map<Integer, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Integer, Boolean> checked) {
        this.checked = checked;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        //this.total = total;
    }

    public Integer getParcelas() {
        return parcelas;
    }

    public void setParcelas(Integer parcelas) {
        this.parcelas = parcelas;
    }

    public Boolean getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(Boolean selecionado) {
        this.selecionado = selecionado;
    }

    public void setArquivoSelecionado(String arquivoSelecionado) {
        this.arquivoSelecionado = arquivoSelecionado;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public Boolean getBaixaManual() {
        return baixaManual;
    }

    public void setBaixaManual(Boolean baixaManual) {
        this.baixaManual = baixaManual;
    }

    public Date getDataBaixa() {
        return dataBaixa;
    }

    public void setDataBaixa(Date dataBaixa) {
        this.dataBaixa = dataBaixa;
    }

    public Integer getMotivoBaixa() {
        return motivoBaixa;
    }

    public void setMotivoBaixa(Integer motivoBaixa) {
        this.motivoBaixa = motivoBaixa;
    }

    public List<Associado> getAssociadosParcelamentos(){
        if(associadosParcelamento.isEmpty()){
            associadosParcelamento = DAOFactory.createParcelamentoDAO().buscarAssociadosParcelamento();
        }
        return associadosParcelamento;
    }
    
    public void carregarTitulosAssociados(ValueChangeEvent event) {
        if (event.getNewValue() != event.getOldValue()) {
            if (event.getNewValue() instanceof Associado) {
                titulos = DAOFactory.createTituloDAO().buscaPorAssEst((Associado) event.getNewValue(), 11);
                checked.clear();
                total = new BigDecimal(0.00);
                selecionado = false;
            }
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public void carregarTitulosSelecionados(AjaxBehaviorEvent event) {
        total = new BigDecimal(0.00);
        for (Titulo t : titulos) {
            if (checked.containsKey(t.getId()) && checked.get(t.getId())) {
                total = total.add(t.getValor());
            }
        }
        FacesContext.getCurrentInstance().renderResponse();
    }

    public void selecionarTodos(AjaxBehaviorEvent event) {
        boolean ok = selecionado;
        total = new BigDecimal(0.00);
        for (Titulo t : titulos) {
            checked.put(t.getId(), ok);
            if (ok) {
                total = total.add(t.getValor());
            }
        }
        FacesContext.getCurrentInstance().renderResponse();
    }

    public void validarValor(FacesContext context, UIComponent component, Object value) {
        if (total.doubleValue() == 0.00) {
            throw new ValidatorException(new FacesMessage("Valor inválido! Selecione um débito!"));
        }

    }

    public String gerarParcelas() {
        List<Titulo> selecionados = new ArrayList<Titulo>();
        for (Titulo t : titulos) {
            if (checked.get(t.getId())) {
                selecionados.add(t);
            }
        }
        Parcelamento parcelamento = new Parcelamento();
        parcelamento.setQtde(parcelas);
        parcelamento.setTotal(total);
        parcelamento.setAssociado(associado);
        parcelamento.setTitulos(selecionados);
        parcelamento.gerarParcelas();
        try {
            DAOFactory.createParcelamentoDAO().addEntity(parcelamento);
            DAOFactory.createTituloDAO().atualizarTitulosNegociados(selecionados);
        } catch (DAOException daoex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, "Erro ao gravar parcelamento!", daoex);
            return "falha";
        }
        return "sucesso";
    }

    public List<String[]> getArquivos() {
        File[] files = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/cobranca") + "/").listFiles();
        List<String[]> lista = new ArrayList<String[]>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for (File file : files) {
            String[] arq = {"", "", ""};
            arq[0] = file.getName();
            arq[1] = String.valueOf(sdf.format(new Date(file.lastModified())));
            arq[2] = String.valueOf(file.length()) + " bytes";
            lista.add(arq);
        }
        return lista;
    }

    public String getLocalArquivo(){
        return LOCAL_ARQUIVO;
    }

    public String salvarArquivo(){
        return "upload/uploadcobranca";
    }

    public String apagarArquivo() {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/telefonia");
        File file = new File(caminho + "/" + arquivoSelecionado);
        if (file.delete()) {
            return "upload/uploadcobranca";
        }
        return "falha";
    }

    public String processarArquivo(){
        try {
            Header header = Retorno.processar(FacesContext.getCurrentInstance().getExternalContext().getRealPath(LOCAL_ARQUIVO)+"/"+arquivoSelecionado);
            int qtde = 0;
            if(!(null == header.getRegistros())){
                for(Registro r : header.getRegistros()){
                    qtde = qtde + DAOFactory.createTituloDAO().atualizaTituloRetorno(r.getCodMov(), r.getDtMovimento(), r.getValorPago(), r.getNossoNro());
                }
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, qtde+" títulos atualizados!",null));
            return "sucesso";
        }  catch (RetornoCobrancaInvalidoException ex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(),null));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(),null));
        } catch (IOException ex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro na aplicação!",null));
        }
        return "falha";
    }

    public List<Associado> getAssociadosBoletos(){
        try {
            return DAOFactory.createAssociadoDAO().findAllByNome();
        } catch (DAOException ex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Referencia> getReferencias(){
        return DAOFactory.createReferenciaDAO().buscaTodasDecrescente();
    }

    public String gerarBoleto(){
        Titulo t = DAOFactory.createTituloDAO().buscarPorAssRef(associado, referencia);
        List<Titulo> lista = new ArrayList();
        lista.add(t);
        try {
            DownloadArq.download(CobrancaBB.gerarCobrancaTitulos(lista), "boleto.pdf");
        } catch (Exception ex) {
            Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, null, ex);
            return "falha";
        }
        return "sucesso";
    }

    public String baixarTitulos(){
        List<Titulo> selecionados = new ArrayList<Titulo>();
        for (Titulo t : titulos) {
            if (checked.get(t.getId())) {
                selecionados.add(t);
            }
        }
        TituloDAO titDAO = DAOFactory.createTituloDAO();
        for (Titulo tit : selecionados){
            tit.setEstado(motivoBaixa);
            tit.setPagamento(dataBaixa);
            try {
                titDAO.updateEntity(tit);
            } catch (DAOException ex) {
                Logger.getLogger(CobrancaMB.class.getName()).log(Level.SEVERE, null, ex);
                return "falha";
            }
        }
        return "sucesso";
    }

    public String relParcelamentosAssociados() throws DAOException {
        List<Parcelamento> lista = DAOFactory.createParcelamentoDAO().buscarParcelamentosAssociados(associado);
        Map parametros = new HashMap();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Parcelamentos por Associado");
        parametros.put("SUBREPORT_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relParcelamentosAssociados.jasper");
        return "sucesso";
    }

    private void geraRelatorio(List lista, Map parametros, String nome) {
        String caminhoRelJasper = //"/var/www/vhosts/abepomi.com.br/appservers/glassfish-3x/domains/domain1/docroot/WEB-INF/classes/relatorios/"+nome;
                FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/" + nome;

        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
        JasperPrint impressao = null;
        //System.out.println(caminhoRelJasper);
        try {

            impressao = JasperFillManager.fillReport(caminhoRelJasper, parametros, ds);
            try {
                DownloadArq.download(JasperExportManager.exportReportToPdf(impressao), "relatorio.pdf");
            } catch (Exception ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro ao gerar relatório: " + nome, ex);
            }
        } catch (JRException e) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Falha ao chamar org.abepomi.mb.AssociadoMB.gerarRelatorio", e);
        }
    }
}
