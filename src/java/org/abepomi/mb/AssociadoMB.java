/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.abepomi.model.util.Estados;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.arqs.ArqEmails;
import org.abepomi.cobranca.CobrancaBB;
import org.abepomi.dao.AssDADAO;
import org.abepomi.dao.AssociadoDAO;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.VeiculoDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.AssociadoEmail;
import org.abepomi.dao.util.AssociadoLinhaUtilRel;
import org.abepomi.dao.util.DetalheTelefoniaUtilRel;
import org.abepomi.dao.util.FaturaUtilRelCusto;
import org.abepomi.dao.util.FaturamentoRefRel;
import org.abepomi.dao.util.RefVivoRel;
import org.abepomi.mail.EmailAbepomi;
import org.abepomi.mail.EmailMensalThread;
import org.abepomi.mb.util.DownloadArq;
import org.abepomi.mb.util.GeraMd5;
import org.abepomi.model.AssDA;
import org.abepomi.model.Associado;
import org.abepomi.model.ConvDA;
import org.abepomi.model.Dependente;
import org.abepomi.model.Faturamento;
import org.abepomi.model.Linha;
import org.abepomi.model.MovDA;
import org.abepomi.model.Referencia;
import org.abepomi.model.Veiculo;
import org.abepomi.model.cobranca.Titulo;
import org.abepomi.model.util.Categoria;
import org.abepomi.model.util.EstadoDA;
import org.abepomi.model.util.EstadosCivil;
import org.abepomi.model.util.Pagto;
import org.abepomi.model.util.Parentesco;
import org.abepomi.model.util.Sexo;
import org.abepomi.model.util.Status;
import org.abepomi.model.util.Validacao;
import org.abepomi.pdf.PdfUtil;

/**
 *
 * @author Julianno
 */
public class AssociadoMB implements Serializable {

    public AssociadoMB() {
        associado = new Associado();
        dependente = new Dependente();
        veiculo = new Veiculo();

        /*
        AssociadoLinhaUtil assLU = new AssociadoLinhaUtil(1, "", new GregorianCalendar(2011, 04, 25).getTime(), new GregorianCalendar(2012, 02, 19).getTime(), 0);
        assLU.calculaProporcao(new GregorianCalendar(2012, 01, 20).getTime(), new GregorianCalendar(2012, 02, 19).getTime());
         */
    }
    private Associado associado;
    private Dependente dependente;
    private Veiculo veiculo;
    private AssDA assDA = null;
    private List<Associado> associados;
    private int selecionado;
    private String fromId;
    private String filtro;
    private String busca;
    private static AssociadoDAO assDAO = DAOFactory.createAssociadoDAO();
    //private static DependenteDAO depDAO = DAOFactory.createDependenteDAO();
    private static VeiculoDAO veiDAO = DAOFactory.createVeiculoDAO();
    private static AssDADAO daDAO = DAOFactory.createAssDADAO();
    private Referencia referencia;
    private String relNome = "";
    private Date dataInicio = null;

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public List<Date> getDatasInicio() {
        if (fromId.equals("relFaturaTelefonica")) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "entrou no relFaturaTelefonica");
            return DAOFactory.createFaturaDAO().buscarDatasInicio();
        } else if (fromId.equals("relFatVivo")) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "relFatVivo");
            return DAOFactory.createFaturaVivoDAO().buscarFaturas();
        }
        Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "relFatVivoTelefonicaNULO");
        return null;
    }

    public String getRelNome() {
        return relNome;
    }

    public void setRelNome(String relNome) {
        this.relNome = relNome;
    }

    public String salvar() throws DAOException {
        String retorno = "falha";
        //try {
        try {
            if (associado.getId() == 0) {
                assDAO.addEntity(associado);
                cadastrarDA();
            } else {
                assDAO.updateEntity(associado);
            }
            retorno = "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: salvar()", ex);
        }
        //} finally {
        associado = new Associado();
        return retorno;
        //}



    }

    public AssDA getAssDA() {
        return assDA;
    }

    public void setAssDA(AssDA assDA) {
        this.assDA = assDA;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public List<Associado> getAssociados() {
        return associados;
    }

    public void setAssociados(List<Associado> associados) {
        this.associados = associados;
    }

    public String buscarAssociado() {
        try {
            if (filtro.equals("nome")) {
                associados = assDAO.buscaPorNome(busca);
            } else if (filtro.equals("cpf")) {
                associados = assDAO.buscaPorCpf(busca);
            }
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: buscarAssociado()", ex);
        }

        busca = "";
        return "pesquisar";
    }

    public String buscarTodosAssociados() {
        try {
            associados = assDAO.findAllByNome();
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: buscarTodosAssociados()", ex);
        }

        return "pesquisar";

    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String getFiltro() {
        return filtro;
    }

    public String getBusca() {
        return busca;
    }

    public void setBusca(String busca) {
        this.busca = busca;
    }

    public int getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(int selecionado) {
        this.selecionado = selecionado;
    }

    public String selecionarAssociado() {
        try {
            associado = assDAO.findById(selecionado);
            assDA = daDAO.buscaPorAssociado(associado);
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: selecionarAssociado()", ex);
        }
        return fromId;
    }

    public String pesquisarAssociado() {
        return "pesquisar";
    }

    public String cancelar() {
        associado = new Associado();
        dependente = new Dependente();
        return "cadastro";
    }

    public Map getEstados() {
        Map<Estados, String> est = new LinkedHashMap<Estados, String>();
        for (Estados type : Estados.values()) {
            est.put(type, type.name());
        }
        return est;
    }

    public Map getParentesco() {
        Map<Parentesco, String> par = new LinkedHashMap<Parentesco, String>();
        for (Parentesco type : Parentesco.values()) {
            par.put(type, type.name());
        }
        return par;
    }

    public Map getCivil() {
        Map<EstadosCivil, String> est = new LinkedHashMap<EstadosCivil, String>();
        for (EstadosCivil type : EstadosCivil.values()) {
            est.put(type, type.name());
        }
        return est;
    }

    public Map getSexo() {
        Map<Sexo, String> sexo = new LinkedHashMap<Sexo, String>();
        for (Sexo type : Sexo.values()) {
            sexo.put(type, type.name());
        }
        return sexo;
    }

    public Map getPagto() {
        Map<Pagto, String> pagto = new LinkedHashMap<Pagto, String>();
        for (Pagto type : Pagto.values()) {
            pagto.put(type, type.name());
        }
        return pagto;
    }

    public Map getCategoria() {
        Map<Categoria, String> categoria = new LinkedHashMap<Categoria, String>();
        for (Categoria type : Categoria.values()) {
            categoria.put(type, type.name());
        }
        return categoria;
    }

    public Dependente getDependente() {
        return dependente;
    }

    public void setDependente(Dependente dependente) {
        this.dependente = dependente;
    }

    public String salvarDependente() {

        if (dependente.getId() == 0) {
            associado.getDependentes().add(dependente);
            dependente.setAssociado(associado);
        }
        try {
            assDAO.updateEntity(associado);
            associado = assDAO.findById(associado.getId());
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: salvarDependente()", ex);
        }



        dependente = new Dependente();

        return "dependentes";
    }

    public String alterarDependente() {
        return "dependentes";
    }

    public String excluirDependente() {
        /*associado.getDependentes().remove(dependente);
        dependente.setAssociado(null);
        try {
        assDAO.updateEntity(associado);
        depDAO.updateEntity(dependente);
        depDAO.removeEntity(dependente);
        } catch (DAOException ex) {
        Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        dependente = new Dependente();*/
        return "dependentes";
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String cadastrarDependentes() {
        if (associado.getId() != 0) {
            try {
                associado = assDAO.findById(associado.getId());
            } catch (DAOException ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: cadastrarDependentes()", ex);
            }
        }
        return "dependentes";
    }

    public String cadastrarVeiculos() {
        if (associado.getId() != 0) {
            try {
                associado = assDAO.findById(associado.getId());
            } catch (DAOException ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: cadastrarVeiculos()", ex);
            }
        }
        return "veiculos";
    }

    public String alterarVeiculo() {
        return "veiculos";
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public boolean isAssociadoSelecionado() {
        return associado.getId() != 0;
    }

    public boolean isAssociadosVazio() {
        return (null == associados || associados.isEmpty());
    }

    public boolean isDependentesVazio() {
        return (null == associado.getDependentes() || associado.getDependentes().isEmpty());
    }

    public boolean isVeiculosVazio() {
        return (null == associado.getVeiculos() || associado.getVeiculos().isEmpty());
    }

    public String incluirVeiculo() {

        if (veiculo.getId() == 0) {
            associado.getVeiculos().add(veiculo);
            veiculo.setAssociado(associado);
        }
        try {
            assDAO.updateEntity(associado);
            associado = assDAO.findById(associado.getId());
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: incluirVeiculo()", ex);
        }



        veiculo = new Veiculo();

        return "veiculos";
    }

    public String excluirVeiculo() {
        try {
            veiDAO.delete(veiculo);
            associado = assDAO.findById(associado.getId());
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: excluirVeiculo()", ex);
        }

        veiculo = new Veiculo();

        return "veiculos";
    }

    public String cadastrarDA() throws DAOException {
        ConvDA cda = DAOFactory.createConvDADAO().findById(1);
        if (associado.getId() != 0) {
            try {
                assDA = daDAO.buscaPorAssociado(associado);
            } catch (DAOException ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro: cadastrarDA()", ex);
            }
            if (null == assDA) {
                String aux = Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(associado.getId()), 5);
                aux = aux + Validacao.geraDigitoVerificador(aux);
                assDA = new AssDA();
                assDA.setIdenDA(aux);
                assDA.setAssociado(associado);
                assDA.setOpcao(EstadoDA.E);
                assDA.setDtOpcao(new Date());
                assDA.setConvenio(cda);
                salvarDA();
            }
        }
        return "associadoDA";
    }

    public boolean isCadastradoDA() {
        return (!(null == assDA));
    }

    public String getId() {
        if (associado.getId() != 0) {
            return Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(associado.getId()), 5)
                    + associado.getCategoria().name();
        } else {
            return "";
        }

    }

    public void salvarDA() {
        try {
            daDAO.addEntity(assDA);
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Falha ao chamar org.abepomi.mb.AssociadoMB.salvarDA()", ex);
        }
    }

    public void imprimirDA() throws Exception {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        ByteArrayOutputStream arquivo = new ByteArrayOutputStream();
        PdfUtil.gerarAutDebito(assDA, caminho, arquivo);
        DownloadArq.download(arquivo.toByteArray(), "AutorizaçãoDébito.pdf");
    }

    public void imprimirTermo() throws Exception {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        ByteArrayOutputStream arquivo = new ByteArrayOutputStream();
        PdfUtil.gerarTermoAnuencia(associado, caminho, arquivo);
        DownloadArq.download(arquivo.toByteArray(), "TermoAnuencia.pdf");
    }

    public String relVeiculos() throws DAOException {
        List<Veiculo> lista = DAOFactory.createVeiculoDAO().listaPorAssociado();
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Veículos");
        geraRelatorio(lista, parametros, "relVeiculos.jasper");
        return "sucesso";
    }

    private String relFatVivo() throws DAOException {
        List<RefVivoRel> lista = DAOFactory.createRelatorioVivoDAO().recuperarFaturaPorReferencia(dataInicio, associado.getId());
        associado = new Associado();
        Map parametros = new HashMap();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Demonstrativo de Utilização de Telefonia Vivo");
        parametros.put("SUBREPORT_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relFaturaTelefoneVivo.jasper");
        return "sucesso";
    }

    public String relLinhasPorAssociado() throws DAOException {
        List<AssociadoLinhaUtilRel> lista = DAOFactory.createAssociadoLinhaDAO().linhasPorAssociado();
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Linhas por Associado");
        geraRelatorio(lista, parametros, "relLinhasPorAssociado.jasper");
        return "sucesso";
    }

    public String relCustoPorLinha() throws DAOException {
        List<FaturaUtilRelCusto> lista = DAOFactory.createFaturaDAO().custoPorTelefone();
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Custo por Fatura");
        geraRelatorio(lista, parametros, "relCustoPorLinha.jasper");
        return "sucesso";
    }

    public String relLinhasCadastradas() throws DAOException {
        List<Linha> lista = DAOFactory.createLinhaDAO().findAll();
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Linhas Cadastradas");
        geraRelatorio(lista, parametros, "relLinhasCadastradas.jasper");
        return "sucesso";
    }

    public String relatorios() throws DAOException {
        if (fromId.equals("relEnviados")) {
            relEnviados();
            return "sucesso";
        }
        if (fromId.equals("relOcorrencias")) {
            relOcorrencias();
            return "sucesso";
        }
        if (fromId.equals("relDebitados")) {
            relDebitados();
            return "sucesso";
        }
        if (fromId.equals("relNDebitados")) {
            relNDebitados();
            //relFaturaTelefonica();
            return "sucesso";
        }
        if (fromId.equals("relFaturaTelefonica")) {
            relFaturaTelefonica();
            return "sucesso";
        }
        if (fromId.equals("relReceitasPorReferencia")) {
            relReceitasPorReferencia();
            return "sucesso";
        }
        if (fromId.equals("relReceitas")) {
            relReceitas();
            return "sucesso";
        }

        if (fromId.equals("relNaoEnviados")) {
            relNaoEnviados();
            return "sucesso";
        }

        if (fromId.equals("relFatVivo")) {
            relFatVivo();
            return "sucesso";
        }

        if (fromId.equals("enviarEmails")) {
            return enviarEmailFatura();
        }

        if (fromId.equals("envEmailsDeb")) {
            return enviarEmailNDebitados();
        }

        if (fromId.equals("relTitulosSituacao")) {
            return relTitulosSituacao();
        }

        return "falha";
    }

    public String relEnviados() throws DAOException {
        List<MovDA> lista = DAOFactory.createMovDADAO().buscaPorMesRef(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Débitos Enviados");
        geraRelatorio(lista, parametros, "relEnviados.jasper");
        return "";
    }

    public String relTitulosSituacao() throws DAOException {
        List<Titulo> lista = DAOFactory.createTituloDAO().buscarPorReferencia(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Títulos por Referência Situação");
        parametros.put("REFERENCIA", referencia.getRef());
        geraRelatorio(lista, parametros, "relTitulosSituacao.jasper");
        return "sucesso";
    }

    public String relDebCadastrados() throws DAOException {
        List<AssDA> lista = DAOFactory.createAssDADAO().buscaPorEstado(EstadoDA.C);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Débitos Automáticos Cadastrados");
        geraRelatorio(lista, parametros, "relDebCadastrados.jasper");
        return "";
    }

    public String relDebNaoCadastrados() throws DAOException {
        List<AssDA> lista = DAOFactory.createAssDADAO().buscaPorNaoEstado(EstadoDA.C);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Débitos Automáticos Não Cadastrados");
        geraRelatorio(lista, parametros, "relDebNCadastrados.jasper");
        return "";
    }

    private void geraRelatorio(List lista, Map parametros, String nome) {
        String caminhoRelJasper = //"/var/www/vhosts/abepomi.com.br/appservers/glassfish-3x/domains/domain1/docroot/WEB-INF/classes/relatorios/"+nome;
                FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/" + nome;

        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
        JasperPrint impressao = null;
        //System.out.println(caminhoRelJasper);
        try {

            impressao = JasperFillManager.fillReport(caminhoRelJasper, parametros, ds);
            try {
                DownloadArq.download(JasperExportManager.exportReportToPdf(impressao), "relatorio.pdf");
            } catch (Exception ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro ao gerar relatório: " + nome, ex);
            }
        } catch (JRException e) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Falha ao chamar org.abepomi.mb.AssociadoMB.gerarRelatorio", e);
        }
    }

    public String relOcorrencias() throws DAOException {
        List<MovDA> lista = DAOFactory.createMovDADAO().buscaPorMesRef(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Retorno do Débito Automático");
        geraRelatorio(lista, parametros, "relOcorrencias.jasper");
        return "";
    }

    public String relDebitados() throws DAOException {
        List<MovDA> lista = DAOFactory.createMovDADAO().buscaDebitados(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório dos Débitos Efetuados");
        geraRelatorio(lista, parametros, "relOcorrencias.jasper");
        return "";
    }

    private String relNDebitados() throws DAOException {
        List<MovDA> lista = DAOFactory.createMovDADAO().buscaNDebitados(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório dos Débitos Não Efetuados");
        geraRelatorio(lista, parametros, "relNDebitados.jasper");
        return "";
    }

    private String relReceitasPorReferencia() throws DAOException {
        List<FaturamentoRefRel> lista = DAOFactory.createFaturamentoDAO().receitaPorReferencia(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Receitas por Referência");
        geraRelatorio(lista, parametros, "relReceitaTotalRefLand_novo.jasper");
        return "";
    }

    private String relNaoEnviados() throws DAOException {
        List<FaturamentoRefRel> lista = DAOFactory.createFaturamentoDAO().faturadosSemDebito(referencia);
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatórios de Débitos Não Enviados ao Débito Automático");
        geraRelatorio(lista, parametros, "relReceitaTotalRefLand_novo.jasper");
        return "";
    }

    private String relReceitas() throws DAOException {
        List<FaturamentoRefRel> lista = DAOFactory.createFaturamentoDAO().receitaPorReferencia();
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Relatório de Receitas");
        geraRelatorio(lista, parametros, "relReceitaTotalRefLand_rec.jasper");
        return "";
    }

    private String relFaturaTelefonica() throws DAOException {
        List<DetalheTelefoniaUtilRel> lista = DAOFactory.createDetalheDAO().getTotaisLinhaPorReferencia(dataInicio, associado.getId());
        associado = new Associado();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        Map parametros = new HashMap();
        parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        parametros.put("NOME_RELATORIO", "Demonstrativo de Utilização de Telefonia");
        parametros.put("SUB_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relFaturaTelefone.jasper");
        return "";
    }

    public void exportarEmails() throws DAOException, Exception {
        DownloadArq.download(ArqEmails.exportarCSV(assDAO.findAll()), "Abepomi.csv");
    }

    public String desativar() {
        associado.setExclusao(new Date());
        associado.setStatus(Status.I);
        try {
            assDAO.updateEntity(associado);
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro ao desativar associado!", ex);
            return "falha";
        }
        associado = new Associado();
        return "sucesso";
    }

    public String reativar() {
        associado.setInclusao(new Date());
        associado.setExclusao(null);
        associado.setStatus(Status.A);
        try {
            assDAO.updateEntity(associado);
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro ao desativar associado!", ex);
            return "falha";
        }
        associado = new Associado();
        return "sucesso";
    }

    public List<Associado> getAssociadosNome() {
        try {
            return assDAO.findAllByNome();
        } catch (DAOException ex) {
            Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Erro ao retornar associado!!!", ex);
        }

        return null;
    }

    public String enviarEmailFatura() {
        List<AssociadoEmail> assocs = new ArrayList<AssociadoEmail>();
        Map<AssociadoEmail, byte[]> map = new HashMap<AssociadoEmail, byte[]>();

        assocs = DAOFactory.createAssociadoDAO().buscaAssociadoEmail();

        for (AssociadoEmail a : assocs) {
            Titulo titulo = null;
            try {
                titulo = DAOFactory.createTituloDAO().buscarPorAssRef(DAOFactory.createAssociadoDAO().findById(a.getId()), referencia);
            } catch (DAOException ex) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, null, ex);
            }
            map.put(a, CobrancaBB.gerarCobrancaTitulo(titulo));
        }

        EmailMensalThread enviarEmais = new EmailMensalThread(map,referencia);
        enviarEmais.start();
        return "sucesso";
    }

    public String enviarEmailNDebitados() {
        List<Faturamento> fat = null;
        fat = DAOFactory.createFaturamentoDAO().faturamentoAssociadoNDebitado(referencia);
        EmailAbepomi email = new EmailAbepomi();
        for (Faturamento f : fat) {
            String assunto = "Fatura ABEPOMI - " + f.getReferencia().getRef();
            String texto = "Prezado(a) Sr(a) " + f.getAssociado().getNome().toUpperCase()
                    + ",\n\nConfira no link abaixo seu extrato mensal e o Boleto Bancário para quem ainda não tem o cadastro de débito automático.\n";
            String ran = String.valueOf(new Random().nextInt(100000));
            String cpf = f.getAssociado().getCpf();
            String ass = Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(f.getAssociado().getId()), 5);
            String ref = Validacao.validaTamanho(Validacao.NUMERICO, String.valueOf(f.getReferencia().getId()), 5);
            String has = null;
            try {
                has = GeraMd5.md5(cpf + ass + ref + ran);
            } catch (Exception e) {
                Logger.getLogger(AssociadoMB.class.getName()).log(Level.SEVERE, "Falha ao gerar hash ao enviar emails: public String enviarEmailFatura()", e);
                return "falha";
            }
            String link = "http://www.abepomi.com.br/sistema/faturaweb.jsf?cpf=" + cpf + "&ass=" + ass + "&ref=" + ref + "&ran=" + ran + "&has=" + has;
            //String link = "http://localhost:8080/ABEPOMI/faturaweb.jsf?cpf=" + cpf + "&ass=" + ass + "&ref=" + ref + "&ran=" + ran + "&has=" + has;
            texto = texto + link
                    + "\n\nAssociado(a) coopere com a Abepomi faça seu cadastro para pagamento através de débito automático,\n"
                    + "para quem ainda não autorizou o débito automático informamos que a partir de junho de 2012 estaremos\n"
                    + "cobrando multa e juros para quem pagar com atraso, portanto pague em dia seu boleto e economize dinheiro."
                    + "\n\nAtenciosamente,\n\nABEPOMI.";
            List<Titulo> titulos = new ArrayList();

            Titulo titulo = DAOFactory.createTituloDAO().buscarPorAssRef(f.getAssociado(), f.getReferencia());
            if (null != titulo) {
                titulos.add(titulo);
            }
            if (f.getAssociado().getEmail1().equals("")) {
                email.sendMail("abepomi@gmail.com,juliannoms@gmail.com", assunto, texto, CobrancaBB.gerarCobrancaTitulos(titulos));
            } else {
                email.sendMail(f.getAssociado().getEmail1() + "," + f.getAssociado().getEmail2(), assunto, texto, CobrancaBB.gerarCobrancaTitulos(titulos));
            }
        }
        return "sucesso";
    }
}
