/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.abepomi.dao.AparelhoDAO;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.FornecedorDAO;
import org.abepomi.dao.NotaFiscalDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.model.Aparelho;
import org.abepomi.model.Fornecedor;
import org.abepomi.model.ItensNota;
import org.abepomi.model.NotaFiscal;
import org.abepomi.model.util.OperacaoNota;
import org.abepomi.model.util.TipoPessoa;

/**
 *
 * @author Julianno
 */
@ManagedBean(name = "fornMB")
@ViewScoped
public class FornecedorMB implements Serializable {

    private Fornecedor fornecedor = new Fornecedor();
    private List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
    //
    private Aparelho aparelho = new Aparelho();
    private List<Aparelho> aparelhos = new ArrayList<Aparelho>();
    //
    private NotaFiscal nota = new NotaFiscal();
    private ItensNota item = new ItensNota();
    //
    private boolean mostrarAparelhos = false;

    public FornecedorMB() {
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public List<Fornecedor> getFornecedores() {
        try {
            fornecedores = DAOFactory.createFornecedorDAO().findAll();
        } catch (DAOException ex) {
            Logger.getLogger(FornecedorMB.class.getName()).log(Level.SEVERE, "Erro ao recuperar fornecedores!", ex);
        }
        return fornecedores;
    }

    public void setFornecedores(List<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

    public Aparelho getAparelho() {
        return aparelho;
    }

    public void setAparelho(Aparelho aparelho) {
        this.aparelho = aparelho;
    }

    public boolean isMostrarAparelhos() {
        return mostrarAparelhos;
    }

    public void setMostrarAparelhos(boolean mostrarAparelhos) {
        this.mostrarAparelhos = mostrarAparelhos;
    }

    public List<Aparelho> getAparelhos() {
        try {
            aparelhos = DAOFactory.createAparelhoDAO().findAll();
        } catch (DAOException ex) {
            Logger.getLogger(FornecedorMB.class.getName()).log(Level.SEVERE, "Erro ao recuperar aparelhos!", ex);
        }
        return aparelhos;
    }

    public void setAparelhos(List<Aparelho> aparelhos) {
        this.aparelhos = aparelhos;
    }

    public NotaFiscal getNota() {
        return nota;
    }

    public void setNota(NotaFiscal nota) {
        this.nota = nota;
    }

    public ItensNota getItem() {
        return item;
    }

    public void setItem(ItensNota item) {
        this.item = item;
    }

    public String salvarFornecedor() {
        FornecedorDAO fornDAO = DAOFactory.createFornecedorDAO();
        try {
            //para ser tirado 
            fornecedor.setTipo(TipoPessoa.J);
            fornecedor.setAssociacao(DAOFactory.createAssociacaoDAO().findById(1));
            //**************
            if (fornecedor.getId() == 0) {
                fornDAO.addEntity(fornecedor);
            } else {
                fornDAO.updateEntity(fornecedor);
            }
        } catch (DAOException ex) {
            Logger.getLogger(FornecedorMB.class.getName()).log(Level.SEVERE, "Erro ao salvar fornecedor!", ex);
            return "Falha";
        }
        return "sucesso";
    }

    public String salvarAparelho() {
        AparelhoDAO apaDAO = DAOFactory.createAparelhoDAO();
        try {
            if (aparelho.getId() == 0) {
                apaDAO.addEntity(aparelho);
            } else {
                apaDAO.updateEntity(aparelho);
            }
        } catch (DAOException ex) {
            Logger.getLogger(FornecedorMB.class.getName()).log(Level.SEVERE, "Erro ao salvar aparelho!", ex);
            return "falha";
        }
        return "sucesso";
    }

    public String limparAparelho(){
        aparelho = new Aparelho();
        return "/notafiscal/aparelhos";
    }

    public String salvarNota() {
        /*para melhorar*/
        nota.setOperacao(OperacaoNota.E);
        /***************/

        if(nota.getItens().isEmpty()){
            FacesContext.getCurrentInstance().addMessage("tbItens", new FacesMessage("A nota fiscal deve ter pelo menos 1 item!"));
            return null;
        }
        nota.setValor(getTotalNF());
        NotaFiscalDAO notaDAO = DAOFactory.createNotaFiscalDAO();
        try {
            notaDAO.addEntity(nota);
        } catch (DAOException ex) {
            Logger.getLogger(FornecedorMB.class.getName()).log(Level.SEVERE, "Erro ao salvar nota fiscal!", ex);
            return "falha";
        }
        return "sucesso";
    }

    public String limparNota() {
        nota = new NotaFiscal();
        return "cadastroNota";
    }

    public String incluirItem() {
        boolean incluir = true;
        if (null == item.getAparelho()) {
            FacesContext.getCurrentInstance().addMessage("aparelho", new FacesMessage("Selecione um aparelho!"));
            incluir = false;
        }
        if (item.getQtde() == 0) {
            FacesContext.getCurrentInstance().addMessage("qtde", new FacesMessage("Quantidade deve ser maior que 0!"));
            incluir = false;
        }
        /*if(nota.getOperacao().equals(OperacaoNota.S)&&item.getQtde()>aparelho.getEstoque()){
            FacesContext.getCurrentInstance().addMessage("qtde", new FacesMessage("Quantidade maior que o estoque disponível!"));
            incluir = false;
        }*/
        if (incluir) {
            tratarInclusaoItem();
            item = new ItensNota();
        }
        return null;
    }

    /*public Map getOperacoes() {
        Map<OperacaoNota, String> op = new LinkedHashMap<OperacaoNota, String>();
        for (OperacaoNota type : OperacaoNota.values()) {
            op.put(type, type.name());
        }
        return op;
    }*/

    private void tratarInclusaoItem() {
        for (ItensNota i : nota.getItens()) {
            if (i.getAparelho().equals(item.getAparelho())) {
                i.setPreco(item.getPreco());
                i.setQtde(item.getQtde());
                return;
            }
        }
        nota.getItens().add(item);
        item.setNotaFiscal(nota);
    }

    public Double getTotalNF(){
        Double total = 0.00;
        for (ItensNota it : nota.getItens()){
            total = total + it.getPreco() * it.getQtde();
        }
        return total;
    }

    public String carregarAlteracao(){
        return "/notafiscal/aparelhos";
    }
}
