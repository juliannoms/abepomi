/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import org.abepomi.arqs.FaturaTelefonia;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.mb.util.FacesUtil;
import org.abepomi.model.Aparelho;
import org.abepomi.model.AparelhosAssociados;
import org.abepomi.model.AssociacaoLinha;
import org.abepomi.model.Associado;
import org.abepomi.model.AssociadoLinha;
import org.abepomi.model.Contrato;
import org.abepomi.model.DetalheTelefonia;
import org.abepomi.model.ExtrasTelefonia;
import org.abepomi.model.Linha;
import org.abepomi.model.Plano;
import org.abepomi.model.util.TipoLinha;

/**
 *
 * @author Julianno
 */
public class TelefoniaMB implements Serializable {

    private static final String LOCAL_DO_ARQUIVO = "/upload/telefonia/";
    //
    private Plano plano = null;
    private Contrato contrato = null;
    private Linha linha = null;
    private AssociadoLinha assLinha = null;
    private Associado associado = null;
    private AssociacaoLinha asoLinha = null;
    private ExtrasTelefonia extraTel = null;
    private DetalheTelefonia detalhe = null;
    private AparelhosAssociados apaAss = null;
    //
    private List<Plano> planos = null;
    private List<Contrato> contratos = null;
    private List<Linha> linhas = null;
    private List<Linha> linhasLivres = null;
    private List<String[]> arquivos = null;
    private List<AssociadoLinha> linhasAssociados = null;
    private List<AssociacaoLinha> linhasAssociacao = null;
    private List<DetalheTelefonia> detalhesAssociado = null;
    private List<Aparelho> aparelhos = null;
    //
    private int parcelasHabil = 0;
    private Double habilitacao = 0.00;
    //
    private boolean atualizaContratos = true;
    private boolean atualizaLinhas = true;
    private boolean atualizaPlanos = true;
    private boolean atualizaLinhasLivres = true;
    private boolean comAparelho = false;
    private boolean comHabilitacao = false;
    //private boolean atualizaDetalhes = true;
    private String arquivoSelecionado = "";

    public TelefoniaMB() {
        contrato = new Contrato();
        linha = new Linha();
        plano = new Plano();
        assLinha = new AssociadoLinha();
        associado = new Associado();
        asoLinha = new AssociacaoLinha();
        extraTel = new ExtrasTelefonia();
        detalhe = new DetalheTelefonia();
        apaAss = new AparelhosAssociados();
        //
        contratos = new ArrayList<Contrato>();
        linhas = new ArrayList<Linha>();
        linhasLivres = new ArrayList<Linha>();
        planos = new ArrayList<Plano>();
        arquivos = new ArrayList<String[]>();
        linhasAssociados = new ArrayList<AssociadoLinha>();
        linhasAssociacao = new ArrayList<AssociacaoLinha>();
        detalhesAssociado = new ArrayList<DetalheTelefonia>();
        aparelhos = new ArrayList<Aparelho>();
    }

    public ExtrasTelefonia getExtraTel() {
        return extraTel;
    }

    public void setExtraTel(ExtrasTelefonia extraTel) {
        this.extraTel = extraTel;
    }

    public List<AssociacaoLinha> getLinhasAssociacao() {
        linhasAssociacao = DAOFactory.createAssociacaoLinhaDAO().linhasUsoAssociacao();
        return linhasAssociacao;
    }

    public void setLinhasAssociacao(List<AssociacaoLinha> linhasAssociacao) {
        this.linhasAssociacao = linhasAssociacao;
    }

    public AssociacaoLinha getAsoLinha() {
        return asoLinha;
    }

    public void setAsoLinha(AssociacaoLinha asoLinha) {
        this.asoLinha = asoLinha;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public List<AssociadoLinha> getLinhasAssociados() {
        return linhasAssociados;
    }

    public void setLinhasAssociados(List<AssociadoLinha> linhasAssociados) {
        this.linhasAssociados = linhasAssociados;
    }

    public String getArquivoSelecionado() {
        return arquivoSelecionado;
    }

    public void setArquivoSelecionado(String arquivoSelecionado) {
        this.arquivoSelecionado = arquivoSelecionado;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public int getParcelasHabil() {
        return parcelasHabil;
    }

    public void setParcelasHabil(int parcelasHabil) {
        this.parcelasHabil = parcelasHabil;
    }

    public Double getHabilitacao() {
        return habilitacao;
    }

    public void setHabilitacao(Double habilitacao) {
        this.habilitacao = habilitacao;
    }

    public boolean isComAparelho() {
        return comAparelho;
    }

    public void setComAparelho(boolean comAparelho) {
        this.comAparelho = comAparelho;
    }

    public boolean isComHabilitacao() {
        return comHabilitacao;
    }

    public void setComHabilitacao(boolean comHabilitacao) {
        this.comHabilitacao = comHabilitacao;
    }

    public List<Contrato> getContratos() {
        if (contratos.isEmpty() || atualizaContratos) {
            try {
                contratos = DAOFactory.createContratoDAO().findAll();
                atualizaContratos = false;
            } catch (DAOException ex) {
                Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao retornar Contratos!", ex);
            }
        }
        return contratos;
    }

    public void setContratos(List<Contrato> contratos) {
        this.contratos = contratos;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    public List<Linha> getLinhas() {
        if (linhas.isEmpty() || atualizaLinhas) {
            try {
                linhas = DAOFactory.createLinhaDAO().findAll();
            } catch (DAOException ex) {
                Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao retornar linhas!", ex);
            }
        }
        return linhas;
    }

    public List<Linha> getLinhasLivres() {
        if (linhasLivres.isEmpty() || atualizaLinhasLivres || atualizaLinhas) {
            linhasLivres = DAOFactory.createLinhaDAO().getLinhasLivres();
        }
        return linhasLivres;
    }

    public void setLinhas(List<Linha> linhas) {
        this.linhas = linhas;
    }

    public AssociadoLinha getAssLinha() {
        return assLinha;
    }

    public void setAssLinha(AssociadoLinha assLinha) {
        this.assLinha = assLinha;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public List<Plano> getPlanos() {
        if (planos.isEmpty() || atualizaPlanos) {
            try {
                planos = DAOFactory.createPlanoDAO().findAll();
                atualizaPlanos = false;
            } catch (DAOException ex) {
                Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao retornar planos!", ex);
            }
        }
        return planos;
    }

    public void setPlanos(List<Plano> planos) {
        this.planos = planos;
    }

    public DetalheTelefonia getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(DetalheTelefonia detalhe) {
        this.detalhe = detalhe;
    }

    public List<DetalheTelefonia> getDetalhesAssociado() {
        return detalhesAssociado;
    }

    public AparelhosAssociados getApaAss() {
        return apaAss;
    }

    public void setApaAss(AparelhosAssociados apaAss) {
        this.apaAss = apaAss;
    }

    public List<Aparelho> getAparelhos() {
        return DAOFactory.createAparelhoDAO().buscarComEstoque();
    }

    public void setAparelhos(List<Aparelho> aparelhos) {
        this.aparelhos = aparelhos;
    }

    public String salvarContrato() {
        atualizaContratos = true;
        try {
            if (contrato.getId() == 0) {
                DAOFactory.createContratoDAO().addEntity(contrato);
            } else {
                DAOFactory.createContratoDAO().updateEntity(contrato);
            }
            contrato = new Contrato();
            return "contratos";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao cadastrar contrato!", ex);
            //FacesMessage message = new FacesMessage("Erro ao cadastrar contrato!");

        }
        return "falha";
    }

    public String limparContrato() {
        contrato = new Contrato();
        return "contratos";
    }

    public String salvarLinha() {
        atualizaLinhas = true;
        try {
            DAOFactory.createLinhaDAO().addEntity(linha);
            linha = new Linha();
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao cadastrar a linha!", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro ao cadastrar linha!!!",null));
        }
        return "linhas";
    }

    public String limparLinha() {
        linha = new Linha();
        return "linhas";
    }

    public String salvarPlano() {
        atualizaPlanos = true;
        try {
            if (plano.getCodigo() == 0) {
                DAOFactory.createPlanoDAO().addEntity(plano);
            } else {
                DAOFactory.createPlanoDAO().updateEntity(plano);
            }
            plano = new Plano();
            return "planos";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao cadastrar o plano!", ex);
        }
        return "falha";

    }

    public String limparPlano() {
        plano = new Plano();
        return "planos";
    }

    public String salvarAssociadoLinha() {

        /**/
        atualizaLinhasLivres = true;
        List<ExtrasTelefonia> extras = new ArrayList<ExtrasTelefonia>();
        if (isComHabilitacao()) {
            BigDecimal habilitacaoB = new BigDecimal(habilitacao.toString()).setScale(2);
            BigDecimal parcelasB = new BigDecimal(parcelasHabil + ".00").setScale(2);
            BigDecimal parcela = habilitacaoB.divide(parcelasB, 2, RoundingMode.DOWN);
            BigDecimal resto = habilitacaoB.subtract(parcela.multiply(parcelasB));
            BigDecimal auxiliar = new BigDecimal("0.01").setScale(2);
            for (int i = 1; i <= parcelasHabil; i++) {
                ExtrasTelefonia extra = new ExtrasTelefonia();
                extra.setDescricao("HABILITAÇÃO DE NOVA LINHA " + i);
                extra.setLinha(assLinha.getLinha().getNumero());
                if (resto.doubleValue() > 0.00) {
                    extra.setValor(parcela.add(auxiliar).doubleValue());
                    resto = resto.subtract(auxiliar);
                } else {
                    extra.setValor(parcela.doubleValue());
                }
                extra.setTipo(ExtrasTelefonia.TIPO_HABILITACAO);
                extras.add(extra);
            }
        }

        if (isComAparelho() && apaAss.getValor() > 0.00) {
            BigDecimal aparelhoB = new BigDecimal(apaAss.getValor().toString()).setScale(2);
            BigDecimal parcelasB = new BigDecimal(apaAss.getParcelas() + ".00").setScale(2);
            BigDecimal parcela = aparelhoB.divide(parcelasB, 2, RoundingMode.DOWN);
            BigDecimal resto = aparelhoB.subtract(parcela.multiply(parcelasB));
            BigDecimal auxiliar = new BigDecimal("0.01").setScale(2);
            //Double parcela = aparelho / parcelasAparelho;
            for (int i = 1; i <= apaAss.getParcelas(); i++) {
                ExtrasTelefonia extra = new ExtrasTelefonia();
                extra.setDescricao(i + "ª PARCELA " + apaAss.getAparelho().getNome());
                extra.setLinha(assLinha.getLinha().getNumero());
                if (resto.doubleValue() > 0.00) {
                    extra.setValor(parcela.add(auxiliar).doubleValue());
                    resto = resto.subtract(auxiliar);
                } else {
                    extra.setValor(parcela.doubleValue());
                }
                extra.setTipo(ExtrasTelefonia.TIPO_APARELHO);
                extras.add(extra);
            }
        }
        assLinha.getLinha().setLivre(false);
        try {
            DAOFactory.createAssociadoLinhaDAO().addEntity(assLinha);
            if (isComAparelho()) {
                apaAss.setAssociado(assLinha.getAssociado());
                apaAss.setData(assLinha.getEntrega());
                DAOFactory.createApAssociadosDAO().addEntity(apaAss);
            }
            for (ExtrasTelefonia e : extras) {
                DAOFactory.createExtrasTelefoniaDAO().addEntity(e);
            }
            return limparAssociadoLinha();
        } catch (DAOException daoex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao vender linha!", daoex);
        }
        return "falha";
    }

    public String salvarAssociacaoLinha() {
        atualizaLinhasLivres = true;
        asoLinha.getLinha().setLivre(false);
        try {
            DAOFactory.createAssociacaoLinhaDAO().addEntity(asoLinha);
            return limparAssociacaoLinha();
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao registrar linha p/ associação!", ex);
        }
        return "falha";
    }

    public String limparAssociacaoLinha() {
        asoLinha = new AssociacaoLinha();
        return "associacaoLinhas";
    }

    public String limparAssociadoLinha() {
        assLinha = new AssociadoLinha();
        apaAss = new AparelhosAssociados();
        habilitacao = 0.00;
        parcelasHabil = 0;
        return "associadosLinhas";
    }

    public List<SelectItem> getTiposLinha() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        for (TipoLinha type : TipoLinha.values()) {
            lista.add(new SelectItem(type, type.toString()));
        }
        return lista;
    }

    public List<Associado> getAssociados() {
        try {
            return DAOFactory.createAssociadoDAO().findAllByNome();
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao retornar associados!", ex);
        }
        return null;
    }

    public List<String[]> carregarArquivos() {
        File[] files = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/telefonia") + "/").listFiles();
        List<String[]> lista = new ArrayList<String[]>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for (File file : files) {
            String[] arq = {"", "", ""};
            arq[0] = file.getName();
            arq[1] = String.valueOf(sdf.format(new Date(file.lastModified())));
            arq[2] = String.valueOf(file.length()) + " bytes";
            lista.add(arq);
        }
        return lista;
    }

    public List<String[]> getArquivos() {
        arquivos = carregarArquivos();
        return arquivos;
    }

    public String getLocalArquivo() {
        return LOCAL_DO_ARQUIVO;
    }

    public String salvarArquivo() {
        return "sucesso";
    }

    public String apagarArquivo() {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/upload/telefonia");
        File file = new File(caminho + "/" + arquivoSelecionado);
        if (file.delete()) {
            return "sucesso";
        }
        return "falha";
    }

    public String processarArquivo() {
        String erro = "";
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath(LOCAL_DO_ARQUIVO);
        File file = new File(caminho + "/" + arquivoSelecionado);
        try {
            //System.out.println("Começou aqui: " + new Date());
            FaturaTelefonia.processar(file);
            //FaturaTelefonia.faturar(null);
            //System.out.println("Terminou aqui: " + new Date());
            return "faturar";
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro: arquivo não encontrado!", ex);
            erro = "Arquivo não encontrado!";
        } catch (IOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro: não foi possível acessar o arquivo!", ex);
            erro = "Não foi possível acessar o arquivo!";
        } catch (ParseException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro: formato inválido!", ex);
            erro = "Formato inválido!";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro: não foi possível gravar a fatura!\n" + ex.getMessage(), ex);
            erro = "Não foi possível gravar a fatura!\n" + ex.getMessage();
        } catch (Exception ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            erro = ex.getMessage();
        } finally {
            file = null;
        }
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, erro, erro);
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "falha";
    }

    public void validarDataEntrega(FacesContext context, UIComponent toValidate, Object value) {
        Date data = (Date) value;
        if (data.getTime() > new Date().getTime()) {
            throw new ValidatorException(new FacesMessage("Data de entrega posterior a data de hoje!"));
        }
        if (data.getTime() < new Date().getTime() - 1000 * 60 * 60 * 24 * 35L) {
            throw new ValidatorException(new FacesMessage("Data da entrega anterior a 35 dias!"));
        }
    }

    public void carregarLinhasAssociado(ValueChangeEvent event) {
        if (event.getNewValue() != event.getOldValue()) {
            assLinha = new AssociadoLinha();
            linhasAssociados = new ArrayList<AssociadoLinha>();
            if (event.getNewValue() instanceof Associado) {
                linhasAssociados = DAOFactory.createAssociadoLinhaDAO().linhasAtivasPorAssociado((Associado) event.getNewValue());
            }
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public void carregarLinhasAssociadoAFaturar(ValueChangeEvent event) {
        if (event.getNewValue() != event.getOldValue()) {
            detalhe = new DetalheTelefonia();
            detalhesAssociado = new ArrayList<DetalheTelefonia>();
            if (event.getNewValue() instanceof Associado) {
                detalhesAssociado = DAOFactory.createDetalheDAO().getDetalhesSFaturar(((Associado) event.getNewValue()).getId());
            }
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public void carregarAsoLinha(ValueChangeEvent event) {
        if (event.getNewValue() != event.getOldValue()) {
            if (event.getNewValue() instanceof AssociacaoLinha) {
                asoLinha = (AssociacaoLinha) event.getNewValue();
            }
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public String devolverAssociadoLinha() {
        try {
            assLinha.getLinha().setLivre(true);
            DAOFactory.createAssociadoLinhaDAO().updateEntity(assLinha);
            List<ExtrasTelefonia> extras = DAOFactory.createExtrasTelefoniaDAO().getPorLinhaSFaturar(assLinha.getLinha().getNumero());
            for (ExtrasTelefonia extra : extras) {
                extra.setDescricao(extra.getDescricao() + "-" + extra.getLinha());
                extra.setTipo(ExtrasTelefonia.TIPO_TRANSFERIDO);
                extra.setLinha(String.valueOf(assLinha.getAssociado().getId()));
                DAOFactory.createExtrasTelefoniaDAO().updateEntity(extra);
            }
            assLinha = new AssociadoLinha();
            associado = new Associado();
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar devolução!", null);
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "falha";
    }

    public String devolverAssociacaoLinha() {
        try {
            asoLinha.getLinha().setLivre(true);
            DAOFactory.createAssociacaoLinhaDAO().updateEntity(asoLinha);
            asoLinha = new AssociacaoLinha();
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar devolução!", null);
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "falha";
    }

    public String salvarLancamentoExtra() {
        //extraTel.setTipo(ExtrasTelefonia.TIPO_DESCONTO);
        extraTel.setLinha(detalhe.getLinha());
        extraTel.setFaturada(true);
        extraTel.setFatura(detalhe.getFatura());
        try {
            DAOFactory.createExtrasTelefoniaDAO().addEntity(extraTel);
            if (extraTel.getTipo().equals(ExtrasTelefonia.TIPO_ACRESCIMO)) {
                detalhe.setAcrescimo(new BigDecimal(detalhe.getAcrescimo().toString()).setScale(2, RoundingMode.DOWN).add(new BigDecimal(extraTel.getValor().toString()).setScale(2, RoundingMode.DOWN)).doubleValue());
            } else {
                detalhe.setDesconto(new BigDecimal(detalhe.getDesconto().toString()).setScale(2, RoundingMode.DOWN).add(new BigDecimal(extraTel.getValor().toString()).setScale(2, RoundingMode.DOWN)).doubleValue());
            }
            DAOFactory.createDetalheDAO().updateEntity(detalhe);
            assLinha = new AssociadoLinha();
            associado = new Associado();
            extraTel = new ExtrasTelefonia();
            return "sucesso";
        } catch (DAOException daoex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, daoex.getMessage(), daoex);
        }
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao cadastrar lançamento extra!", null);
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "falha";
    }

    public String alteraPlanoLinha() {
        try {
            DAOFactory.createAssociadoLinhaDAO().updateEntity(assLinha);
            assLinha = new AssociadoLinha();
            associado = new Associado();
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao trocar plano de telefonia!", null);
        FacesContext.getCurrentInstance().addMessage(null, message);
        return "falha";
    }

    public Double getTotal() {
        return detalhe.getAcrescimo() + detalhe.getAparelho() - detalhe.getCredito() - detalhe.getDesconto() + (detalhe.getExcedidos() * detalhe.getValor_minuto())
                + detalhe.getFranquia() + detalhe.getHabilitacao() + detalhe.getOutros() + detalhe.getPacotes() + detalhe.getPlano();
    }

    public String faturar() {
        try {
            FaturaTelefonia.faturar(null);
            return "sucesso";
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaMB.class.getName()).log(Level.SEVERE, "Erro ao faturar!", ex);
        }
        return "falha";
    }

    public void carregarValorAparelho(AjaxBehaviorEvent event) {
        getApaAss().setValor(getApaAss().getAparelho().getValorVenda());
        FacesUtil.cleanSubmittedValues(event.getComponent().getParent());
    }

    public void validarValorDesconto(FacesContext context, UIComponent toValidate, Object value) {
        Double valor = new Double(value.toString());
        if (valor == 0.00) {
            throw new ValidatorException(new FacesMessage("Valor deve ser maior que 0,00!"));
        }
        if (extraTel.getTipo().equals(ExtrasTelefonia.TIPO_DESCONTO) && valor > getTotal()) {
            throw new ValidatorException(new FacesMessage("Valor deve ser menor que o valor a faturar!"));
        }
    }
}
