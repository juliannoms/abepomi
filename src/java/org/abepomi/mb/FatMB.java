/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.ExtrasDAO;
import org.abepomi.dao.FaturamentoDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.DetalheTelefoniaUtil;
import org.abepomi.model.Associacao;
import org.abepomi.model.Associado;
import org.abepomi.model.Extras;
import org.abepomi.model.Faturamento;
import org.abepomi.model.Referencia;
import org.abepomi.model.cobranca.Parcela;
import org.abepomi.model.cobranca.util.TituloUtil;
import org.abepomi.model.util.Categoria;
import org.abepomi.model.util.Status;

/**
 *
 * @author Julianno
 */
public class FatMB implements Serializable {

    private Referencia referencia;
    private Extras extra;
    private Date vencimento = null;
    private boolean naoDebitados = false;

    public FatMB() {
        referencia = new Referencia();
        extra = new Extras();
    }

    public Extras getExtra() {
        return extra;
    }

    public void setExtra(Extras extra) {
        this.extra = extra;
    }

    public Referencia getReferencia() {
        return referencia;
    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Referencia> getReferencias() {
        return DAOFactory.createReferenciaDAO().buscaRefSemFaturamento();
    }

    public List<Referencia> getTodasReferencias() throws DAOException {
        return DAOFactory.createReferenciaDAO().buscaTodasDecrescente();
    }

    public boolean isNaoDebitados() {
        return naoDebitados;
    }

    public void setNaoDebitados(boolean naoDebitados) {
        this.naoDebitados = naoDebitados;
    }

    public String salvarReferencia() {
        try {
            DAOFactory.createReferenciaDAO().addEntity(referencia);
            referencia = new Referencia();
        } catch (DAOException ex) {
            Logger.getLogger(AssociacaoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar referência!", ex);
            return "falha";
        }
        return "sucesso";
    }

    public String gerarReferencia() {

        Referencia r = DAOFactory.createReferenciaDAO().buscarUltima();
        if (null == r) {
            referencia.setRef(new SimpleDateFormat("MMyyyy").format(new Date()));
        }else if(r.isFaturado()){
            GregorianCalendar c = new GregorianCalendar();
            try {
                c.setTime(new SimpleDateFormat("MMyyyy").parse(r.getRef()));
                c.add(GregorianCalendar.MONTH, 1);
                referencia.setRef(new SimpleDateFormat("MMyyyy").format(c.getTime()));
            } catch (ParseException ex) {
                Logger.getLogger(FatMB.class.getName()).log(Level.SEVERE, "Erro...", ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao parsear data!", null));
                return "falha";
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Existe uma referência sem faturar!", null));
            return "falha";
        }
        referencia.setGeracao(new Date());
        referencia.setFaturado(false);
        return "referencia";
    }

    public String gerarFaturamento() throws DAOException {

        /*
         * Colocar aqui comparação entre quantidade de contratos e fechamento a faturar...
         * se igual continua, senão para!
         */

        List<Associado> associados = DAOFactory.createAssociadoDAO().findAll();
        DecimalFormat df = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        Associacao aso = DAOFactory.createAssociacaoDAO().findById(1);
        Double contribuicao = aso.getMultContribuicao() * aso.getUnContribuicao() / 100;
        FaturamentoDAO fatDAO = DAOFactory.createFaturamentoDAO();

        DAOFactory.createFechamentoTelefoniaDAO().atualizarReferencia(referencia);

        for (Associado ass : associados) {
            if (!ass.getStatus().equals(Status.I)) {
                Faturamento fat = new Faturamento();
                fat.setReferencia(referencia);
                fat.setAssociado(ass);
                fat.setServicos(0.00);
                if (ass.isConveniado()) {
                    fat.setMensalidade(0.00);
                } else {
                    if (ass.getCategoria().equals(Categoria.C)) {
                        fat.setMensalidade(Double.parseDouble(df.format(contribuicao * 3.00)));
                    } else if (ass.getCategoria().equals(Categoria.F) || ass.getCategoria().equals(Categoria.T)) {
                        fat.setMensalidade(Double.parseDouble(df.format(contribuicao * 2.00)));
                    } else {
                        fat.setMensalidade(0.00);
                    }
                }
                fat.setExtra(DAOFactory.createExtrasDAO().totalPorAssociado(ass, referencia));

                DetalheTelefoniaUtil tel = DAOFactory.createDetalheDAO().getTotalPorReferenciaAssociado(referencia.getId(), ass.getId());
                fat.setTelefonia(tel.getTotal());
                fat.setVivo(DAOFactory.createDetalheVivoDAO().totalVivoPorAssociado(ass.getId()));

                List<Parcela> parcelas = DAOFactory.createParcelaDAO().parcelasAFaturar(ass.getId());
                if (null != parcelas) {
                    for (Parcela p : parcelas) {
                        fat.setParcelas(fat.getParcelas().add(p.getValor()));
                        p.setSituacao(6);
                        p.setReferencia(referencia);
                        DAOFactory.createParcelaDAO().updateEntity(p);
                    }
                }


                fatDAO.addEntity(fat);
            }
        }
        referencia.setFaturado(true);
        DAOFactory.createReferenciaDAO().updateEntity(referencia);
        DAOFactory.createFaturaVivoDAO().atualizarNaoFaturadas(referencia.getId());

        return "sucesso";
    }

    public String salvarExtra() {
        ExtrasDAO exDAO = DAOFactory.createExtrasDAO();
        try {
            extra.setId(exDAO.proximaId());
            exDAO.addEntity(extra);
            extra = new Extras();
        } catch (DAOException ex) {
            Logger.getLogger(FatMB.class.getName()).log(Level.SEVERE, "Erro: org.abepomi.mb.FatMB.salvarExtra()", ex);
            return "falha";
        }
        return "sucesso";
    }

    public String gerarBoletos() {
        List<Faturamento> fat = null;
        if (naoDebitados) {
            fat = DAOFactory.createFaturamentoDAO().faturamentoAssociadoNDebitado(referencia);
        } else {
            fat = DAOFactory.createFaturamentoDAO().faturamentoAssociadoBoleto(referencia);
        }
        try {
            TituloUtil.gerarTitulosFaturamentos(fat, vencimento);
        } catch (DAOException ex) {
            Logger.getLogger(FatMB.class.getName()).log(Level.SEVERE, "Erro ao gerarBoletos", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro ao gerar os boletos!", null));
            return "falha";
        }
        return "sucesso";
    }
}
