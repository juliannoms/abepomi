/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.abepomi.cobranca.CobrancaBB;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.FaturamentoDAO;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.dao.util.DetalheTelefoniaUtilRel;
import org.abepomi.dao.util.RefVivoRel;
import org.abepomi.mb.util.DownloadArq;
import org.abepomi.mb.util.GeraMd5;
import org.abepomi.model.Associado;
import org.abepomi.model.Faturamento;
import org.abepomi.model.FaturamentoPk;
import org.abepomi.model.Referencia;
import org.abepomi.model.cobranca.Titulo;

/**
 *
 * @author Julianno
 */
@ManagedBean(name = "getBean")
@RequestScoped
public class BeanGetMB {

    private static String[] FATURAMENTO_MENSAL = {"Faturamento Mensal", "0"};
    private static String[] FATURA_CLARO = {"Fatura Claro", "1"};
    private static String[] FATURA_VIVO = {"Fatura Vivo", "2"};
    private static String[] BOLETO = {"Boleto Bancário", "3"};

    public void preRenderView() {
        if (FacesContext.getCurrentInstance().getMessageList().isEmpty()) {
            FaturamentoDAO fatDAO = null;
            try {
                associado = DAOFactory.createAssociadoDAO().findById(Integer.parseInt(ass));
                referencia = DAOFactory.createReferenciaDAO().findById(Integer.parseInt(ref));
                FaturamentoPk fatPk = new FaturamentoPk();
                fatPk.setAssociado(associado);
                fatPk.setReferencia(referencia);
                fatDAO = DAOFactory.createFaturamentoDAO();
                faturamento = fatDAO.findById(fatPk);
                titulo = DAOFactory.createTituloDAO().buscarPorAssRef(associado, referencia);
            } catch (DAOException ex) {
                Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, null, ex);
            }

            links.add(FATURAMENTO_MENSAL);

            if(null!=titulo)
                links.add(BOLETO);

            if (faturamento.getTelefonia() > 0.00) {
                links.add(FATURA_CLARO);
            }

            if (faturamento.getVivo() > 0.00) {
                links.add(FATURA_VIVO);
            }
            
        }
        if (rel == 0) {
            relatorioFaturamento();
        }
        if (rel == 1) {
            relFaturaTelefonica();
        }
        if (rel == 2) {
            relatorioVivo();
        }

        if(rel == 3){
            List<Titulo> lista = new ArrayList();
            lista.add(titulo);
            try {
                DownloadArq.download(CobrancaBB.gerarCobrancaTitulos(lista), "boleto.pdf");
            } catch (Exception ex) {
                Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String relatorioFaturamento() {
        List<Faturamento> lista = new ArrayList();
        lista.add(faturamento);
        Map parametros = new HashMap();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        try {
            parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        } catch (DAOException ex) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        parametros.put("NOME_RELATORIO", "Demonstrativo Mensal do Associado");
        parametros.put("SUBREPORT_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relFatAssociado.jasper");
        return "falha";
    }

    public String relFaturaTelefonica() {
        List<DetalheTelefoniaUtilRel> lista = DAOFactory.createDetalheDAO().getTotaisLinhaAssReferencia(referencia.getId(), associado.getId());
        associado = new Associado();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        Map parametros = new HashMap();
        try {
            parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        } catch (DAOException ex) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        parametros.put("NOME_RELATORIO", "Demonstrativo de Utilização de Telefonia");
        parametros.put("SUB_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relFaturaTelefone.jasper");
        return "";
    }

    public String relatorioVivo() {
        List<RefVivoRel> lista = DAOFactory.createRelatorioVivoDAO().recuperarFaturaAssReferencia(referencia.getId(), associado.getId());
        Map parametros = new HashMap();
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/";
        try {
            parametros.put("ASSOCIACAO", DAOFactory.createAssociacaoDAO().findById(1));
        } catch (DAOException ex) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        parametros.put("NOME_RELATORIO", "Demonstrativo de Utilização de Telefonia Vivo");
        parametros.put("SUBREPORT_DIR", caminhoRelJasper);
        geraRelatorio(lista, parametros, "relFaturaTelefoneVivo.jasper");
        return "falha";
    }

    private void geraRelatorio(List lista, Map parametros, String nome) {
        String caminhoRelJasper = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF/classes/relatorios/" + nome;
        JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
        JasperPrint impressao = null;
        try {
            impressao = JasperFillManager.fillReport(caminhoRelJasper, parametros, ds);
            try {
                DownloadArq.download(JasperExportManager.exportReportToPdf(impressao), "relatorio.pdf");
            } catch (Exception ex) {
                Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, "Erro ao gerar relatório: " + nome, ex);
            }
        } catch (JRException e) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, "Falha ao chamar org.abepomi.mb.BeanGetMB.gerarRelatorio", e);
        }
    }

    public void validarHash(FacesContext context, UIComponent toValidate, Object value) {
        String paramCPF = context.getExternalContext().getRequestParameterMap().get("cpf");
        String paramAss = context.getExternalContext().getRequestParameterMap().get("ass");
        String paramRef = context.getExternalContext().getRequestParameterMap().get("ref");
        String paramRan = context.getExternalContext().getRequestParameterMap().get("ran");
        String valueHash = String.valueOf(value);
        try {
            if (valueHash.equals(GeraMd5.md5(paramCPF + paramAss + paramRef + paramRan))) {
                return;
            } else {
                throw new ValidatorException(new FacesMessage("Hash inválido!"));
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, "validarHash", ex);
            throw new ValidatorException(new FacesMessage("Impossível validar o Hash!"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(BeanGetMB.class.getName()).log(Level.SEVERE, "validarHash", ex);
            throw new ValidatorException(new FacesMessage("Impossível validar o Hash!"));
        }
    }

    //getters and setters
    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;


    }

    public Faturamento getFaturamento() {
        return faturamento;


    }

    public void setFaturamento(Faturamento faturamento) {
        this.faturamento = faturamento;


    }

    public Referencia getReferencia() {
        return referencia;


    }

    public void setReferencia(Referencia referencia) {
        this.referencia = referencia;


    }

    public String getAss() {
        return ass;


    }

    public void setAss(String ass) {
        this.ass = ass;


    }

    public String getCpf() {
        return cpf;


    }

    public void setCpf(String cpf) {
        this.cpf = cpf;


    }

    public String getHas() {
        return has;


    }

    public void setHas(String has) {
        this.has = has;


    }

    public String getRan() {
        return ran;


    }

    public void setRan(String ran) {
        this.ran = ran;


    }

    public String getRef() {
        return ref;


    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public int getRel() {
        return rel;
    }

    public void setRel(int rel) {
        this.rel = rel;
    }

    public List<String[]> getLinks() {
        return links;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    //attrs
    private Associado associado;
    private Referencia referencia;
    private Faturamento faturamento;
    private Titulo titulo;
    private List<String[]> links = new ArrayList();
    //viewPAram
    private String cpf = "";
    private String ass = "";
    private String ref = "";
    private String ran = "";
    private String has = "";
    private Integer rel = -1;

    //constructors
    public BeanGetMB() {
    }
}
