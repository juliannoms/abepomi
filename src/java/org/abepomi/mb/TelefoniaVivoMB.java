/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.abepomi.arqs.FaturaTelefonia;
import org.abepomi.dao.vivo.AssociadoVivoDAO;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.commons.DAOException;
import org.abepomi.mb.util.FacesUtil;
import org.abepomi.model.Aparelho;
import org.abepomi.model.AparelhosAssociados;
import org.abepomi.model.Associado;
import org.abepomi.model.ExtrasTelefonia;
import org.abepomi.model.util.TipoLinha;
import org.abepomi.model.vivo.AssociadoVivo;
import org.abepomi.model.vivo.ContratoVivo;
import org.abepomi.model.vivo.LinhaVivo;

/**
 *
 * @author Julianno
 */
@ManagedBean(name = "teleVivo")
@SessionScoped
public class TelefoniaVivoMB implements Serializable {

    public TelefoniaVivoMB() {
        assVivo = new AssociadoVivo();
        assVivoDAO = DAOFactory.createAssociadoVivoDAO();
        associados = new ArrayList<Associado>();
        associado = new Associado();
        linha = new LinhaVivo();
        contrato = new ContratoVivo();
        apAssociado = new AparelhosAssociados();
        extra = new ExtrasTelefonia();

    }
    private AssociadoVivo assVivo;
    private AssociadoVivoDAO assVivoDAO;
    private Associado associado;
    private LinhaVivo linha;
    private ContratoVivo contrato = null;
    private AparelhosAssociados apAssociado = null;
    private ExtrasTelefonia extra = null;
    private Integer assId = null;
    private Date dataDevolucao = null;
    private String linhaDevolucao = null;
    //
    private List<LinhaVivo> linhas = null;
    private List<ContratoVivo> contratos = null;
    private List<Associado> associados = null;
    private List<String> linhasAssociados = null;
    //
    private boolean mostrarLinhas = false;
    private boolean mostrarContratos = false;
    private boolean alteracao = false;
    private boolean comAparelho = false;
    private boolean comHabilitacao = false;
    //
    private Double valorHabil = 0.00;
    private int parcelaHabil = 0;

    public AssociadoVivo getAssVivo() {
        return assVivo;
    }

    public void setAssVivo(AssociadoVivo assVivo) {
        this.assVivo = assVivo;
    }

    public List<Associado> getAssociados() {
        try {
            associados = DAOFactory.createAssociadoDAO().findAllByNome();
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao recuperar associados teleVivo!", ex);
        }
        return associados;
    }

    public List<Associado> getAssociadosComLinhasVivo() {
        associados = DAOFactory.createAssociadoVivoDAO().buscaAssociadoComLinha();
        return associados;
    }

    public void setAssociados(List<Associado> associados) {
        this.associados = associados;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public boolean isMostrarLinhas() {
        return mostrarLinhas;
    }

    public void setMostrarLinhas(boolean mostrarLinhas) {
        this.mostrarLinhas = mostrarLinhas;
    }

    public boolean isMostrarContratos() {
        return mostrarContratos;
    }

    public void setMostrarContratos(boolean mostrarContratos) {
        this.mostrarContratos = mostrarContratos;
    }

    public boolean isAlteracao() {
        return alteracao;
    }

    public void setAlteracao(boolean alteracao) {
        this.alteracao = alteracao;
    }

    public boolean isComAparelho() {
        return comAparelho;
    }

    public void setComAparelho(boolean comAparelho) {
        this.comAparelho = comAparelho;
    }

    public ContratoVivo getContrato() {
        return contrato;
    }

    public void setContrato(ContratoVivo contrato) {
        this.contrato = contrato;
    }

    public AparelhosAssociados getApAssociado() {
        return apAssociado;
    }

    public void setApAssociado(AparelhosAssociados apAssociado) {
        this.apAssociado = apAssociado;
    }

    public boolean isComHabilitacao() {
        return comHabilitacao;
    }

    public void setComHabilitacao(boolean comHabilitacao) {
        this.comHabilitacao = comHabilitacao;
    }

    public int getParcelaHabil() {
        return parcelaHabil;
    }

    public void setParcelaHabil(int parcelaHabil) {
        this.parcelaHabil = parcelaHabil;
    }

    public Double getValorHabil() {
        return valorHabil;
    }

    public void setValorHabil(Double valorHabil) {
        this.valorHabil = valorHabil;
    }

    public ExtrasTelefonia getExtra() {
        return extra;
    }

    public void setExtra(ExtrasTelefonia extra) {
        this.extra = extra;
    }

    public Integer getAssId() {
        return assId;
    }

    public void setAssId(Integer assId) {
        this.assId = assId;
    }

    public List<LinhaVivo> getLinhasLivre() {
        return DAOFactory.createLinhaVivoDAO().buscarLinhasVivoLivres();
    }

    public List<Aparelho> getAparelhos() {
        return DAOFactory.createAparelhoDAO().buscarComEstoque();
    }

    public List<LinhaVivo> getLinhas() throws DAOException {
        if (null == linhas) {
            linhas = DAOFactory.createLinhaVivoDAO().findAll();
        }
        return linhas;
    }

    public void setLinhas(List<LinhaVivo> linhas) {
        this.linhas = linhas;
    }

    private List<ExtrasTelefonia> getParcelasAparelho() {
        List<ExtrasTelefonia> extras = new ArrayList<ExtrasTelefonia>();
        if (apAssociado.getValor() > 0.00) {
            BigDecimal valorB = new BigDecimal(apAssociado.getValor().toString()).setScale(2);
            BigDecimal parcelasB = new BigDecimal(apAssociado.getParcelas() + ".00").setScale(2);
            BigDecimal parcela = valorB.divide(parcelasB, 2, RoundingMode.DOWN);
            BigDecimal resto = valorB.subtract(parcela.multiply(parcelasB));
            BigDecimal auxiliar = new BigDecimal("0.01").setScale(2);
            for (int i = 1; i <= apAssociado.getParcelas(); i++) {
                ExtrasTelefonia ext = new ExtrasTelefonia();
                ext.setDescricao("PARCELA " + i + " AQUISIÇÃO APARELHO!");
                ext.setLinha(assVivo.getLinha());
                if (resto.doubleValue() > 0.00) {
                    ext.setValor(parcela.add(auxiliar).doubleValue());
                    resto = resto.subtract(auxiliar);
                } else {
                    ext.setValor(parcela.doubleValue());
                }
                ext.setTipo(ExtrasTelefonia.TIPO_APARELHO_VIVO);
                extras.add(ext);
            }
        }
        return extras;
    }

    private List<ExtrasTelefonia> getParcelasHabilitacao() {
        List<ExtrasTelefonia> extras = new ArrayList<ExtrasTelefonia>();
        BigDecimal valorB = new BigDecimal(valorHabil.toString()).setScale(2);
        BigDecimal parcelasB = new BigDecimal(parcelaHabil + ".00").setScale(2);
        BigDecimal parcela = valorB.divide(parcelasB, 2, RoundingMode.DOWN);
        BigDecimal resto = valorB.subtract(parcela.multiply(parcelasB));
        BigDecimal auxiliar = new BigDecimal("0.01").setScale(2);
        for (int i = 1; i <= parcelaHabil; i++) {
            ExtrasTelefonia ext = new ExtrasTelefonia();
            ext.setDescricao("PARCELA " + i + " HABILITAÇÃO!");
            ext.setLinha(assVivo.getLinha());
            if (resto.doubleValue() > 0.00) {
                ext.setValor(parcela.add(auxiliar).doubleValue());
                resto = resto.subtract(auxiliar);
            } else {
                ext.setValor(parcela.doubleValue());
            }
            ext.setTipo(ExtrasTelefonia.TIPO_HABILITACAO_VIVO);
            extras.add(ext);
        }
        return extras;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public String getLinhaDevolucao() {
        return linhaDevolucao;
    }

    public void setLinhaDevolucao(String linhaDevolucao) {
        this.linhaDevolucao = linhaDevolucao;
    }

    public String salvarAssVivo() {
        try {
            assVivoDAO.addEntity(assVivo);
            if (isComAparelho()) {
                DAOFactory.createApAssociadosDAO().addEntity(apAssociado);
                for (ExtrasTelefonia ext : getParcelasAparelho()) {
                    DAOFactory.createExtrasTelefoniaDAO().addEntity(ext);
                }
            }
            if (isComHabilitacao()) {
                for (ExtrasTelefonia e : getParcelasHabilitacao()) {
                    DAOFactory.createExtrasTelefoniaDAO().addEntity(e);
                }
            }
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar AssociadoVivo e AparelhosAssociados!", ex);
            return "falha";
        }

        return limparAssVivo();
    }

    public String limparAssVivo() {
        assVivo = new AssociadoVivo();
        associado = new Associado();
        apAssociado = new AparelhosAssociados();
        valorHabil = 0.00;
        parcelaHabil = 0;
        return "assVivo";
    }

    public String salvarLinhaVivo() {
        try {
            if (isAlteracao()) {
                DAOFactory.createLinhaVivoDAO().updateEntity(linha);
                setAlteracao(false);
            } else {
                DAOFactory.createLinhaVivoDAO().addEntity(linha);
            }
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar LinhaVivo", ex);
        }
        linhas = null;
        linha = new LinhaVivo();
        return "/telefonia/linhas_vivo";
    }

    public String limparLinhaVivo() {
        linha = new LinhaVivo();
        setAlteracao(false);
        return "/telefonia/linhas_vivo";
    }

    public String salvarContrato() {
        try {
            if (contrato.getId() != 0) {
                DAOFactory.createContratoVivoDAO().updateEntity(contrato);
            } else {
                DAOFactory.createContratoVivoDAO().addEntity(contrato);
            }
            contrato = new ContratoVivo();
            contratos = null;
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar ContratoVivo", ex);
            return "falha";
        }
        return "/telefonia/contratos_vivo";

    }

    public String limparContrato() {
        contrato = new ContratoVivo();
        return "/telefonia/contratos_vivo";
    }

    public String apagarContrato() {
        try {
            DAOFactory.createContratoVivoDAO().removeEntity(contrato);
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao apagar contrato!", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contrato não pode ser apagado!", null));
            return "falha";
        }
        contrato = new ContratoVivo();
        contratos = null;
        return "/telefonia/contratos_vivo";
    }

    public List<SelectItem> getTiposLinha() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        for (TipoLinha type : TipoLinha.values()) {
            lista.add(new SelectItem(type, type.toString()));
        }
        return lista;
    }

    public List<ContratoVivo> getContratos() throws DAOException {
        if (null == contratos) {
            contratos = DAOFactory.createContratoVivoDAO().findAll();
        }
        return contratos;
    }

    public void setContratos(List<ContratoVivo> contratos) {
        this.contratos = contratos;
    }

    public LinhaVivo getLinha() {
        return linha;
    }

    public void setLinha(LinhaVivo linha) {
        this.linha = linha;
    }

    public void updateValor(AjaxBehaviorEvent event) {
        getApAssociado().setValor(getApAssociado().getAparelho().getValorVenda());
        FacesUtil.cleanSubmittedValues(event.getComponent().getParent());
    }

    public List<Associado> getAssociadoLinhaVivo() {
        return DAOFactory.createAssociadoVivoDAO().buscaAssociadoComLinha();
    }

    public List<String> getLinhasAssociados() {
        return linhasAssociados;
    }

    public void carregarLinhasAssociados(ValueChangeEvent event) {
        if (event.getNewValue() != event.getOldValue()) {
            if (event.getNewValue() instanceof Integer) {
                linhasAssociados = DAOFactory.createAssociadoVivoDAO().buscaLinhaPorAssId((Integer) event.getNewValue());
            }
            FacesContext.getCurrentInstance().renderResponse();
        }
    }

    public String salvarLancamentoExtra() {
        try {
            DAOFactory.createExtrasTelefoniaDAO().addEntity(extra);
        } catch (DAOException ex) {
            Logger.getLogger(TelefoniaVivoMB.class.getName()).log(Level.SEVERE, "Erro ao salvar extras vivo!", ex);
        }
        return limparLancamentoExtra();
    }

    public String limparLancamentoExtra() {
        extra = new ExtrasTelefonia();
        assId = null;
        return "/telefonia/extra_vivo";
    }

    public String faturarFaturasVivo() {
        FaturaTelefonia.faturarVivo();
        return "sucesso";
    }

    public String devolverLinhaAssociado(){
        boolean retorno = DAOFactory.createAssociadoVivoDAO().atualizarDevolucao(assId, linhaDevolucao, dataDevolucao);
        assId = null;
        linhaDevolucao = null;
        dataDevolucao = null;
        if(retorno){
            return "sucesso";
        }
        return "falha";
    }
}
