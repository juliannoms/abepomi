/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.mb.util;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

/**
 *
 * @author Julianno
 */
public class FacesUtil {

    public static void cleanSubmittedValues(UIComponent component) {
        if (component instanceof EditableValueHolder) {
            EditableValueHolder evh = (EditableValueHolder) component;
            evh.setSubmittedValue(null);
            evh.setValue(null);
            evh.setLocalValueSet(false);
            evh.setValid(true);
        }
        if (component.getChildCount() > 0) {
            for (UIComponent child : component.getChildren()) {
                cleanSubmittedValues(child);
            }
        }
    }

}
