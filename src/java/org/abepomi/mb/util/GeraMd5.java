/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Julianno
 */
public class GeraMd5 {

    public static String md5(String chave) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = null;
        md = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, md.digest(chave.getBytes("UTF-8")));
        return hash.toString(16);
    }
}
