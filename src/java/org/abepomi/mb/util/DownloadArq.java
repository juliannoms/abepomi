/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Julianno
 */
public class DownloadArq {

    private static HttpServletResponse getServletResponse() {
        return (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
    }

    private static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public static void download(byte[] arquivo, String nome) throws Exception {
        InputStream is = new ByteArrayInputStream(arquivo);
        HttpServletResponse response = getServletResponse();
        //response.setContentType("application/pdf");
        response.setContentLength(arquivo.length);
        response.addHeader("Content-Disposition", "attachment; filename=" + "\"" + nome + "\"");
        /*ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(arquivo,0,arquivo.length);*/
        PrintWriter outputStream = response.getWriter();
        int i = 0;
        while(i<arquivo.length){
            outputStream.write(is.read());
            i++;
        }
        is.close();
        outputStream.flush();
        outputStream.close();
        getFacesContext().responseComplete();
    }
}
