/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.abepomi.mb;

import java.io.Serializable;
import java.util.List;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.PaginaDAO;
import org.abepomi.model.Pagina;

/**
 *
 * @author Julianno
 */

public class PaginasMB implements Serializable{

    List<Pagina> paginasSeguras, paginasLiberadas = null;
    PaginaDAO pagDAO = DAOFactory.createPaginaDAO();

    public PaginasMB(){
            paginasSeguras = pagDAO.getPaginasSeguras();
            paginasLiberadas = pagDAO.getPaginasLiberadas();

    }

    public List<Pagina> getPaginasLiberadas() {
        return paginasLiberadas;
    }

    public List<Pagina> getPaginasSeguras() {
        return paginasSeguras;
    }

}
