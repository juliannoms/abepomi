/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.mb;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.abepomi.dao.DAOFactory;
import org.abepomi.dao.UserDAO;
import org.abepomi.dao.commons.DAOException;
//import org.abepomi.mb.util.GeraMd5;
import org.abepomi.mb.util.GeraMd5;
import org.abepomi.model.Pagina;
import org.abepomi.model.User;

/**
 *
 * @author Julianno
 */
public class UserMB implements Serializable {

    private User user = new User();
    private User novoUser = new User();
    private UserDAO userDAO = DAOFactory.createUserDAO();
    private List<User> usuarios = new ArrayList<User>();
    private String busca = "";
    private String senha = "";
    private boolean alterando = false;

    public UserMB() {
    }

    public boolean isAlterando() {
        return alterando;
    }

    public boolean isListaUsuariosVazia() {
        return (usuarios.size() > 0);
    }

    public void setAlterando(boolean alterando) {
        this.alterando = alterando;
    }

    public String getBusca() {
        return busca;
    }

    public void setBusca(String busca) {
        this.busca = busca;
    }

    public List<User> getUsuarios() {
        return usuarios;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User currentUser) {
        this.user = currentUser;
    }

    public User getNovoUser() {
        return novoUser;
    }

    public void setNovoUser(User novoUser) {
        this.novoUser = novoUser;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Pagina> getPaginasUser() {
        List<Pagina> paginas;
        paginas = novoUser.getPaginas();
        novoUser.setPaginas(null);
        return paginas;
    }

    public void setPaginasUser(List<Pagina> paginas) {
        novoUser.setPaginas(paginas);
    }

    public String salvar() {
        //UserDAO userDAO = DAOFactory.createUserDAO();
        try {
            novoUser.getPaginas().addAll(DAOFactory.createPaginaDAO().getPaginasLiberadas());
            if (isAlterando()) {
                //novoUser.setSenha(md5(novoUser.getSenha()));
                userDAO.updateEntity(novoUser);
            } else {
                novoUser.setSenha(GeraMd5.md5(novoUser.getLogin()));
                userDAO.addEntity(novoUser);
            }
            setAlterando(false);
            novoUser = new User();
        } catch (NoSuchAlgorithmException nsa) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", nsa);
            return "falha";
        } catch (UnsupportedEncodingException uee) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", uee);
            return "falha";
        } catch (DAOException ex) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", ex);
            return "falha";
        }
        return "sucesso";
    }

    public String login() {
        User other = null;
        //UserDAO userDAO = DAOFactory.createUserDAO();
        try {
            other = userDAO.findById(user.getLogin());
        } catch (DAOException ex) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao logar", ex);
        }

        if (null == other) {
            FacesContext.getCurrentInstance().addMessage("principal", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não existe!!!", ""));
            return "loginPage";
        }

        try {
            if (!(GeraMd5.md5(user.getSenha()).equals(other.getSenha()))) {
                FacesContext.getCurrentInstance().addMessage("principal", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário e/ou senha não conferem!!!", ""));
                return "loginPage";
            }
        } catch (NoSuchAlgorithmException nsa) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", nsa);
            return "falha";
        } catch (UnsupportedEncodingException uee) {
            Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", uee);
            return "falha";
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("currentUser", other);

        if (user.getSenha().equals(user.getLogin())) {
            return "alterarSenha";
        }

        /*
        EmailAbepomi email = new EmailAbepomi();
        email.sendMail("juliannoms@gmail.com", "Logon de usuário!", "O usuário '"+other.getLogin()+"' entrou no sistema!\nNome: "+other.getNome()+".\nData: "+new Date()+".");
         */
        return "index";
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("currentUser", null);
        return "loginPage";
    }

    public String selecionar() {
        /*try {
        novoUser = userDAO.findById(novoUser.getLogin());
        } catch (DAOException ex) {
        Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao selecionar usuário!", ex);
        }*/
        //novoUser.setPaginas(null);
        setAlterando(true);
        return "cadastroUsuario";
    }

    public String buscar() {
        usuarios = userDAO.buscarPorLogin(busca);
        return "pesquisaUsuario";
    }

    public String limpar() {
        novoUser = new User();
        setAlterando(false);
        return "cadastroUsuario";
    }

    public String alterarSenha() {
        if (user.getSenha().equals(senha)) {
            user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentUser");
            try {
                user.setSenha(GeraMd5.md5(senha));
            } catch (NoSuchAlgorithmException nsa) {
                Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", nsa);
                return "falha";
            } catch (UnsupportedEncodingException uee) {
                Logger.getLogger(UserMB.class.getName()).log(Level.SEVERE, "Erro ao salvar usuário!", uee);
                return "falha";
            }
            try {
                userDAO.updateEntity(user);
            } catch (DAOException ex) {
                FacesContext.getCurrentInstance().addMessage("principal", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao alterar senha!", ""));
                return "loginPage";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage("principal", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha e confirmação não confere!", ""));
            return "alterarSenha";
        }

        return "index";

    }
}
