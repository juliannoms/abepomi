/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.abepomi.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.abepomi.model.util.Validacao;

/**
 *
 * @author Julianno
 */
public class ValidaCpfCnpj implements Validator {

    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String valor = (String) value;
        if (!valor.equals("")) {
            if (valor.length() == 11) {
                if (!Validacao.validaCpf(valor)) {
                    FacesMessage message = new FacesMessage();
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    message.setSummary("CPF inválido!");
                    throw new ValidatorException(message);
                }
            } else if (valor.length() == 14) {
                if (!Validacao.validaCnpj(valor)) {
                    FacesMessage message = new FacesMessage();
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    message.setSummary("CNPJ inválido!");
                    throw new ValidatorException(message);
                }
            } else {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Entrada inválida!");
                throw new ValidatorException(message);
            }
        }

    }
}
